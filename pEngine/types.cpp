#include "types.h"

void MatrixCPUAndGPU::createTransponse()
{
	_mT = Matrix4::Transponse(_m);
}
MatrixCPUAndGPU::MatrixCPUAndGPU() :_m(Matrix::Identity()), _mT(Matrix::Identity()) {}
MatrixCPUAndGPU::MatrixCPUAndGPU(const Matrix4& m) :_m(m)
{
	createTransponse();
}
Matrix4& MatrixCPUAndGPU::getCPUMatrix()
{
	return _m;
}
Matrix4& MatrixCPUAndGPU::getGPUMatrix()
{
	return _mT;
}
void MatrixCPUAndGPU::updateCPUMatrix(const Matrix4& m)
{
	_m = m;
	createTransponse();
}

ModelPosition::ModelPosition(const MatrixCPUAndGPU& w) : world(w) {}
void ModelPosition::generateNormal()
{
	normal = Matrix4::Transponse(Matrix4::Inverse(world.getCPUMatrix()));
}
MatrixCPUAndGPU& ModelPosition::getWorld() { return world; }
MatrixCPUAndGPU& ModelPosition::getNormals() { return normal; }