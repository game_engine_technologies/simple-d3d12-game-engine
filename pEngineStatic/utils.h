#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <cassert>

#include "types.h"

using namespace std;

class LineUnicode : public wstring
{
public:
	friend std::wistream& operator>>(std::wistream& is, LineUnicode& l);

};

class LineAscii : public string
{
public:
	friend std::istream& operator>>(std::istream& is, LineAscii& l);

};
