#pragma once

#include "pch.h"
#include "utils.h"

struct DYNLIB_PENGINE Material
{
	Vector ambient;
	Vector diffuse;
	Vector specular;
	float shininess;

	Material();
	Material(const Vector& a, const Vector& d, const Vector& s, const float& sn);
};

class DYNLIB_PENGINE ContainerMaterials
{
	map<wstring, Material> materials;

public:
	ContainerMaterials();
	bool addMaterial(const wstring& name, const Material& m);
	bool getMaterial(const wstring& name, Material** m);
	bool getFirst(wstring& name);
};