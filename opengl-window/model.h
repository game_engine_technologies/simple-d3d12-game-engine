#pragma once

#include "../pEngineStatic/types.h"
#include "../pEngineStatic/cBufferTypes.h"

#include "../pEngine/camera.h"
#include "../pEngine/lights.h"
#include "../pEngine/materials.h"
#include "../pEngine/sun.h"
#include "../pEngine/torch.h"

#include "pso.h"
#include "CBufferCreator.h"

class Model;
using  ModelPtr = shared_ptr<Model>;

struct GameParametersOpenGL
{
	map<wstring, CBufferGeneralPtr>* cBuffers;
	ContainerMaterials* materials;
	Camera* camera;
	Sun* sunLight;
	LightPoint* lightCenter;
	Torch* torch;
	Matrix view;
	Matrix proj;
	Vector torchProperty;

	GameParametersOpenGL() {}
};

class Model
{
protected:
	GLuint VBO;
	GLuint VAO;
	GLuint IBO;

	wstring psoKey;

	vector< size_t> countIndex;
	vector<wstring> texturesKey;
	vector<wstring> materialsKey;
	vector<bool> dSides;

public:
	virtual void createModel(map<wstring, PipelineStateObjectPtr> psos, BlankModel& data) = 0;

	virtual void draw(ContainerTextures& textures, PipelineStateObjectPtr& pso, GameParametersOpenGL& gameParameters) = 0;

	virtual void addWorld(Matrix m) { }
	virtual void addWorld(MatrixCPUAndGPU m) { }
	virtual void editWorld(Matrix m, size_t index) { }
	virtual void editWorld(MatrixCPUAndGPU m, size_t index) { }
	virtual wstring& getPsoKey() = 0;
};

class ModelIdentity: public Model
{
	vector<ModelPosition> worlds;

public:
	void createModel(map<wstring, PipelineStateObjectPtr> psos, BlankModel& data) override
	{
		countIndex = data.shiftIndexs;
		texturesKey = data.texturesName;
		materialsKey = data.materials;
		dSides = data.doubleSides;
		psoKey = data.psoName;

		auto pso = psos[psoKey];
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &IBO);
		// Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * data.vertexs.size(), data.vertexs.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Index) * data.indexs.size(), data.indexs.data(), GL_STATIC_DRAW);

		// attrib
		for (size_t i(0); i < pso->getInputLayout().size(); i++)
		{
			auto&& il = pso->getInputLayout().at(i);
			glVertexAttribPointer(i, il.sizeMember, GL_FLOAT, GL_FALSE, il.shiftBytesAll, (GLvoid*)il.shiftBytesCurrent);
			glEnableVertexAttribArray(i);
		}

		glBindBuffer(GL_ARRAY_BUFFER, 0); // Note that this is allowed, the call to glVertexAttribPointer registered VBO as the currently bound vertex buffer object so afterwards we can safely unbind

		glBindVertexArray(0); // Unbind VAO (it's always a good thing to unbind any buffer/array to prevent strange bugs), remember: do NOT unbind the EBO, keep it bound to this VAO

	}

	void draw(ContainerTextures& textures, PipelineStateObjectPtr& pso, GameParametersOpenGL& gameParameters) override
	{
		cbufferCamera cbCam(Matrix::Identity(), gameParameters.view, gameParameters.proj);
		cbufferLight cbLight(gameParameters.sunLight->getLightSun().getColor(), gameParameters.sunLight->getLightSun().getDirection(),
			gameParameters.lightCenter->getColor(), gameParameters.lightCenter->getPosition(), (Vector)gameParameters.camera->getPosition(),
			Matrix::Identity(),
			gameParameters.torch->getLight().getColor(), gameParameters.torch->getLight().getDirection(), gameParameters.torch->getLight().getPosition(),
			gameParameters.torchProperty);
		cbufferMaterial cbMat;

		GLuint sp = pso->getShaderProgram();
		glUseProgram(sp);
		glBindVertexArray(VAO);
		for (size_t j(0); j < worlds.size(); j++)
		{
			cbCam.world = worlds[j].getWorld().getGPUMatrix();
			cbLight.normals = worlds[j].getNormals().getGPUMatrix();
			gameParameters.cBuffers->at(L"cbCamera")->updateData(sp, (void*)&cbCam);
			gameParameters.cBuffers->at(L"cbLight")->updateData(sp, (void*)&cbLight);
			size_t startIndex(0);
			for (size_t l(0); l < texturesKey.size(); l++)
			{
				Texture* texture(nullptr);
				if (texturesKey[l] != IS_COLOR && textures.getTexture(texturesKey[l], &texture))
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, texture->getTexture());
					glUniform1i(glGetUniformLocation(sp, "texture1"), 0);
				}
				Material* m(nullptr);
				if (gameParameters.materials->getMaterial(materialsKey[l], &m))
				{
					cbMat = { m->ambient, m->diffuse, m->specular, m->shininess };
					gameParameters.cBuffers->at(L"cbMaterial")->updateData(sp, (void*)&cbMat);
				}
				//glDrawArrays(GL_TRIANGLES, 0, 6); // draw vertex
				if(dSides[l])
					glDisable(GL_CULL_FACE);
				else
				{
					glFrontFace(GL_CW);
					glEnable(GL_CULL_FACE);
					glCullFace(GL_BACK);
				}
				glDrawElements(GL_TRIANGLES, countIndex[l], GL_UNSIGNED_INT, (void*)(startIndex * sizeof(Index))); // draw index
				startIndex += countIndex[l];
			}
		}
		glBindVertexArray(0);
	}

	void addWorld(Matrix m) override { ModelPosition _m(m);  worlds.push_back(_m); }
	void addWorld(MatrixCPUAndGPU m)  override { ModelPosition _m(m);  worlds.push_back(_m); }
	void editWorld(Matrix m, size_t index) override { ModelPosition _m(m);  worlds[index] = _m; }
	void editWorld(MatrixCPUAndGPU m, size_t index) override { ModelPosition _m(m);  worlds[index] = _m; }
	wstring& getPsoKey() override { return psoKey; }

	~ModelIdentity()
	{
		glDeleteVertexArrays(1, &VAO);
		glDeleteBuffers(1, &VBO);
		glDeleteBuffers(1, &IBO);
	}
};

class ModelInstancing: public Model
{
	vector<vector<Matrix>> strBufferData;
	vector<GLuint> vboInstanced;
	GLuint countInstanced;

public:
	void setStructuredBuffer(vector<Matrix>& strBufferData)
	{
		this->strBufferData.push_back(std::move(strBufferData));
	}

	void createModel(map<wstring, PipelineStateObjectPtr> psos, BlankModel& data) override
	{
		countInstanced = strBufferData[0].size();
		countIndex = data.shiftIndexs;
		texturesKey = data.texturesName;
		materialsKey = data.materials;
		dSides = data.doubleSides;
		psoKey = data.psoName;

		auto pso = psos[psoKey];
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &IBO);
		// Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * data.vertexs.size(), data.vertexs.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Index) * data.indexs.size(), data.indexs.data(), GL_STATIC_DRAW);

		// attrib
		size_t num(0);
		for (size_t i(0); i < pso->getInputLayout().size(); i++)
		{
			auto&& il = pso->getInputLayout().at(i);
			glVertexAttribPointer(num, il.sizeMember, GL_FLOAT, GL_FALSE, il.shiftBytesAll, (GLvoid*)il.shiftBytesCurrent);
			glEnableVertexAttribArray(num++);
		}

		vboInstanced.resize(strBufferData.size());
		for (size_t i(0), j(0); i < strBufferData.size(); i++)
		{
			glGenBuffers(1, &vboInstanced[i]);
			glBindBuffer(GL_ARRAY_BUFFER, vboInstanced[i]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Matrix) * strBufferData[i].size(), strBufferData[i].data(), GL_STATIC_DRAW);

			for (size_t k(0); k < 4; j++, k++)
			{
				glEnableVertexAttribArray(num);
				auto&& il = pso->getInputLayoutInstanced().at(j);
				glVertexAttribPointer(num, il.sizeMember, GL_FLOAT, GL_FALSE, il.shiftBytesAll, (GLvoid*)il.shiftBytesCurrent);
				glVertexAttribDivisor(num++, 1);
			}

			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		
		glBindVertexArray(0); 
	}

	void draw(ContainerTextures& textures, PipelineStateObjectPtr& pso, GameParametersOpenGL& gameParameters) override
	{
		cbufferCamera cbCam(Matrix::Identity(), gameParameters.view, gameParameters.proj);
		cbufferLight cbLight(gameParameters.sunLight->getLightSun().getColor(), gameParameters.sunLight->getLightSun().getDirection(),
			gameParameters.lightCenter->getColor(), gameParameters.lightCenter->getPosition(), (Vector)gameParameters.camera->getPosition(),
			Matrix::Identity(),
			gameParameters.torch->getLight().getColor(), gameParameters.torch->getLight().getDirection(), gameParameters.torch->getLight().getPosition(),
			gameParameters.torchProperty);
		cbufferMaterial cbMat;

		GLuint sp = pso->getShaderProgram();
		glUseProgram(sp);
		glBindVertexArray(VAO);

		size_t startIndex(0);
		for (size_t l(0); l < texturesKey.size(); l++)
		{
			gameParameters.cBuffers->at(L"cbCamera")->updateData(sp, (void*)&cbCam);
			gameParameters.cBuffers->at(L"cbLight")->updateData(sp, (void*)&cbLight);
			Texture* texture(nullptr);
			if (texturesKey[l] != IS_COLOR && textures.getTexture(texturesKey[l], &texture))
			{
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, texture->getTexture());
				glUniform1i(glGetUniformLocation(sp, "texture1"), 0);
			}
			Material* m(nullptr);
			if (gameParameters.materials->getMaterial(materialsKey[l], &m))
			{
				cbMat = { m->ambient, m->diffuse, m->specular, m->shininess };
				gameParameters.cBuffers->at(L"cbMaterial")->updateData(sp, (void*)&cbMat);
			}
			if (dSides[l])
				glDisable(GL_CULL_FACE);
			else
			{
				glFrontFace(GL_CW);
				glEnable(GL_CULL_FACE);
				glCullFace(GL_BACK);
			}
			glDrawElementsInstanced(GL_TRIANGLES, countIndex[l], GL_UNSIGNED_INT, (void*)(startIndex * sizeof(Index)), countInstanced);

			startIndex += countIndex[l];
		}

		glBindVertexArray(0);
	}

	wstring& getPsoKey() override { return psoKey; }

	~ModelInstancing()
	{
		glDeleteVertexArrays(1, &VAO);
		glDeleteBuffers(1, &VBO);
		glDeleteBuffers(1, &IBO);
		for (auto&& vbo : vboInstanced)
			glDeleteBuffers(1, &vbo);
	}
};