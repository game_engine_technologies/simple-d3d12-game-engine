#include "game_prop.h"

GameQuality::GameQuality(const GameQualityConst& cq) : currentQuality(cq)
{
	foldersShader.insert({ QUALITY_VERY_LOW , L"very-low" });
	foldersShader.insert({ QUALITY_LOW , L"low" });
}
wstring& GameQuality::getFolder()
{
	return foldersShader[currentQuality];
}