#pragma once

#include "sBufferTypes.h"

#include <any>
#include <cassert>

using namespace gmath;
using namespace std;

using Index = unsigned int;

using _byte = unsigned char;

class _bitset;

template <bool Direction, bool TypeIterator, class iter_category>
class IteratorBitset
{
public:
	// ������������� � stl
	using difference_type = std::ptrdiff_t;
	using value_type = bool;
	using pointer = std::conditional_t< TypeIterator, const bool*, bool*>;
	using reference = std::conditional_t< TypeIterator, const bool&, bool&>;
	using iterator_category = iter_category;

	using container = std::conditional_t< TypeIterator, const _bitset*, _bitset*>;

private:
	friend class IteratorBitset<Direction, !TypeIterator, iter_category>;

	container _ptr;
	size_t index;

public:
	IteratorBitset(container type);
	IteratorBitset(size_t index);
	IteratorBitset(container type, size_t index);

	value_type operator*()const; // ���� ������������ ������ �� �������� (�� ������ ������!)
	IteratorBitset& operator++();
	IteratorBitset operator++(int);

	// ��������� ��������� � ��������������
	bool operator==(const  IteratorBitset<Direction, TypeIterator, iter_category>& r)const;
	bool operator!=(const  IteratorBitset<Direction, TypeIterator, iter_category>& r)const;

	// ��������� �������������� ����
	operator IteratorBitset<true, true, iter_category>() const;

};

template<bool Direction, bool TypeIterator, class iter_category>
inline IteratorBitset<Direction, TypeIterator, iter_category>::IteratorBitset(container type) : _ptr(type), index(0) {}

template<bool Direction, bool TypeIterator, class iter_category>
inline IteratorBitset<Direction, TypeIterator, iter_category>::IteratorBitset(size_t index):_ptr(nullptr), index(index)
{
}

template<bool Direction, bool TypeIterator, class iter_category>
inline IteratorBitset<Direction, TypeIterator, iter_category>::IteratorBitset(container type, size_t index): _ptr(type), index(index)
{
}

template<bool Direction, bool TypeIterator, class iter_category>
inline auto IteratorBitset<Direction, TypeIterator, iter_category>::operator*() const -> value_type
{
	return _ptr->get(index);
}

template<bool Direction, bool TypeIterator, class iter_category>
inline IteratorBitset<Direction, TypeIterator, iter_category>& IteratorBitset<Direction, TypeIterator, iter_category>::operator++()
{
	Direction ? index++ : index--;
	return *this;
}

template<bool Direction, bool TypeIterator, class iter_category>
inline IteratorBitset<Direction, TypeIterator, iter_category> IteratorBitset<Direction, TypeIterator, iter_category>::operator++(int)
{
	auto&& result = *this;
	Direction ? index++ : index--;
	return result;
}

template<bool Direction, bool TypeIterator, class iter_category>
inline bool IteratorBitset<Direction, TypeIterator, iter_category>::operator==(const IteratorBitset<Direction, TypeIterator, iter_category>& r) const
{
	return index == r.index;
}

template<bool Direction, bool TypeIterator, class iter_category>
inline bool IteratorBitset<Direction, TypeIterator, iter_category>::operator!=(const IteratorBitset<Direction, TypeIterator, iter_category>& r) const
{
	return !(*this == r);
}

class _bitset
{
	vector < _byte> buffer;
	size_t countBits;

	void add(const _byte& b, size_t count_add);
	_byte reverseByte(const _byte& elem);

public:
	static const size_t countBitsInByte;
	using ullong = unsigned long long;

	using iterator = IteratorBitset<true, false, std::random_access_iterator_tag>;
	using const_iterator = IteratorBitset<true, true, std::random_access_iterator_tag>;
	using riterator = IteratorBitset<false, false, std::random_access_iterator_tag>;
	using const_riterator = IteratorBitset<false, true, std::random_access_iterator_tag>;

	using difference_type = void;
	using value_type = bool;
	using pointer = void;
	using reference = void;
	using iterator_category = void;

	iterator begin();
	iterator end();
	const_iterator cbegin()const;
	const_iterator cend()const;
	riterator rbegin();
	riterator rend();
	const_riterator rcbegin()const;
	const_riterator rcend()const;

	_bitset();
	_bitset(size_t s);
	_bitset(const _byte& b);
	_bitset operator=(const _byte& b);

	void push_back_byte(const _byte& b);
	void push_back_bit(const bool& b);

	string to_string() const;
	string to_string(size_t index, size_t count = 1)const; // ������ ����, ���������� ����� ��� ������
	ullong to_number(size_t index, size_t count = 1)const; // ������ ����, ���������� ����� ��� ��������� �����
	_bitset to_bitset_reverse(size_t index, size_t count = 1)const;

	void clear();

	bool get(size_t index)const;
	bool operator[](size_t index);
	bool set(size_t index, bool new_value);

	size_t size_bits()const;
	size_t size_bytes()const;
	
	_byte* data();
	_byte* data_add_last(size_t count);

	void reverse();
};

struct Vertex
{
	Vector3 pos;
	Vector3 normals;
	Vector4 color;

	Vertex(const Vector3& p, const Vector3& n, const Vector4& cl) :pos(p), normals(n), color(cl) {}
	Vertex() :pos(), normals(), color() {}
};

struct DateTime // ����� � ����
{
	int seconds;
	int minutes;
	int hours;
	int days;
	int mounts;
	int years;

	DateTime(int s, int m, int h, int d, int mo, int y) :seconds(s), minutes(m), hours(h), days(d), mounts(mo), years(1900 + y) {}
	DateTime() :seconds(0), minutes(0), hours(0), days(0), mounts(0), years(1900) {}
};

struct BoundingBoxData // ������ ��� �������� BBox
{
	float xl, yd, zf; // ����, ��� � �����
	float xr, yu, zr; // �����, ���� � ���
	Vector center; // ����� ������

	BoundingBoxData(float xl, float yd, float zf, float xr, float yu, float zr) :xl(xl), yd(yd), zf(zf), xr(xr), yu(yu), zr(zr) {}
	BoundingBoxData() :xl(0), yd(0), zf(0), xr(0), yu(0), zr(0) {}
};

struct BlankModel
{
	vector<Index> indexs;
	vector<size_t> shiftIndexs;
	vector<Vertex> vertexs;
	vector<wstring> materials;
	wstring psoName;
	vector<wstring> texturesName;
	vector<bool> doubleSides;
	wstring nameModel;
	bool instansing = false;
	vector<any> instansingBuffer;
};

// General
enum TypeImage : _byte
{
	TI_RGB = 0,
	TI_RGBA
};

struct Image
{
	vector<_byte> imageData; // �������� ��� ���������� � ����� �����������
	size_t width;       // ������ ����������� 
	size_t height;      // ������ ����������� 

	struct _tga
	{
		size_t  bpp;        // �������� ���������� ��� �� �������     
		size_t bytesPerPixel; // ���� � �������
	} tgaInfo;

	struct _png
	{
		_byte depthBit; // ������� �������
		_byte compressedMethod; // ����� ������
		_byte filterMethod; // ����� ����������
		_byte interlaceMethod; // ����� ������������ (������� �������� ����)
		_bitset buffCompressedImage; // �������� ��� ���������� � ������ �����������
	} pngInfo;

	TypeImage type; // ���������� �������� � * ImageData (TI_RGB | TI_RGBA)
};

enum ModelConstants : wchar_t
{
	isColor = L'\n'
};

enum TypeRender : int
{
	D3D_RENDER = 0,
	OPENGL_RENDER
};

#define IS_COLOR wstring(1, ModelConstants::isColor)