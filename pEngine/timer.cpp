#include "timer.h"

void SystemTimer::clear()
{
	oldTime = newTime = allTime = 0;
}
SystemTimer::SystemTimer()
{
	clear();
}
void SystemTimer::start()
{
	clear();
}
void SystemTimer::setBeforeTime()
{
	oldTime = clock();
}
void SystemTimer::setAfterTime()
{
	newTime = clock();
	allTime = newTime - oldTime;
}
bool SystemTimer::isSecond() const
{
	return allTime >= 1000.f;
}
bool SystemTimer::isSecond_1_10() const
{
	return allTime >= 100.f;
}
bool SystemTimer::isSecond_1_100() const
{
	return allTime >= 10.f;
}
bool SystemTimer::isSecond_1_1000() const
{
	return allTime >= 1.f;
}
float SystemTimer::getCurrentTime()
{
	return allTime;
}

void FPSCounter::clear()
{
	timerFps.clear();
	frameCount = 0;
}
FPSCounter::FPSCounter()
{
	clear();
}
void FPSCounter::start()
{
	clear();
}
void FPSCounter::setBeforeTime()
{
	timerFps.setBeforeTime();
}
void FPSCounter::setAfterTime()
{
	timerFps.setAfterTime();
}
bool FPSCounter::isReady() const
{
	return timerFps.allTime >= 500.f;
}
size_t FPSCounter::getFps()
{
	size_t fps = 1000 * frameCount / timerFps.allTime;
	clear();
	return fps;
}
FPSCounter& FPSCounter::operator++()
{
	frameCount++;
	return *this;
}
