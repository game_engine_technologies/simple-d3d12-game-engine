#pragma once

#include "types.h"

struct cbufferCamera
{
	Matrix4 world;
	Matrix4 view;
	Matrix4 projection;

	cbufferCamera();
	cbufferCamera(const Matrix4& w, const Matrix4& v, const Matrix4& p);
};

 struct cbufferLight
{
	Matrix4 normals;
	Color colorDirectional;
	Vector directionalDirectional;
	Color colorPoint;
	Vector positionPoint;
	Vector cameraPosition;
	Vector directionalSpot;
	Vector positionSpot;
	Vector colorSpot;
	Vector propertySpot;

	cbufferLight();
	cbufferLight(const Color& c, const Vector& d, const Color& colp, const Vector& pp, const Vector& cp, const Matrix4& n,
		const Color& cs, const Vector& ds, const Vector& ps, const Vector& prop);
};

struct cbufferMaterial
{
	Vector ambient;
	Vector diffuse;
	Vector specular;
	float shininess;

	cbufferMaterial();
	cbufferMaterial(const Vector& a, const Vector& d, const Vector& s, const float& sn);
};

