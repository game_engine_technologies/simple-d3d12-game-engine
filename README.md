# Simple D3D9 game engine

- support load *.obj, pObj - models;
- textures;
- lights;
- fps - camera;
- sound.

## Demonstration

### [pseudo city] all model blinn-phong, sound (diffuse, ambient, specular) & (point, spot, directional) [d3d12]
[![pseudo city all model blinn-phong, sound (diffuse, ambient, specular) & (point, spot, directional) d3d12](demonstration/[pseudo_city]_all_model_blinn-phong_sound_(diffuse_ambient_specular)_&_(point_spot_directional)_[d3d12].gif)](https://youtu.be/0KfKs_Tjek8)

### all model blinn-phong (diffuse, ambient, specular) & (point, spot, directional) [d3d12]
[![all model blinn-phong (diffuse, ambient, specular) & (point, spot, directional) d3d12](demonstration/all_model_blinn-phong_(diffuse_ambient_specular)_&_(point_spot_directional)_[d3d12].gif)](https://youtu.be/syl7iwHeBJ8)

### blinn - phong light - point [d3d12]
[![blinn - phong light - point d3d12](demonstration/blinn_-_phong_light_-_point_[d3d12].gif)](https://youtu.be/hta1ROLX0WI)

### blinn - phong light - specular [d3d12]
[![blinn - phong light - specular d3d12](demonstration/blinn_-_phong_light_-_specular_[d3d12].gif)](https://youtu.be/yYrV7SGh5TY)

### phong light test - diffuse [d3d12]
[![phong light test - diffuse d3d12](demonstration/phong_light_test_-_diffuse_[d3d12].gif)](https://youtu.be/cbe9FqBr2es)

### test fps camera & sound [d3d12]
[![test fps camera & sound d3d12](demonstration/test_fps_camera_&_sound_[d3d12].gif)](https://youtu.be/oza_oyMErBc)

### textures [d3d12]
[![textures d3d12](demonstration/textures_[d3d12].gif)](https://youtu.be/-mt1_kWgrzw)
