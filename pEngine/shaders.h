#pragma once

#include <string>

#include "utils.h"

using namespace std;

class DYNLIB_PENGINE TypeShaders
{
public:
	static wstring vs;
	static wstring ps;
};

class DYNLIB_PENGINE PairShaders
{
	wstring pShader;

protected:
	wstring vShader;

public:
	PairShaders();
	PairShaders(const wstring& vs, const wstring& ps);
	wstring& getVertexShaderPath();
	virtual wstring& getPixelShaderPath();
};

class DYNLIB_PENGINE CombinedPairShaders : public PairShaders
{
public:
	CombinedPairShaders(const wstring& s);
	wstring& getPixelShaderPath() override;

};
