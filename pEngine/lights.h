#pragma once

#include "pch.h"
#include "utils.h"

class DYNLIB_PENGINE Light
{
protected:
	Color color;

public:
	Light();
	Light(const Color& c);
	Color& getColor();
	void setColor(const Color& c);
};

class DYNLIB_PENGINE LightDirection : public Light
{
	Vector dir;

public:
	LightDirection();
	LightDirection(const Color& c, const Vector& d);
	Vector& getDirection();
	void setDirection(const Vector& d);
};

class DYNLIB_PENGINE LightPoint : public Light
{
	Vector pos;

public:
	LightPoint();
	LightPoint(const Color& c, const Vector& p);
	Vector& getPosition();
	void setPosition(const Vector& p);
};

class DYNLIB_PENGINE LightSpot : public Light
{
	Vector pos;
	Vector dir;
	float cutOff;
	float outerCutOff;

public:
	LightSpot();
	LightSpot(const Color& c, const Vector& p, const Vector& d, float coff, float outerCutOff);
	Vector& getPosition();
	void setPosition(const Vector& p);
	Vector& getDirection();
	void setDirection(const Vector& d);
	float& getCutOff();
	void setCutOff(float coff);
	float& getOuterCutOff();
	void setOuterCutOff(float outerCutOff);
};

