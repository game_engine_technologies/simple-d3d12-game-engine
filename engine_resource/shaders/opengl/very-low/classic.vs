#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 color;

out vec4 outPosWorld;
out vec3 outNormal;
out vec4 outColor;

struct cbCamera
{
	mat4 world;
	mat4 view;
	mat4 perspective;
};

uniform cbCamera camera;

void main()
{
	outPosWorld = vec4(position, 1.0);
	gl_Position = vec4(position, 1.0) * camera.world * camera.view * camera.perspective;
	outNormal = normal;
	outColor = color;
};