#include "sBufferTypes.h"

sbufferInstancing::sbufferInstancing() :world(Matrix4::Identity()), normals(Matrix4::Identity()) {}
sbufferInstancing::sbufferInstancing(const Matrix4& w, const Matrix4& n) :world(w), normals(n) {}