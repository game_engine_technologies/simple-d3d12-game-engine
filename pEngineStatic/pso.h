#pragma once

#include <map>

#include "types.h"

using namespace std;

using PsoInputPair = pair< TypeRender, size_t>; // �������� ����������?

class PsoInputAssemblerDataCreator
{
	static map<PsoInputPair, size_t> countBytes; // ���������� ������ ��� ��������� �������� �� �������( ����� �������, ���������, ������� � �.�.)
	
	static map<PsoInputPair, size_t> createMap()
	{
		return
		{
			{{D3D_RENDER, 3}, 6 }, // DXGI_FORMAT
			{{D3D_RENDER, 4}, 2 }, // DXGI_FORMAT
			{{OPENGL_RENDER, 3}, 3 },
			{{OPENGL_RENDER, 4}, 4 }
		};
	}

public:
	static size_t getData(const PsoInputPair& pip)
	{
		size_t psb;
		if (countBytes.find(pip) == countBytes.end())
		{
			// error!
			exit(1);
		}
		psb = countBytes[pip];
		return psb;
	}

};

map<PsoInputPair, size_t> PsoInputAssemblerDataCreator::countBytes(createMap());

