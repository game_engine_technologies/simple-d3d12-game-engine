#include "utils.h"

std::wistream& operator>>(std::wistream& is, LineUnicode& l)
{
	std::getline(is, l, L'\n');
	return is;
}

std::istream& operator>>(std::istream& is, LineAscii& l)
{
	std::getline(is, l, '\n');
	return is;
}

