#include "types.h"

const size_t _bitset::countBitsInByte = 8;

void _bitset::clear()
{
	buffer.clear();
	countBits = 0;
}

auto _bitset::begin() -> iterator
{
	return  iterator(this);
}

auto _bitset::end() -> iterator
{
	return iterator(countBits);
}

auto _bitset::cbegin()const -> const_iterator
{
	return  const_iterator(this);
}

auto _bitset::cend()const -> const_iterator
{
	return const_iterator(countBits);
}

auto _bitset::rbegin() -> riterator
{
	return  riterator(this, countBits-1);
}

auto _bitset::rend() -> riterator
{
	return riterator(size_t(-1));
}

auto _bitset::rcbegin()const -> const_riterator
{
	return  const_riterator(this, countBits - 1);
}

auto _bitset::rcend()const -> const_riterator
{
	return const_riterator(size_t(-1));
}

_bitset::_bitset():countBits(0)
{
}

_bitset::_bitset(size_t size):countBits(size)
{
	int s(-1);
	if (countBits < countBitsInByte)
		s = countBits == 0 ? 0 : 1;
	else
		s = countBits / countBitsInByte;
	buffer.resize(s);
}

_bitset::_bitset(const _byte& b)
{
	buffer.push_back(b);
	countBits = countBitsInByte;
}

_bitset _bitset::operator=(const _byte& b)
{
	this->clear();
	buffer.push_back(b);
	countBits = countBitsInByte;
	return *this;
}

void _bitset::add(const _byte& b, size_t count_add)
{
	if (countBits % countBitsInByte == 0)
	{
		buffer.push_back(b);
		countBits += count_add;
	}
	else
	{
		_bitset tmp(b);
		for (size_t i(0); i < tmp.size_bits(); ++i)
			push_back_bit(tmp[i]);
	}
}

_byte _bitset::reverseByte(const _byte& elem)
{
	_byte number(elem);
	number = (number & 0x55) << 1 | (number & 0xAA) >> 1;
	number = (number & 0x33) << 2 | (number & 0xCC) >> 2;
	number = (number & 0x0F) << 4 | (number & 0xF0) >> 4;
	return number;
}

void _bitset::push_back_byte(const _byte& b)
{
	this->add(b, countBitsInByte);
}

void _bitset::push_back_bit(const bool& b)
{
	int index(-1);
	if (countBits % countBitsInByte == 0)
	{
		this->add(0, 1);
		index = countBits - 1;
	}
	else
		index = countBits++;
	set(index, b);
}

//string _bitset::to_string()
//{
//	string out;
//	int _countBits(countBits - 1);
//	auto constructNumber = [](string& out, _byte b, int& _countBits) -> bool
//	{
//		for (int i(countBitsInByte - 1); i >= 0; i--)
//		{
//			out += std::to_string(static_cast<bool>(b & (1 << i)));
//			if (--_countBits < 0)
//				return false;
//		}
//		out += " ";
//	};
//	for (auto&& b : buffer)
//		if (!constructNumber(out, b, _countBits))
//			break;
//	return out;
//}

string _bitset::to_string() const
{
	string out;
	int _countBits(countBits - 1);
	auto constructNumber = [](string& out, _byte b, int& _countBits) -> bool
	{
		for (int i(countBitsInByte - 1); i >= 0; i--)
		{
			out += std::to_string(static_cast<bool>(b & (1 << i)));
			if (--_countBits < 0)
				return false;
		}
		out += " ";
	};
	for (auto&& b : buffer)
		if (!constructNumber(out, b, _countBits))
			break;
	return out;
}

string _bitset::to_string(size_t index, size_t count)const
{
	//assert(count >= 1 && count <= 64);
	assert(index >= 0 && index < buffer.size() * countBitsInByte);
	if (index + count >= buffer.size() * countBitsInByte)
		count = (buffer.size() * countBitsInByte) - index;

	string outString;
	int startIndexByte = index / countBitsInByte;
	//int endIndexByte = (index + count) / countBitsInByte;
	//int startIndexBit = index;
	//int endIndexBit = index + count;
	int indexDigitNumber = count - 1;
	int currentIndexBit = countBitsInByte - (index % countBitsInByte) - 1;
	auto constructNumber = [](_byte b, int currentIndexBit, int& indexDigitNumber, string& outString) -> bool
	{
		for (; currentIndexBit >= 0; --currentIndexBit)
		{
			outString += (b & (1 << currentIndexBit)) ? "1" : "0";
			if (--indexDigitNumber < 0)
				return false;
		}
		outString += " ";
		return true;
	};
	while (true)
	{
		_byte b = buffer[startIndexByte++];
		if (!constructNumber(b, currentIndexBit, indexDigitNumber, outString))
			break;
		currentIndexBit = countBitsInByte - 1;
	}
	return outString;
}

auto _bitset::to_number(size_t index, size_t count)const -> ullong
{
	assert(count >= 1 && count <= 64);
	assert(index >= 0 && index < buffer.size() * countBitsInByte);
	if (index + count >= buffer.size() * countBitsInByte)
		count = (buffer.size() * countBitsInByte) - index;

	int outNumber = 0;
	int startIndexByte = index / countBitsInByte;
	//int endIndexByte = (index + count) / countBitsInByte;
	//int startIndexBit = index;
	//int endIndexBit = index + count;
	int indexDigitNumber = count - 1;
	int currentIndexBit = countBitsInByte - (index % countBitsInByte) - 1;
	auto constructNumber = [](_byte b, int currentIndexBit, int& indexDigitNumber, int& outNumber) -> bool
	{
		for (; currentIndexBit >= 0; --currentIndexBit)
		{
			if (b & (1 << currentIndexBit)) // ���� ��� � ����� == 1
				outNumber ^= (1 << indexDigitNumber); // �� � �������� ����� ����������� ������ ��� �� 1, ����� �������� �� ��������� (���� 0)
			/*else
				outNumber &= ~(1 << indexDigitNumber);*/
			if (--indexDigitNumber < 0)
				return false;
		}
		return true;
	};
	while (true)
	{
		_byte b = buffer[startIndexByte++];
		if (!constructNumber(b, currentIndexBit, indexDigitNumber, outNumber))
			break;
		currentIndexBit = countBitsInByte - 1;
	}
	return outNumber;
}

_bitset _bitset::to_bitset_reverse(size_t i, size_t count)const
{
	size_t index(i);
	_bitset out;
	assert(count >= 1 && count <= 64);
	assert(index >= 0 && index < buffer.size() * countBitsInByte);
	if (index + count >= buffer.size() * countBitsInByte)
		count = (buffer.size() * countBitsInByte) - index;
	index += count;
	for (int j(0); j < count; j++)
		out.push_back_bit(get(--index));
	return out;
}


bool _bitset::get(size_t index)const
{
	assert(index >= 0 && index < buffer.size() * countBitsInByte);
	int indexByte = index / countBitsInByte;
	int indexBit = countBitsInByte - (index % countBitsInByte) - 1;
	return buffer[indexByte] & (1 << indexBit);
}

bool _bitset::operator[](size_t index)
{
	return get(index);
}

bool _bitset::set(size_t index, bool new_value)
{
	assert(index >= 0 && index < buffer.size() * countBitsInByte);
	int indexByte = index / countBitsInByte;
	int indexBit = countBitsInByte - (index % countBitsInByte) - 1;
	return new_value ? buffer[indexByte] |= (1 << indexBit) : buffer[indexByte] &= ~(1 << indexBit);
}

size_t _bitset::size_bits()const
{
	return countBits;
}

size_t _bitset::size_bytes()const
{
	return buffer.size();
}

_byte* _bitset::data()
{
	return buffer.data();
}

_byte* _bitset::data_add_last(size_t count)
{
	size_t prew_size = buffer.size();
	buffer.resize(buffer.size() + count);
	countBits += countBitsInByte * count;
	return buffer.data() + prew_size;
}

void _bitset::reverse()
{
	for (auto&& b : buffer)
		b = reverseByte(b);
}
