#include "sun.h"

bool Sun::isStop()
{
	if (currentWait == 0)
		return true;
	if (updater.isSecond())
	{
		--currentWait;
		updater.setBeforeTime();
	}
	return false;
}
void Sun::init(const Color& c, const Vector& p, int w)
{
	position = p;
	center = { 0,0,0,0 };
	light = LightDirection(c, Vector::createVectorForDoublePoints(position, center));
	rotate = Matrix::Identity();
	updater.start();
	updater.setBeforeTime();
	wait = w;
	currentWait = 0;
}
Matrix Sun::update(bool& status)
{
	status = false;
	Matrix newPosSun = Matrix::Identity();
	updater.setAfterTime();
	if (!isStop())
	{
		return Matrix::Identity();
	}
	if (updater.isSecond_1_100())
	{
		updater.setBeforeTime();
		angle += 0.2f;
		if (angle > 180)
		{
			angle = 0;
			currentWait = wait;
			//return; // ������������������, ����� ������ �������� � �����
		}
		rotate = Matrix::CreateRotateMatrixZ(GeneralMath::degreesToRadians(-angle));
		newPosSun = Matrix::CreateTranslationMatrixXYZ(position[vx], position[vy], position[vz]) * rotate;
		Vector v = position * rotate;
		light.setDirection(Vector::createVectorForDoublePoints(v, center));
		status = true;
	}
	return newPosSun;
}
LightDirection& Sun::getLightSun() { return light; }
void Sun::setModel(const wstring& s) { modelSun = s; }
wstring& Sun::getNameModel() { return modelSun; }
Vector& Sun::getStartPosition() { return position; }