#include "cBufferTypes.h"

cbufferCamera::cbufferCamera() :world(Matrix4::Identity()), view(Matrix4::Identity()), projection(Matrix4::Identity()) {}
cbufferCamera::cbufferCamera(const Matrix4& w, const Matrix4& v, const Matrix4& p) :world(w), view(v), projection(p) {}

cbufferLight::cbufferLight() :colorDirectional(), directionalDirectional(), colorPoint(), positionPoint(), cameraPosition(), normals(Matrix4::Identity()), colorSpot(),
directionalSpot(), positionSpot(), propertySpot() {}
cbufferLight::cbufferLight(const Color& c, const Vector& d, const Color& colp, const Vector& pp, const Vector& cp, const Matrix4& n,
	const Color& cs, const Vector& ds, const Vector& ps, const Vector& prop) :colorDirectional(c), directionalDirectional(d),
	colorPoint(colp), positionPoint(pp), cameraPosition(cp), normals(n), colorSpot(cs), directionalSpot(ds), positionSpot(ps), propertySpot(prop) {}

cbufferMaterial::cbufferMaterial() :ambient(), diffuse(), specular(), shininess(0.f) {}
cbufferMaterial::cbufferMaterial(const Vector& a, const Vector& d, const Vector& s, const float& sn) :ambient(a), diffuse(d), specular(s), shininess(sn) {}