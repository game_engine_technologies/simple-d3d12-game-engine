#pragma once

#include "types.h"

class Texture
{
	GLuint texture;
	static GLuint globalTextureID;

public:
	bool loadTexture(const wstring& path);
	GLuint& getTexture();
};

class ContainerTextures
{
	map<wstring, Texture> textures;
	size_t count;

public:
	ContainerTextures();

	bool addTexture(const wstring& filename);

	bool getTexture(const wstring& filename, Texture** texture);

	size_t& getSize();

	map<wstring, Texture>& getTexturesBuffer();
};

