#include "Calculate.h"

std::pair<Point, Point> Computer::calculateRay(int _x, int _y, const RectF& vp, const Matrix& matrixProjection, const Matrix& cameraPos)
{
	Vector vectorLine = matrixProjection[0];
	float px = (((2.f * _x) / vp.width) - 1.f) / vectorLine[vx];
	vectorLine = matrixProjection[1];
	float py = (((-2.f * _y) / vp.height) + 1.f) / vectorLine[vy];

	Vector rayOr(0.f, 0.f, 0.f, 0.f);
	Vector rayDirection(px, py, 1.f, 0.f);

	Matrix notview(Matrix::Inverse(cameraPos));
	rayOr.vector4TransformCoord(notview);
	rayDirection.vector4TransformNormal(notview);
	rayDirection.normalize();

	return { (Point)rayOr, (Point)rayDirection };
}

float Computer::intersectionRayAndPlane(const Point& point, const Point& normal, const std::pair<Point, Point>& ray)
{
	return -Point::dot(ray.first - point, normal) / Point::dot(ray.second, normal);
}

Point Computer::getPointIntersectionRayAndPlane(const std::pair<Point, Point>& ray, float t)
{
	return ray.first + (Point)(t * ray.second); // �������� ����� �����������
}

bool Computer::belongsPointInTriangle(const Point& a, const Point& b, const Point& c, const Point& m, const Point& n)
{
	Vector p(n);
	Vector p0(a);
	Vector p1(b);
	Vector p2(c);

	Vector o(m); // ����������� �����
	Vector tmp;

	// ������ ��� �������
	Vector oa(p0 - o);
	Vector ob(p1 - o);
	tmp = p * Vector4::cross(oa, ob);
	float v1(tmp[vx]);

	// ������ ��� �������
	Vector oc(p2 - o);
	tmp = p * Vector4::cross(ob, oc);
	float v2(tmp[vx]);

	// ������ ��� �������
	tmp = p * Vector4::cross(oc, oa);
	float v3(tmp[vx]);

	// ������ ��������� �����
	if ((v1 < 0 && v2 < 0 && v3 < 0) || (v1 >= 0 && v2 >= 0 && v3 >= 0))
		return true;
	return false;
}

Point Computer::generateNormal(const Point& a, const Point& b, const Point& c)
{
	Vector p0(a);
	Vector p1(b);
	Vector p2(c);
	Vector u = p1 - p0;
	Vector v = p2 - p0;
	Vector p = Vector::cross(u, v);
	p = Vector::Normalize(p);
	return (Point)p;
}

