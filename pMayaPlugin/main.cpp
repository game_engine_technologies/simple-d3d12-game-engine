#include "progressivePlugin.h"

static MString nameLoadPluginScript("pEngineLoadPlugin.mel");

MStatus initializePlugin(MObject obj)
{
	// load scripts pEngineTools
	MString command;
	CHECK_MSTATUS(MGlobal::executeCommand("internalVar-usd", command));
	command += "pEnginePlugin/" + nameLoadPluginScript + "\"";
	command = "source \"" + command;
	CHECK_MSTATUS(MGlobal::executeCommand(command));
	CHECK_MSTATUS(MGlobal::executeCommand("pEngineLoadPlugin();"));

	// load plugin pEngine
	pObjectPlugin::initProperties();
	if (!pObjectPlugin::isInit)
		return MStatus::kFailure;
	MFnPlugin plugin(obj, pObjectPlugin::props["$vendor_plugin"].c_str(), pObjectPlugin::props["$version_plugin"].c_str(), "Any"); // �������������� ������: ������, ��������� �� Maya, ��� ����������, ������, ������ API maya
	
	// ������������ ���������� ������ �����
	MStatus result = plugin.registerFileTranslator(pObjectPlugin::props["$name_plugin"].c_str(),  // ��� ����� ������������ � ����� �������
		"none", // ���� � ������
		pObjectPlugin::creator, // �������, ������� ������������ ����� ����
		NULL, // mel - ������, ������� ���������� ����� ��������� ���������� ����� � ��������� ���� ����������
		pObjectPlugin::optionScriptDefaultValues); // �������� ���������� �� - ���������, ������� ����� �������� ����������� ����            
	if (result != MStatus::kSuccess)
		return result;

	// ������������ ����� ��������
	return pEngineMaterial::initMaterial(plugin, pObjectPlugin::props["$game_path"]);
}
//////////////////////////////////////////////////////////////

MStatus uninitializePlugin(MObject obj)
{
	MString command;
	CHECK_MSTATUS(MGlobal::executeCommand("internalVar-usd", command));
	command += "pEnginePlugin/" + nameLoadPluginScript + "\"";
	command = "source \"" + command;
	CHECK_MSTATUS(MGlobal::executeCommand(command));
	CHECK_MSTATUS(MGlobal::executeCommand("pEngineUnloadGui();"));

	MFnPlugin plugin(obj);
	MStatus result = plugin.deregisterFileTranslator(pObjectPlugin::props["$name_plugin"].c_str());
	if (result != MStatus::kSuccess)
		return result;

	return pEngineMaterial::unInitMaterial(plugin);
}