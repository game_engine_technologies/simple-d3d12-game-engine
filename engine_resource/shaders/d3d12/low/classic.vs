cbuffer cbCamera: register(b0)
{ 
	matrix world;
	matrix view; 
	matrix perspective; 
};

struct VS_INPUT
{
    float3 pos : POSITION;
	float3 normal : NORMAL;
    float4 color: COLOR;
};

struct VS_OUTPUT
{
    float4 pos: SV_POSITION;
	float4 posWorld: POSITION;
	float3 normal : TEXCOORD0;
    float4 color: COLOR;
};

VS_OUTPUT main(VS_INPUT input)
{
	VS_OUTPUT _out = (VS_OUTPUT)0;
	_out.pos = mul(float4(input.pos, 1), world);
	_out.posWorld = mul(float4(input.pos, 1), world);
    _out.pos = mul(_out.pos, view); 
    _out.pos = mul(_out.pos, perspective);
    _out.color = input.color;
	_out.normal = input.normal;
    return _out;
}