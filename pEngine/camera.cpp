#include "camera.h"

CameraProperties::CameraProperties() :moveLeftRight(0.f), moveBackForward(0.f), camYaw(0.f), camPitch(0.f)
{
	lastPosMouse = { 0, 0 };
	mouseCurrState = { 0,0 };
}

Camera::Camera(const Vector3& p, const Vector3& d)
{
}
Camera::Camera(const Matrix4& v, const Matrix4& p) :view(v), projection(p) {}
void Camera::updateView(const Vector3& p, const Vector3& d, const Vector3& h) { view = _view(p, d, h); }
void Camera::updateProjection(float fov, float aspect, float zn, float zf) { projection = _projection(fov, aspect, zn, zf); }
void Camera::setRenderFunctions(const TypeRender& tr)
{
	switch (tr)
	{
	case OPENGL_RENDER:
	{
		_view = static_cast<Matrix(*)(const Vector3&, const Vector3&, const Vector3&)>(&Matrix::CreateLookAtRHMatrix);
		_projection = static_cast<Matrix(*)(const float&, const float&, const float&, const float&)>(&Matrix::CreatePerspectiveFovRHMatrix);
		break;
	}

	case D3D_RENDER:
	{
		_view = static_cast<Matrix(*)(const Vector3&, const Vector3&, const Vector3&)>(&Matrix::CreateLookAtLHMatrix);
		_projection = static_cast<Matrix(*)(const float&, const float&, const float&, const float&)>(&Matrix::CreatePerspectiveFovLHMatrix);
		break;
	}
	}
}
MatrixCPUAndGPU& Camera::getView() { return view; }
MatrixCPUAndGPU& Camera::getProjection() { return projection; }
Vector3& Camera::getPosition() { return camPosition; }
Vector3& Camera::getUp() { return camUp; }
Vector3& Camera::getTarget() { return camTarget; }
Vector3& Camera::getForward() { return camForward; }
Vector3& Camera::getRight() { return camRight; }
Vector3 Camera::hCamStandart(0.0f, 1.0f, 0.0f);

StaticCamera::StaticCamera(const Vector3& p, const Vector3& d) :Camera(p, d) {}
void StaticCamera::setRenderFunctions(const TypeRender& tr) 
{
	Camera::setRenderFunctions(tr); 
}
void StaticCamera::updateCamera(map<char, bool>& keys, const Point2& mDelta, double time) {}
float StaticCamera::getSpeed(float time, bool isX) {return 0;}

bool GameCamera::_move_opengl(map<char, bool>& keyStatus, double time)
{
	float speedRun = getSpeed(time, keyStatus['X']);
	float speed = getSpeed(time, false);
	size_t countSteps(0);

	if (keyStatus['A'])
	{
		currentProperties.moveLeftRight += speed;
		countSteps++;
	}
	if (keyStatus['D'])
	{
		currentProperties.moveLeftRight -= speed;
		countSteps++;
	}
	if (keyStatus['W'])
	{
		currentProperties.moveBackForward += speedRun;
		countSteps++;
	}
	if (keyStatus['S'])
	{
		currentProperties.moveBackForward -= speed;
		countSteps++;
	}
	if (countSteps > 0)
		return true;
	return false;
}
bool GameCamera::_move_d3d(map<char, bool>& keyStatus, double time)
{
	float speedRun = getSpeed(time, keyStatus['X']);
	float speed = getSpeed(time, false);
	size_t countSteps(0);

	if (keyStatus['A'])
	{
		currentProperties.moveLeftRight -= speed;
		countSteps++;
	}
	if (keyStatus['D'])
	{
		currentProperties.moveLeftRight += speed;
		countSteps++;
	}
	if (keyStatus['W'])
	{
		currentProperties.moveBackForward += speedRun;
		countSteps++;
	}
	if (keyStatus['S'])
	{
		currentProperties.moveBackForward -= speed;
		countSteps++;
	}
	if (countSteps > 0)
		return true;
	return false;
}
bool GameCamera:: _rotate_opengl(const Point2& mDelta)
{
	if ((mDelta[vx] != 0) || (mDelta[vy] != 0))
	{
		currentProperties.camYaw -= (mDelta[vx] * 0.001f); // todo? 
		currentProperties.camPitch += (mDelta[vy] * 0.001f);
		currentProperties.lastPosMouse = currentProperties.mouseCurrState;
		return true;
	}
	return false;
}
bool GameCamera:: _rotate_d3d(const Point2& mDelta)
{
	if ((mDelta[vx] != 0) || (mDelta[vy] != 0))
	{
		currentProperties.camYaw += (mDelta[vx] * 0.001f); // todo? 
		currentProperties.camPitch += (mDelta[vy] * 0.001f);
		currentProperties.lastPosMouse = currentProperties.mouseCurrState;
		return true;
	}
	return false;
}
GameCamera::GameCamera(float step, float run, const Vector3& p, const Vector3& d) : _step(step), _run(run)
{
	currentProperties = {};
	DefaultForward = Vector3(0.0f, 0.0f, 1.0f);
	DefaultRight = Vector3(1.0f, 0.0f, 0.0f);
	camForward = Vector3(0.0f, 0.0f, 1.0f);
	camRight = Vector3(1.0f, 0.0f, 0.0f);
	camPosition = p;
	camTarget = d;
	camUp = hCamStandart;
}
void GameCamera::setRenderFunctions(const TypeRender& tr)
{
	Camera::setRenderFunctions(tr);
	switch (tr)
	{
	case OPENGL_RENDER:
	{
		_move = bind(&GameCamera::_move_opengl, this, _1, _2);
		_rotate = bind(&GameCamera::_rotate_opengl, this, _1);
		break;
	}

	case D3D_RENDER:
	{
		_move = bind(&GameCamera::_move_d3d, this, _1, _2);
		_rotate = bind(&GameCamera::_rotate_d3d, this, _1);
		break;
	}
	}
}
void GameCamera::updateCamera(map<char, bool>& keyStatus, const Point2& mDelta, double time)
{
	bool moveStatus = _move(keyStatus, time);
	bool rotateStatus = _rotate(mDelta);
	if (!moveStatus && !rotateStatus)
		return;

	float angle = 1.483f; // 85 градусов
	if (currentProperties.camPitch > 0)
		currentProperties.camPitch = min(currentProperties.camPitch, angle);
	else
		currentProperties.camPitch = max(currentProperties.camPitch, -angle);

	Matrix4 camRotationMatrix = Matrix4::CreateRotateMatrixXYZ(currentProperties.camPitch, currentProperties.camYaw, 0);
	camTarget = DefaultForward;
	camTarget.vector3TransformCoord(camRotationMatrix);
	camTarget.normalize();

	Matrix4 RotateYTempMatrix = Matrix4::CreateRotateMatrixY(currentProperties.camYaw);
	camRight = DefaultRight;
	camRight.vector3TransformCoord(RotateYTempMatrix);
	camUp.vector3TransformCoord(RotateYTempMatrix);
	camForward = DefaultForward;
	camForward.vector3TransformCoord(RotateYTempMatrix);

	camPosition += camRight * currentProperties.moveLeftRight;
	camPosition += camForward * currentProperties.moveBackForward;

	camTarget = camPosition + camTarget;

	updateView(camPosition, camTarget, camUp);

	currentProperties.moveLeftRight = currentProperties.moveBackForward = 0.f;
}
float GameCamera::getSpeed(float time, bool isX)
{
	if (isX)
		return _run * time;
	return _step * time;
}