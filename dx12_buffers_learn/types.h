#pragma once

#include <windows.h>
#include <d3d12.h>
#include <dxgi1_4.h>
#include <D3Dcompiler.h>
#include "d3dx12.h"
#include <wrl.h>
#include "DDSTextureLoader.h"

using Microsoft::WRL::ComPtr;

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "D3Dcompiler.lib")

#include "../pEngine/pch.h"
#include "../pEngine/types.h"

enum Sizes
{
	featureLevelsSize = 4
};


