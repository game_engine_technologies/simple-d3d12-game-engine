#include "lights.h"

Light::Light() :color() {}
Light::Light(const Color& c) :color(c) {}
Color& Light::getColor() { return color; }
void Light::setColor(const Color& c) { color = c; }

LightDirection::LightDirection() :Light(), dir() {}
LightDirection::LightDirection(const Color& c, const Vector& d) :Light(c), dir(d) {}
Vector& LightDirection::getDirection() { return dir; }
void LightDirection::setDirection(const Vector& d) { dir = d; }

LightPoint::LightPoint() : Light(), pos() {}
LightPoint::LightPoint(const Color& c, const Vector& p) :Light(c), pos(p) {}
Vector& LightPoint::getPosition() { return pos; }
void LightPoint::setPosition(const Vector& p) { pos = p; }

LightSpot::LightSpot() : Light(), pos(), dir(), cutOff(0.f), outerCutOff(0.f) {}
LightSpot::LightSpot(const Color& c, const Vector& p, const Vector& d, float coff, float outerCutOff) :Light(c), pos(p), dir(d), cutOff(coff), outerCutOff(outerCutOff) {}
Vector& LightSpot::getPosition() { return pos; }
void LightSpot::setPosition(const Vector& p) { pos = p; }
Vector& LightSpot::getDirection() { return dir; }
void LightSpot::setDirection(const Vector& d) { dir = d; }
float& LightSpot::getCutOff() { return cutOff; }
void LightSpot::setCutOff(float coff) { cutOff = coff; }
float& LightSpot::getOuterCutOff() { return outerCutOff; }
void LightSpot::setOuterCutOff(float outerCutOff) { this->outerCutOff = outerCutOff; }