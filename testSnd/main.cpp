#include "Framework.h"

#include <algorithm>
#include <memory>
#include <array>
#include <fstream>
#include <experimental/filesystem>
#include <map>
#include <iostream>

namespace fs = std::experimental::filesystem;

#include "../math_graphics/math_graphics/mathematics.h"
using namespace gmath;

#include "../pSound/sound.h"

using namespace std;

#define	TEST_WAVE_FILE		"rnd_dog3.wav"


//enum SoundType : int
//{
//	STATIC_SOUND = 0,
//	ACTOR_SOUND
//};
//
//class Sound;
//class Listener;
//
//class SoundDevice
//{
//	std::multimap< ALuint, Sound> staticSound;
//	std::multimap< ALuint, Sound> actorSound;
//	std::map< std::wstring, ALuint> soundBuffer;
//	std::shared_ptr<Listener> listener;
//
//	void destroy();
//public:
//	bool init(const Vector3& p, const Vector3& d, const Vector3& h);
//	bool addSound(const std::wstring& filename, const Vector3& p, const SoundType& type, bool looped, bool streamed);
//	void update(const Vector3& p, const Vector3& d, const Vector3& h);
//	void setVolume(float v);
//};
//
//
//class Listener
//{
//	std::vector<ALfloat> camPos;
//	std::vector<ALfloat> camSpeed;
//	std::vector<ALfloat> camOrient;
//public:
//
//	bool init(const Vector3& p, const Vector3& d, const Vector3& h);
//	void updatePosition(const Vector3& p, const Vector3& d, const Vector3& h);
//	void setVolume(float v);
//
//	~Listener()
//	{
//		//OutputDebugString("delete listener");
//	}
//};
//
//
//class SoundLoader
//{
//public:
//	virtual bool loadFile(const std::wstring& filename, ALuint& buffer) = 0;
//};
//
//class WavLoader : public SoundLoader
//{
//public:
//	bool loadFile(const std::wstring& filename, ALuint& buffer) override;
//};
//
//
//class Sound
//{
//	std::vector<ALfloat> soundPos; // ������� �����
//	std::vector<ALfloat> soundSpeed; // �������� �����
//	bool  mLooped;
//	ALuint mSourceID;	// ������������� ���������
//	bool mStreamed; // ��������� �� ����
//
//
//public:
//	Sound(const Vector3& p, ALuint& buffer, bool looped, bool streamed);
//	void init(const Vector3& p, ALuint& buffer, bool looped, bool streamed);
//
//	bool isStreamed();
//	void play();
//	void update();
//	void update_debug();
//	void close();
//	void move(const Vector3& p);
//	void stop();
//
//};
//



//int main()
//{
//	ALuint      uiBuffer;
//	ALuint      uiSource;
//	ALint       iState;
//
//	// Initialize Framework
//	ALFWInit();
//
//	ALFWprintf("PlayStatic Test Application\n");
//
//	if (!ALFWInitOpenAL())
//	{
//		ALFWprintf("Failed to initialize OpenAL\n");
//		ALFWShutdown();
//		return 0;
//	}
//
//	ALfloat p [] = { 0,0,0 };
//	alListenerfv(AL_POSITION, p);
//	// ��������
//	ALfloat s[] = { 0,0,0 };
//	alListenerfv(AL_VELOCITY, s);
//	// ����������
//	ALfloat t[] = { 0,0,0,0,1,0 };
//	alListenerfv(AL_ORIENTATION, t);
//
//	// Generate an AL Buffer
//	alGenBuffers(1, &uiBuffer);
//
//	// Load Wave file into OpenAL Buffer
//	if (!ALFWLoadWaveToBuffer(TEST_WAVE_FILE, uiBuffer))
//	{
//		ALFWprintf("Failed to load %s\n", TEST_WAVE_FILE);
//	}
//
//	// Generate a Source to playback the Buffer
//	alGenSources(1, &uiSource);
//
//	// Attach Source to Buffer
//	alSourcei(uiSource, AL_BUFFER, uiBuffer);
//
//	alSourcef(uiSource, AL_GAIN, 50.f); // ���������� ��� ���������
//	alListenerf(AL_GAIN, 5.f); // ��������� ����������
//	alSourcei(uiSource, AL_LOOPING, true); // �������� ��� ���
//
//	// Play Source
//	alSourcePlay(uiSource);
//	ALFWprintf("Playing Source ");
//
//	ALfloat ps[] = { 0,0,0 };
//	alSourcefv(uiSource, AL_POSITION, ps); // ������� ���������
//
//	do
//	{
//		Sleep(100);
//		//ps[2] --;
//		//alSourcefv(uiSource, AL_POSITION, ps); // ������� ���������
//		alGetSourcei(uiSource, AL_SOURCE_STATE, &iState); // ����������� ����� �����
//	} while (iState == AL_PLAYING);
//
//	ALFWprintf("\n");
//
//	// Clean up by deleting Source(s) and Buffer(s)
//	alSourceStop(uiSource);
//	alDeleteSources(1, &uiSource);
//	alDeleteBuffers(1, &uiBuffer);
//
//	ALFWShutdownOpenAL();
//
//	ALFWShutdown();
//
//	return 0;
//}



//int main()
//{
//	shared_ptr<SoundDevice> sndDevice;
//	sndDevice.reset(new SoundDevice);
//	if (!sndDevice->init({ 0,0,0 }, {0, 0, 0}, { 0,1,0 }))
//		return 1;
//	if (!sndDevice->addSound(L"rnd_night_1.wav", { 0.f, 0.f, 0.f }, STATIC_SOUND, true, false, 3))
//		return 1;
//
//	while (true)
//	{
//		//Sleep(10);
//	}
//
//
//	return 0;
//}


//void SoundDevice::destroy()
//{
//	for_each(soundBuffer.begin(), soundBuffer.end(), [](auto& p)
//		{
//			alDeleteBuffers(1, &p.second);
//		});
//	ALFWShutdownOpenAL();
//	ALFWShutdown();
//}
//
//bool SoundDevice::init(const Vector3& p, const Vector3& d, const Vector3& h)
//{
//	ALFWInit();
//	if (!ALFWInitOpenAL())
//	{
//		//destroy();
//		return false;
//	}	
//	listener.reset(new Listener);
//	if (!listener->init(p, d, h))
//		return false;
//
//	//std::vector<ALfloat> camPos = { p[vx], p[vy], p[vz] };
//	//std::vector<ALfloat> camSpeed = { 0,0,0 };
//	//std::vector<ALfloat> camOrient = { d[vx], d[vy], d[vz], h[vx], h[vy], h[vz] };
//
//	//camPos = { p[vx], p[vy], p[vz] };
//
//	//// �������
//	//alListenerfv(AL_POSITION, camPos.data());
//	//// ��������
//	//alListenerfv(AL_VELOCITY, camSpeed.data());
//	//// ����������
//	//alListenerfv(AL_ORIENTATION, camOrient.data());
//
//	//ALfloat p1[] = { 0,0,0 };
//	//alListenerfv(AL_POSITION, p1);
//	//// ��������
//	//ALfloat s[] = { 0,0,0 };
//	//alListenerfv(AL_VELOCITY, s);
//	//// ����������
//	//ALfloat t[] = { 0,0,0,0,1,0 };
//	//alListenerfv(AL_ORIENTATION, t);
//	return true;
//}
//
//bool SoundDevice::addSound(const std::wstring& filename, const Vector3& p, const SoundType& type, bool looped, bool streamed)
//{
//	// ��������� ���� �� �������
//	if (!fs::exists(filename))
//		return false;
//	ALuint mSourceID;
//	fs::path soundFile(filename);
//	std::string ext = soundFile.extension().string();
//	std::transform(ext.begin(), ext.end(), ext.begin(), toupper);
//	std::shared_ptr<SoundLoader> loader;
//	if (ext == ".WAV")
//		loader.reset(new WavLoader);
//	if (!loader->loadFile(filename, mSourceID))
//		return false;
//	if (soundBuffer.find(filename) == soundBuffer.end())
//		soundBuffer.insert({ filename, mSourceID });
//	switch (type)
//	{
//	case STATIC_SOUND:
//		staticSound.insert({ mSourceID , Sound(p, mSourceID , looped, streamed) });
//		break;
//
//	case ACTOR_SOUND:
//		actorSound.insert({ mSourceID , Sound(p, mSourceID , looped, streamed) });
//		break;
//	}
//	return true;
//}
//
//void SoundDevice::update(const Vector3& p, const Vector3& d, const Vector3& h)
//{
//	//listener->updatePosition(p, d, h);
//	staticSound.begin()->second.update_debug();
//	/*for_each(staticSound.begin(), staticSound.end(), [](auto& snd)
//		{
//			snd.second.update();
//		});
//	for_each(actorSound.begin(), actorSound.end(), [&p](auto& snd)
//		{
//			snd.second.move(p);
//			snd.second.update();
//		});*/
//}
//
//void SoundDevice::setVolume(float v)
//{
//	listener->setVolume(v);
//}
//
//
//bool Listener::init(const Vector3& p, const Vector3& d, const Vector3& h)
//{
//	// ������������� ������� ���������
//	fill(camSpeed.begin(), camSpeed.end(), 0.f);
//	updatePosition(p, d, h);
//	setVolume(5.f); // default volume
//	return true;
//}
//
//void Listener::updatePosition(const Vector3& p, const Vector3& d, const Vector3& h)
//{
//	camPos = { p[vx], p[vy], p[vz] };
//	camOrient = { d[vx], d[vy], d[vz], h[vx], h[vy], h[vz] };
//
//	// �������
//	alListenerfv(AL_POSITION, camPos.data());
//	// ��������
//	alListenerfv(AL_VELOCITY, camSpeed.data());
//	// ����������
//	alListenerfv(AL_ORIENTATION, camOrient.data());
//}
//
//void Listener::setVolume(float v)
//{
//	alListenerf(AL_GAIN, v); // ��������� ����������
//}
//
//bool WavLoader::loadFile(const std::wstring& filename, ALuint& buffer)
//{
//	alGenBuffers(1, &buffer);
//	std::string cstrFilename(filename.begin(), filename.end());
//	if (!ALFWLoadWaveToBuffer(cstrFilename.c_str(), buffer))
//		return false;
//	return true;
//}
//
//
//
//Sound::Sound(const Vector3& p, ALuint& buffer, bool looped, bool streamed)
//{
//	init(p, buffer, looped, streamed);
//}
//
//void Sound::init(const Vector3& p, ALuint& buffer, bool looped, bool streamed)
//{
//	alGenSources(1, &mSourceID);
//	alSourcei(mSourceID, AL_BUFFER, buffer);
//	fill(soundSpeed.begin(), soundSpeed.end(), 0.f);
//	move(p);
//	alSourcefv(mSourceID, AL_VELOCITY, soundSpeed.data()); // �������� �����?
//	mLooped = looped;
//	alSourcef(mSourceID, AL_PITCH, 1.0f); // ��� �����
//	alSourcef(mSourceID, AL_GAIN, 50.0f); // �������� ����� �� ���� ����������� � ���������
//	alSourcei(mSourceID, AL_LOOPING, mLooped); // �������� ��� ���
//	play();
//}
//
//void Sound::move(const Vector3& p)
//{
//	soundPos = { p[vx], p[vy], p[vz] };
//	alSourcefv(mSourceID, AL_POSITION, soundPos.data()); // ������� ���������
//}
//
//void Sound::play()
//{
//	alSourcePlay(mSourceID);
//}
//
//void Sound::update()
//{
//	ALint iState;
//	alGetSourcei(mSourceID, AL_SOURCE_STATE, &iState);
//	//if (iState != AL_PLAYING) // todo: ���������� ��� ����������� ������
//	//	stop();
//}
//
//void Sound::update_debug()
//{
//	ALint iState;
//	do
//	{
//		Sleep(100);
//		alGetSourcei(mSourceID, AL_SOURCE_STATE, &iState); // ����������� ����� �����
//	} while (iState == AL_PLAYING);
//}
//
//void Sound::close()
//{
//	alSourceStop(mSourceID);
//	if (alIsSource(mSourceID))
//		alDeleteSources(1, &mSourceID);
//}
//
//void Sound::stop()
//{
//	alSourceStop(mSourceID);
//}


struct node
{
	int a;
	size_t b;
};


template<size_t N>
void testStaticArray(const int(&a)[N])
{
	copy(a, a + N, ostream_iterator<int>(cout, " "));
	cout << endl;
}

void testStaticArrayTwo(const int(&a)[5])
{
	copy(a, a + 5, ostream_iterator<int>(cout, " "));
	cout << endl;
}

auto sum(int a, int b) 
{
	return a + b;
}

int main()
{
	size_t s(100);
	node* arr = new node[s];
	//for (size_t i(0); i < s; i++)
	//	arr[i] = new node[1]{ i, i + 5 };

	for (size_t i(0); i < s; i++)
	{
		new(&arr[i]) (node){ (int)i, i + 5 }; // ��������� - ��������� �������� �������� �������, ��� ������ �������� - 
		// ����������� ������� (&������[������]), � ������ �������� - ������������� �������� ((��� ��������)(��������)) 
	}

	for (size_t i(0); i < s; i++)
	{
		cout << arr[i].a << " & " << arr[i].b << endl;
	}
	cout << endl;

	delete arr;

	int arr1[] = { 48,56,2,65,6 };
	testStaticArray(arr1);
	testStaticArrayTwo(arr1);
	cout << sum(48, 2) << endl;

	return 0;
}