#pragma once

#if defined(DYNAMIC_LIBRARY_PENGINE)
#  define DYNLIB_PENGINE __declspec(dllexport)
#else
#  define DYNLIB_PENGINE __declspec(dllimport)
#endif

