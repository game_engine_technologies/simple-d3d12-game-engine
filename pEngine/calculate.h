#pragma once

#include <vector>
#include "types.h"

template< class T>
struct Rect
{
	T left;
	T top;
	T height;
	T width;

	Rect(const T& l, const T& r, const T& t, const T& b);
	Rect();
};

using RectF = Rect<float>;

template<class T>
inline Rect<T>::Rect(const T& l, const T& t, const T& h, const T& w) : left(l), top(t), height(h), width(w)
{
}
template<class T>
inline Rect<T>::Rect():left(), top(), height(), width()
{
}


class DYNLIB_PENGINE Computer
{
	template<typename T> static T _genereateAverageNormal(T p);
	template<typename T, typename... Vertexs> static T _genereateAverageNormal(T sum, Vertexs&... vert);
public:
	static std::pair<Point, Point> calculateRay(int _x, int _y, const RectF& vp, const Matrix& matrixProjection, const Matrix& cameraPos); // ���������� ��� �� ����� �� ����������� ������
	static float intersectionRayAndPlane(const Point& point, const Point& normal, const std::pair<Point, Point>& ray); // ���������� �� ��� ���������?
	static Point getPointIntersectionRayAndPlane(const std::pair<Point, Point>& ray, float t); // �������� ����� ����������� ���� � ���������
	static bool belongsPointInTriangle(const Point& a, const Point& b, const Point& c, const Point& m, const Point& n); // ����� �� ����� � ������������
	static Point generateNormal(const Point& a, const Point& b, const Point& c); // ������������� ����������� �������
	//template<class T> static void transformModel(std::vector<T>& v, const Matrix& m); // ����������������� ������
	template<typename T, typename... Vertexs> static T genereateAverageNormal(int c, T sum, Vertexs&... vert);
};

//template<class T>
//inline void Computer::transformModel(std::vector<T>& v, const Matrix& m)
//{
//	auto center = modelBBox->getCenterBoundingBox(-1);
//	Matrix transform = Matrix::CreateTranslationMatrixXYZ(-center[x], -center[y], -center[z]) * m * Matrix::CreateTranslationMatrixXYZ(center[x], center[y], center[z]);
//	for (auto& vert : v)
//	{
//		Vector3 vertex = (Vector3)vert.Pos;
//		vertex.vector3TransformCoord(transform);
//		vert.Pos = vertex;
//	}
//}
template<typename T>
T Computer::_genereateAverageNormal(T p)
{
	return p;
}
template<typename T, typename... Vertexs>
T Computer::_genereateAverageNormal(T sum, Vertexs&... vert)
{
	return sum + _genereateAverageNormal(vert...);
}
template<typename T, typename ...Vertexs>
T Computer::genereateAverageNormal(int c, T sum, Vertexs& ...vert)
{
	Point s = _genereateAverageNormal(sum, vert...);
	s *= 1.f / c;
	return s;
}

