#include "textures.h"

#include "../pEngineStatic/loaders.h"

GLuint Texture::globalTextureID(0);

bool Texture::loadTexture(const wstring& path)
{
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture); // All upcoming GL_TEXTURE_2D operations now have effect on our texture object
	// Set our texture parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// Set texture filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	
	// //////// SOIL ////////// //
	// Load, create texture and generate mipmaps
	//int width, height;
	//int channels = 3;
	//string fileAscii(path.begin(), path.end());
	//unsigned char* image = SOIL_load_image(fileAscii.c_str(), &width, &height, &channels, SOIL_LOAD_RGB);
	//if (!image)
	//	return false;
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	//glGenerateMipmap(GL_TEXTURE_2D);
	//SOIL_free_image_data(image);
	//glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidentily mess up our texture.

	string fileAscii(path.begin(), path.end());
	Image img;
	if (!ImageLoader::loadFileFromExpansion(fileAscii, &img))
	{
		// �������
		int width, height;
		int channels = 3;
		string fileAscii(path.begin(), path.end());
		unsigned char* image = SOIL_load_image(fileAscii.c_str(), &width, &height, &channels, SOIL_LOAD_RGB);
		if (!image)
			return false;
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(image);
		glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidentily mess up our texture.
		return true;
	}

	// ������ �������� �� ������
	texture = globalTextureID++;
	glGenTextures(1, &texture);  // ������������� OpenGL �������� IDs
	glBindTexture(GL_TEXTURE_2D, texture); // ��������� ���� ��������
	// �������� ����������
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	// �������� ����������
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	auto type = img.type == TI_RGB ? GL_RGB : GL_RGBA;
	glTexImage2D(GL_TEXTURE_2D, 0, type, img.width, img.height, 0, type, GL_UNSIGNED_BYTE, img.imageData.data());
	glGenerateMipmap(GL_TEXTURE_2D);

	return true;
}
GLuint& Texture::getTexture() { return texture; }

ContainerTextures::ContainerTextures() :count(0) {}
bool ContainerTextures::addTexture(const wstring& filename)
{
	if (textures.find(filename) == textures.end())
	{
		Texture t;
		if (!t.loadTexture(filename))
			return false;
		textures.insert({ filename, t });
		count++;
	}
	return true;
}
bool ContainerTextures::getTexture(const wstring& filename, Texture** texture)
{
	if (textures.find(filename) == textures.end())
	{
		// error
		return false;
	}
	*texture = &textures[filename];
	return true;
}
size_t& ContainerTextures::getSize() { return count; }
map<wstring, Texture>& ContainerTextures::getTexturesBuffer() { return textures; }

