#define _HAS_STD_BYTE 0

#include "model.h"

#include "../pEngine/game_prop.h"

#include "../pSound/sound.h"

#include "../pEngine/primitives.h"

#include <any>

#include "../opengl/Simple OpenGL Image Library/src/SOIL.h"

using namespace std::placeholders;

class OpenGLRender;
using OpenGLRenderPtr = shared_ptr< OpenGLRender>;

class OpenGLRender
{
private:

	Coordinate2 positionWindow;
	Size2 sizeWindow;
	Size2 sizeViewport;
	bool updateViewport;
	GLFWwindow* window;
	GLFWmonitor* monitor;
	Color backgroundColor;
	string title;
	bool vsync;

	shared_ptr<SoundDevice> sndDevice;

	shared_ptr<Camera> camera;
	TypeCamera typeCamera;
	CameraProperties camProp;
	map<char, bool> keyStatus;

	GameQualityPtr gameQuality;

	map<wstring, PipelineStateObjectPtr> psos;
	vector<ModelPtr> models;
	map<wstring, size_t> modelsIndex;
	map<wstring, CBufferGeneralPtr> cBuffers;

	ContainerShader shaders;
	ContainerTextures textures;
	ContainerMaterials materials;
	GameParametersOpenGL gameParam;

	Sun sunLight;
	LightPoint lightCenter;
	Torch torch;

	FPSCounter fpsCounter;

	chrono::time_point<std::chrono::steady_clock> timeFrameBefore;
	chrono::time_point<std::chrono::steady_clock> timeFrameAfter;


	void resize(int cx, int cy)
	{
		updateViewport = true;
		drawFrame();
	}

	void key(GLFWwindow* window, int key, int scancode, int action, int mode)
	{
		if (action == GLFW_PRESS)
		{
			if (key == GLFW_KEY_ESCAPE)
				glfwSetWindowShouldClose(window, GL_TRUE);
			else if (key == GLFW_KEY_HOME)
				setFullScreen(isFullscreen() ? false : true);
			else if (keyStatus.find((char)key) != keyStatus.end())
				keyStatus[(char)key] = true;
		}
		else if(action == GLFW_RELEASE)
		{
			if (keyStatus.find((char)key) != keyStatus.end())
				keyStatus[(char)key] = false;
			if (!keyStatus['L'])
				torch.torchUsed(false);
		}
	}

	void mouseCoordinate(GLFWwindow* window, double  x, double  y)
	{
		camProp.mouseCurrState = { static_cast<int>(x), static_cast<int>(y)};
	}

public:
	OpenGLRender(bool vsync, int w, int h, GameQualityConst gq, TypeCamera tc, const string& t) :vsync(vsync), sizeWindow(w, h), typeCamera(tc), updateViewport(true), window(nullptr), monitor(nullptr), title(t)
	{
		backgroundColor = { 0.0f, 0.0f, 0.0f, 1.0f };
		gameQuality.reset(new GameQuality(gq));
	}

	bool init()
	{
		std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
		if (!initRender())
			return false;
		createCamera();
		if (!loadPSO(L"engine_resource/configs/pso.pCfg"))
			return false;
		if (!loadMaterials())
			return false;
		if (!initLight())
			return false;
		
		fpsCounter.start();
		fpsCounter.setBeforeTime();

		if (!initSound())
			return false;
		startSound();

		return true;
	}

	bool initRender()
	{
		// Init GLFW
		glfwInit();
		// Set all the required options for GLFW
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		//glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		window = glfwCreateWindow(sizeWindow[vx], sizeWindow[vy], "OpenGL render", nullptr, nullptr);
		if (window == nullptr)
		{
			glfwTerminate();
			return false;
		}

		glfwMakeContextCurrent(window);
		glfwSwapInterval(vsync ? 1 : 0);

		glfwSetWindowUserPointer(window, this);
		glfwSetWindowSizeCallback(window, OpenGLRender::callbackResize);
		glfwSetKeyCallback(window, OpenGLRender::callbackKey);
		glfwSetCursorPosCallback(window, OpenGLRender::callbackMouseCoordinate);

		monitor = glfwGetPrimaryMonitor();
		glfwGetWindowSize(window, &sizeWindow[sw], &sizeWindow[sh]);
		glfwGetWindowPos(window, &positionWindow[vx], &positionWindow[vy]);
		updateViewport = true;
		// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
		glewExperimental = GL_TRUE;
		// Initialize GLEW to setup the OpenGL Function pointers
		glewInit();
		glEnable(GL_DEPTH_TEST);
		return true;
	}

	void createCamera()
	{
		keyStatus =
		{
			{'A', false},
			{'W', false},
			{'S', false},
			{'D', false},
			{'X', false},
		};
		Vector3 pos(0.f, 5.f, 0.f);
		Vector3 dir(0.f, 5.f, -100.f);
		switch (typeCamera)
		{
		case STATIC_CAMERA:
			camera.reset(new StaticCamera(pos, dir));
			break;

		case GAME_CAMERA:
			float step(30.f);
			float run(50.f);
			camera.reset(new GameCamera(step, run, pos, dir));
			glfwSetCursorPos(window, (sizeWindow[sw] + positionWindow[vx]) / 2, (sizeWindow[sh] + positionWindow[vy]) / 2); 
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
			break;
		}
		camera->setRenderFunctions(OPENGL_RENDER);
		camera->updateView(pos, dir, Camera::hCamStandart);
		camera->updateProjection(0.4f * 3.14f, sizeViewport[sw] / (float)sizeViewport[sh], 0.01f, 1000.f);
	}

	bool loadPSO(const wstring& cfg)
	{
		//load PSO
		bool status(false);
		auto psoProp = PSOLoader::load(status, cfg);
		if (!status)
		{
			// error
			exit(1);
		}
		// go to create PSO (and load shaders, create cBuffers etc)
		CBufferCreator cBCreator;
		for (auto&& pP : psoProp)
		{
			// load shaders
			pP.vsPath.insert(0, L"engine_resource/shaders/opengl/");
			pP.psPath.insert(0, L"engine_resource/shaders/opengl/");
			shaders.addVertexShader(pP.vsPath);
			shaders.addPixelShader(pP.psPath);
			// create cBuffers
			for_each(pP.cBuffNames.begin(), pP.cBuffNames.end(), [this, &cBCreator](const wstring& cBuffName)
				{
					if (cBuffers.find(cBuffName) == cBuffers.end())
						cBuffers.insert({ cBuffName , cBCreator.createCBuffer(cBuffName) });
				}
			);
			// create PSO
			PipelineStateObjectPtr _pso(new PipelineStateObject());
			for_each(pP.cBuffNames.begin(), pP.cBuffNames.end(), [&_pso](const wstring& key)
				{
					if (find(_pso->getCBufferKeys().begin(), _pso->getCBufferKeys().end(), key) == _pso->getCBufferKeys().end())
						_pso->addCBufferKey(key);
				}
			);
			for_each(pP.sBuffNames.begin(), pP.sBuffNames.end(), [&_pso](const wstring& key)
				{
					if (find(_pso->getSBufferKeys().begin(), _pso->getSBufferKeys().end(), key) == _pso->getSBufferKeys().end())
						_pso->addSBufferKey(key);
				}
			);
			if (!_pso->initPipelineStateObject(shaders, pP))
				return false;
			psos.insert({ pP.name, _pso });
		}
		return true;
	}

	bool loadMaterials()
	{
		bool status;
		auto materialsBlank = MaterialLoader::load(status, L"engine_resource/configs/materials.pCfg");
		if (!status)
			return false;
		for (auto& m : materialsBlank)
		{
			Material mat(
				{ m.ambient[vx], m.ambient[vy], m.ambient[vz], m.ambient[vw] },
				{ m.diffuse[vx], m.diffuse[vy], m.diffuse[vz], m.diffuse[vw] },
				{ m.specular[vx] , m.specular[vy], m.specular[vz], m.specular[vw] },
				m.shininess);
			if (!materials.addMaterial(m.name, mat))
			{
				wstring info = wstring(L"The material \"" + m.name + L"\" already exists. Ignoring.");
				string infoAscii(info.begin(), info.end());
				std::cout << infoAscii << std::endl;
			}
		}
		return true;
	}

	bool initLight()
	{
		sunLight.init(Color(1.f, 1.f, 0.776f, 1.f), { -350.f, 0.f, 0.f, 0.f }, 5);
		BlankModel bm;
		Color cl = { 1.f, 0.952f, 0.04f, 1 };
		Primitive::generateSphere(OPENGL_RENDER, bm.vertexs, bm.indexs, 10, 25, 25, cl);
		wstring nameSun = L"sun";
		bm.nameModel = nameSun;
		bm.shiftIndexs.push_back(bm.indexs.size());
		bm.psoName = L"PSONoLight";
		bm.texturesName.push_back(IS_COLOR);
		bm.doubleSides.push_back(false);
		bm.materials.push_back(L"default");
		auto sunPos = sunLight.getStartPosition();
		if (!createModel(bm, Matrix4::CreateTranslationMatrixXYZ(sunPos[vx], sunPos[vy], sunPos[vz])))
			return false;
		sunLight.setModel(nameSun);

		lightCenter = LightPoint({ 1.f, 0.549f, 0.f, 1.f }, { 0.f, 10.f, 0.f, 0.f });

		torch.init({ 1.f, 1.f, 1.f, 1.f }, {}, {}, 30.5f, 40.5f);
		keyStatus.insert({ 'L', false });

		return true;
	}

	bool createModel(BlankModel& data, Matrix4 world = Matrix4::Identity())
	{
		// todo: ����� ���������� ������� ��� �������� �������
		ModelPtr model;
		if (modelsIndex.find(data.nameModel) == modelsIndex.end())
		{
			for (auto&& t : data.texturesName)
			{
				if (t == IS_COLOR)
					continue;
				if (!data.texturesName.empty())
					if (!textures.addTexture(t))
						return false;
			}
		}

		if (data.instansing)
		{
			ModelInstancing* m = new ModelInstancing;
			auto worlds = move(any_cast<vector<Matrix>>(data.instansingBuffer[0]));
			auto normals = move(any_cast<vector<Matrix>>(data.instansingBuffer[0]));
			m->setStructuredBuffer(worlds);
			m->setStructuredBuffer(normals);
			model.reset(std::move(m));
		}
		else
			model.reset(new ModelIdentity);

		if (modelsIndex.find(data.nameModel) == modelsIndex.end())
		{
			model->createModel(psos, data);
			model->addWorld(world);
			models.push_back(model);
			modelsIndex.insert({ data.nameModel, models.size() - 1 });
		}
		else
		{
			size_t index = modelsIndex[data.nameModel];
			models[index]->addWorld(world);
		}
		return true;
	}

	bool initSound()
	{
		sndDevice.reset(new SoundDevice);
		Vector3 pos = camera->getPosition();
		Vector3 tar = camera->getTarget();
		Vector3 up = camera->getUp();
		if (!sndDevice->init(pos, tar, up, 0.f))
			return false;
		if (!sndDevice->addSound(L"engine_resource/sounds/rnd_night_1.wav", { 0.f, 0.f, 0.f }, STATIC_SOUND, true, false, 50.f))
			return false;
		if (!sndDevice->addSound(L"engine_resource/sounds/step.wav", { 0.f, 0.f, 0.f }, ACTOR_SOUND, false, false, 0.5f))
			return false;
		if (!sndDevice->addSound(L"engine_resource/sounds/run.wav", { 0.f, 0.f, 0.f }, ACTOR_SOUND, false, false, 0.5f))
			return false;
		if (!sndDevice->addSound(L"engine_resource/sounds/ambient2.wav", { 0.f, 0.f, 0.f }, AMBIENT_SOUND, true, false, 0.1f))
			return false;
		if (!sndDevice->addSound(L"engine_resource/sounds/heavy_wind_2.wav", Vector3(-90.f, 0.f, 90.f), STATIC_SOUND, true, false, 40.f))
			return false;
		if (!sndDevice->addSound(L"engine_resource/sounds/torch-on.wav", { 0.f, 0.f, 0.f }, ACTOR_SOUND, false, false, 0.6f))
			return false;
		if (!sndDevice->addSound(L"engine_resource/sounds/torch-off.wav", { 0.f, 0.f, 0.f }, ACTOR_SOUND, false, false, 0.6f))
			return false;
		return true;
	}

	void startSound()
	{
		sndDevice->start();
	}



	bool initScene()
	{
		array<Color, 2> colors =
		{
			Color(0.f, .49f, .05f, 1.f),
			Color(0.41f, 0.24f, 0.49f, 1.f)
		};
		Matrix4 trans;

		{
			// plane grass
			BlankModel bm;
			bm.nameModel = L"plane grass";
			bm.doubleSides.push_back(false);
			bm.texturesName.push_back(L"engine_resource/textures/pGrass.dds");
			bm.indexs =
			{
				0, 1, 2,  // First Triangle
				0, 2, 3   // Second Triangle
			};
			bm.shiftIndexs.push_back(bm.indexs.size());
			bm.vertexs =
			{
				Vertex(Vector3(-10000,  -5.f, -10000.0f), Vector3(0.f, 1.f, 0.f), Vector4(-129.f,  130.f, 0.0f, -1.f)),  // tl
				Vertex(Vector3(10000, -5.f, -10000.0f), Vector3(0.f, 1.f, 0.f), Vector4(130.f,  130.f, 0.0f, -1.f)),  // tr
				Vertex(Vector3(10000.f, -5.f, 10000.0f), Vector3(0.f, 1.f, 0.f), Vector4(130.f,  -129.f, 0.0f, -1.f)),  // br
				Vertex(Vector3(-10000.f,  -5.f, 10000.0f), Vector3(0.f, 1.f, 0.f), Vector4(-129.f,  -129.f, 0.0f, -1.f))   // bl
			};
			bm.psoName = L"PSOLightPhong";
			bm.materials.push_back(L"default");
			if (!createModel(bm))
				return false;
		}

		{
			// cube color
			BlankModel bm;
			bm.nameModel = L"cube color";
			Primitive::generateCube(OPENGL_RENDER, bm.vertexs, bm.indexs, 5.f, colors[0]);
			bm.shiftIndexs.push_back(bm.indexs.size());
			bm.texturesName.push_back(IS_COLOR); // color
			bm.materials.push_back(L"default");
			bm.doubleSides.push_back(false);
			bm.instansing = true;
			bm.psoName = L"PSOLightPhongInstansed";
			vector<Matrix> worlds;
			vector<Matrix> normals;
			array<pair<float, float>, 4> coord = { pair<float, float>{-15.f, -15.f}, pair<float, float>{-15.f, 15.f}, pair<float, float>{15.f, -15.f}, pair<float, float>{15.f, 15.f} };
			for (int i(0), y(10); i < 5; i++, y += 20)
			{
				for (int j(0); j < 4; j++)
				{
					auto [_x, _y] = coord[j];
					ModelPosition world(Matrix4::CreateTranslationMatrixXYZ(_x, (float)y, _y) * Matrix::CreateTranslationMatrixX(-80.f));
					worlds.push_back(world.getWorld().getGPUMatrix());
					normals.push_back(world.getNormals().getGPUMatrix());
				}
			}
			bm.instansingBuffer.emplace_back(worlds);
			bm.instansingBuffer.emplace_back(normals);
			if (!createModel(bm))
				return false;
		}

		{
			// cube wood
			BlankModel bm;
			Primitive::generateCube(OPENGL_RENDER, bm.vertexs, bm.indexs, 10.f);
			bm.shiftIndexs.push_back(bm.indexs.size());
			bm.nameModel = L"cube wood";
			bm.texturesName.push_back(L"engine_resource/textures/pWoodDoski.dds");
			bm.psoName = L"PSOLightBlinnPhongInstansed";
			bm.materials.push_back(L"default");
			bm.doubleSides.push_back(false);
			bm.instansing = true;
			float val(20);
			vector<Matrix> worlds;
			vector<Matrix> normals;
			array<pair<float, float>, 4> coord = { pair<float, float>{-val, -val}, pair<float, float>{-val, val}, pair<float, float>{val, -val}, pair<float, float>{val, val} };
			for (int i(0), y(10); i < 3; i++, y += 30)
			{
				for (int j(0); j < coord.size(); j++)
				{
					ModelPosition world(Matrix4::CreateTranslationMatrixXYZ(coord[j].first, (float)y, coord[j].second));
					worlds.push_back(world.getWorld().getGPUMatrix());
					normals.push_back(world.getNormals().getGPUMatrix());
				}
			}
			bm.instansingBuffer.emplace_back(worlds);
			bm.instansingBuffer.emplace_back(normals);
			if (!createModel(bm))
				return false;
		}

		{
			// plane grass, wood and cirpic
			BlankModel bm;
			bm.nameModel = L"plane grass, cirpic and wood";
			bm.texturesName.push_back(L"engine_resource/textures/pHouse.dds");
			bm.texturesName.push_back(L"engine_resource/textures/pWoodDesc.dds");
			bm.materials.push_back(L"default");
			bm.materials.push_back(L"default");
			bm.doubleSides.push_back(true);
			bm.doubleSides.push_back(true);
			bm.indexs =
			{
				5, 7, 6,
				5, 6, 4,
				9, 11, 10,
				9, 10, 8,
			};
			bm.shiftIndexs.push_back(6);
			bm.shiftIndexs.push_back(6);
			bm.vertexs =
			{
				Vertex({-100.f,  0.f,  250.f}, {0,1,0},  {-2,3,0,-1}),
				Vertex({50.f,  0.f,  250.f},{0,1,0}, {3,3,0,-1}),
				Vertex({50.f,  0.f, 50.f},{0,1,0}, {3,-2,0,-1}),
				Vertex({-100.f,  0.f,  50.f},{0,1,0}, {-2,-2,0,-1}),
				Vertex({50.f,  0.f,  150.f},{-1,0,0}, {2, 2 ,0,-1}),
				Vertex({50.f,  0.f, 50.f},{-1,0,0}, {-2, 2,0,-1}),
				Vertex({50.f,  100.f,  150.f},{-1,0,0}, {2,-2,0,-1}),
				Vertex({50.f,  100.f, 50.f},{-1,0,0}, {-2,-2,0,-1}),
				Vertex({50.f,  100.f,  150.f},{-1,0,0}, {1, 1 ,0,-1}), // 8
				Vertex({50.f,  100.f, 50.f},{-1,0,0}, {0, 1,0,-1}), // 9
				Vertex({50.f,  200.f,  150.f},{-1,0,0}, {1,0,0,-1}), // 10
				Vertex({50.f,  200.f, 50.f},{-1,0,0}, {0,0,0,-1}) // 11
			};
			bm.psoName = L"PSOLightBlinnPhong";
			if (!createModel(bm))
				return false;
		}

		{
			BlankModel bm;
			string pathModel = "engine_resource/models/statics/house1.pObject";
			if (!ModelLoader::loadFileFromExpansion(pathModel, TypeRender::OPENGL_RENDER, &bm))
				return false;
			bm.nameModel = wstring(pathModel.begin(), pathModel.end());
			if (!createModel(bm, Matrix::CreateTranslationMatrixZ(-70)))
				return false;
		}

		{
			if (!preloadObjFile("engine_resource/models/statics/test.obj", TypeRender::OPENGL_RENDER, L"PSOLightBlinnPhong", Matrix::CreateTranslationMatrixZ(20.f)))
				return false;
		}

		/*{
			BlankModel bm;
			bm.nameModel = L"test-image-plane";
			Primitive::generatePlane(OPENGL_RENDER, bm.vertexs, bm.indexs, 10, 10);
			bm.texturesName.push_back(L"engine_resource/textures/test.png");
			bm.materials.push_back(L"default");
			bm.psoName = L"PSOLightBlinnPhong";
			bm.shiftIndexs.push_back(bm.indexs.size());
			bm.doubleSides.push_back(true);
			if (!createModel(bm))
				return false;
		}*/

		return true;
	}

	bool preloadObjFile(const string& path, const TypeRender& tr, const wstring& pso, Matrix translation = Matrix::Identity())
	{
		BlankModel bm;
		if (!ModelLoader::loadFileFromExpansion(path, tr, &bm))
			return false;
		bm.psoName = pso;
		bm.nameModel = wstring(path.begin(), path.end());
		wstring firstMaterial;
		if (!materials.getFirst(firstMaterial))
		{
			// TODO: error
			return false;
		}
		for (auto&& t : bm.texturesName)
			bm.materials.push_back(firstMaterial);
		if (!createModel(bm, translation))
			return false;
		return true;
	}



	static void callbackResize(GLFWwindow* window, int cx, int cy)
	{
		void* ptr = glfwGetWindowUserPointer(window);
		if (OpenGLRender* wndPtr = static_cast<OpenGLRender*>(ptr))
			wndPtr->resize(cx, cy);
	}

	static void callbackKey(GLFWwindow* window, int key, int scancode, int action, int mode)
	{
		void* ptr = glfwGetWindowUserPointer(window);
		if (OpenGLRender* wndPtr = static_cast<OpenGLRender*>(ptr))
			wndPtr->key(window, key, scancode, action, mode);
	}

	static void callbackMouseCoordinate(GLFWwindow* window, double  x, double  y)
	{
		void* ptr = glfwGetWindowUserPointer(window);
		if (OpenGLRender* wndPtr = static_cast<OpenGLRender*>(ptr))
			wndPtr->mouseCoordinate(window, x, y);
	}


	void mainLoop()
	{
		while (!glfwWindowShouldClose(window))
		{
			drawFrame();
		}
	}

	bool update(float time)
	{
		updateCamera(time);
		updateSound();
		if (!updateScene())
			return false;
		return true;
	}

	void updateCamera(float time)
	{
		Coordinate2 _posWin;
		Size2 _sizeWin;
		glfwGetWindowSize(window, &_sizeWin[sw], &_sizeWin[sh]);
		glfwGetWindowPos(window, &_posWin[vx], &_posWin[vy]);
		long _x = (_posWin[vx] + _sizeWin[sw]) / 2;
		long _y = (_posWin[vy] + _sizeWin[sh]) / 2;
		glfwSetCursorPos(window, _x, _y);
		Point2 mDelta(camProp.mouseCurrState[vx] - _x, camProp.mouseCurrState[vy] - _y);
		camera->updateCamera(keyStatus, mDelta,time);
	}

	void updateSound()
	{
		Vector3 pos = camera->getPosition();
		Vector3 tar = camera->getTarget();
		Vector3 up = camera->getUp();
		sndDevice->updateListener(pos, tar, up);
		sndDevice->update(AMBIENT_SOUND, L"engine_resource/sounds/ambient2.wav", pos);
		if (keyStatus['W'] || keyStatus['A'] || keyStatus['S'] || keyStatus['D'])
		{
			wstring nameSnd = keyStatus['X'] ? L"engine_resource/sounds/run.wav" : L"engine_resource/sounds/step.wav";
			sndDevice->update(ACTOR_SOUND, nameSnd.c_str(), pos);
			sndDevice->play(ACTOR_SOUND, nameSnd.c_str());
		}
		if (keyStatus['L'] && !torch.isTorchUsed())
		{
			wstring nameSnd;
			if (torch.isEnable())
			{
				nameSnd = L"engine_resource/sounds/torch-off.wav";
				torch.disable();
			}
			else
			{
				nameSnd = L"engine_resource/sounds/torch-on.wav";
				torch.enable();
			}
			torch.torchUsed(true);
			sndDevice->update(ACTOR_SOUND, nameSnd.c_str(), pos);
			sndDevice->play(ACTOR_SOUND, nameSnd.c_str());
		}
	}

	bool updateScene()
	{
		if (torch.isEnable())
		{
			auto targ = camera->getTarget();
			torch.update((Vector)camera->getPosition(), (Vector)(targ - camera->getPosition()));
		}

		bool status;
		Matrix newPosSun = sunLight.update(status);
		if (status)
			models[modelsIndex[sunLight.getNameModel()]]->editWorld(newPosSun, 0);

		gameParam.cBuffers = &cBuffers;
		gameParam.camera = camera.get();
		gameParam.lightCenter = &lightCenter;
		gameParam.sunLight = &sunLight;
		gameParam.torch = &torch;
		gameParam.materials = &materials;
		gameParam.view = camera->getView().getGPUMatrix();
		gameParam.proj = camera->getProjection().getGPUMatrix();
		gameParam.torchProperty = { torch.getLight().getCutOff(), torch.getLight().getOuterCutOff(), (torch.isEnable() ? 1.f : 0.f), 1 };

		return true;
	}

	void drawFrame()
	{
		timeFrameAfter = std::chrono::steady_clock::now();
		float time = std::chrono::duration_cast<std::chrono::milliseconds>(timeFrameAfter - timeFrameBefore).count() * 0.001f;
		cout << time << endl;
		timeFrameBefore = std::chrono::steady_clock::now();

		if (!update(time))
		{
			//error
			exit(1);
		}
		if (updateViewport)
		{
			glfwGetFramebufferSize(window, &sizeViewport[sw], &sizeViewport[sh]);
			glViewport(0, 0, sizeViewport[sw], sizeViewport[sh]);
			camera->updateProjection(0.4f * 3.14f, sizeViewport[sw] / (float)sizeViewport[sh], 0.01f, 1000.f);
			updateViewport = false;
		}

		glClearColor(backgroundColor[cr], backgroundColor[cg], backgroundColor[cb], backgroundColor[ca]);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		for (int i(0); i < models.size(); i++)
		{
			auto pso = psos[models[i]->getPsoKey()];
			models[i]->draw(textures, pso, gameParam);
		}

		glfwSwapBuffers(window);
		glfwPollEvents();

		++fpsCounter;
		fpsCounter.setAfterTime();
		if (fpsCounter.isReady())
		{
			updateFps();
			fpsCounter.setBeforeTime();
		}

	}

	void updateFps()
	{
		glfwSetWindowTitle(window, string("Current FPS: " + to_string(fpsCounter.getFps())).c_str());
	}


	bool isFullscreen()
	{
		return glfwGetWindowMonitor(window) != nullptr;
	}

	void setFullScreen(bool fullscreen)
	{
		if (isFullscreen() == fullscreen)
			return;

		if (fullscreen)
		{
			// backup windwo position and window size
			glfwGetWindowPos(window, &positionWindow[vx], &positionWindow[vy]);
			glfwGetWindowSize(window, &sizeWindow[sw], &sizeWindow[sh]);

			// get reolution of monitor
			const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

			// swithc to full screen
			glfwSetWindowMonitor(window, monitor, 0, 0, mode->width, mode->height, 0);
		}
		else
		{
			// restore last window size and position
			glfwSetWindowMonitor(window, nullptr, positionWindow[vx], positionWindow[vy], sizeWindow[sw], sizeWindow[sh], 0);
		}

		updateViewport = true;
	}

	~OpenGLRender()
	{
		glfwTerminate();
	}
};

int main()
{
	//Image img;
	//if (!ImageLoader::loadFileFromExpansion("engine_resource/textures/test1.png", &img))
	//{
	//	return 0;
	//} 


	OpenGLRenderPtr render(new OpenGLRender(false, 1920, 1080, GameQualityConst::QUALITY_LOW, TypeCamera::GAME_CAMERA, "OpenGL Render"));
	if (!render->init())
		return false;
	if (!render->initScene())
		return false;
	render->mainLoop();

	return 0;
}

// vsync full screen - �� ��������

// ������ ��������� ���������� ����������� �����������

// ��������� ����� �����������(�����������) ��� d3d12 � opengl (����� ��������� ����� ����� ��� OpenGL)
