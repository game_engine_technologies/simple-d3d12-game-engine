#pragma once

#include "types.h"
#include "utils.h"
#include "pch.h"

#include "../pEngineStatic/types.h"
using namespace std::placeholders;

enum TypeCamera : int 
{
	STATIC_CAMERA,
	GAME_CAMERA
};

struct DYNLIB_PENGINE CameraProperties
{
	float moveLeftRight;
	float moveBackForward;
	float camYaw;
	float camPitch;
	float squat;
	Point2 lastPosMouse;
	Point2 mouseCurrState;

	CameraProperties();
};

class DYNLIB_PENGINE Camera
{
	MatrixCPUAndGPU view;
	MatrixCPUAndGPU projection;

	function<Matrix(const Vector3 &, const Vector3 &, const Vector3 &)> _view;
	function<Matrix(const float&, const float&, const float&, const float&)> _projection;

protected:
	Vector3 camPosition;
	Vector3 camUp;
	Vector3 camTarget;
	Vector3 camForward;
	Vector3 camRight;

public:
	static Vector3 hCamStandart;

	Camera() {}
	Camera(const Vector3& p, const Vector3& d);
	Camera(const Matrix4& v, const Matrix4& p);
	void updateView(const Vector3& p, const Vector3& d, const Vector3& h);
	void updateProjection(float fov, float aspect, float zn, float zf);
	MatrixCPUAndGPU& getView();
	MatrixCPUAndGPU& getProjection();
	Vector3& getPosition();
	Vector3& getUp();
	Vector3& getTarget();
	Vector3& getForward();
	Vector3& getRight();

	virtual void setRenderFunctions(const TypeRender& tr);
	virtual void updateCamera(map<char, bool>& keys, const Point2& mDelta, double time) = 0;
	virtual float getSpeed(float time, bool isX) = 0;
};

class DYNLIB_PENGINE StaticCamera : public Camera // pEngine
{
public:
	StaticCamera(const Vector3& p, const Vector3& d);
	void setRenderFunctions(const TypeRender& tr) override;
	void updateCamera(map<char, bool>& keys, const Point2& mDelta, double time) override;
	float getSpeed(float time, bool isX) override;
};

class DYNLIB_PENGINE GameCamera : public Camera // pEngine
{
	float _step;
	float _run;
	CameraProperties currentProperties;
	Vector3 DefaultForward;
	Vector3 DefaultRight;

	function<bool(map<char, bool>&, double)> _move;
	function<bool(const Point2&)> _rotate;

	bool _move_opengl(map<char, bool>& keyStatus, double time);
	bool _move_d3d(map<char, bool>& keyStatus, double time);
	bool _rotate_opengl(const Point2& mDelta);
	bool _rotate_d3d(const Point2& mDelta);

public:
	GameCamera(float step, float run, const Vector3& p, const Vector3& d);
	void setRenderFunctions(const TypeRender& tr) override;
	void updateCamera(map<char, bool>& keyStatus, const Point2& mDelta, double time) override;
	float getSpeed(float time, bool isX) override;
};
