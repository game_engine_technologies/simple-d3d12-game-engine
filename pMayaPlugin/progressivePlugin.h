#pragma once
// Range, Progressive Engine 2018

#include "progressiveMaterial.h"

#if defined (_WIN32)
#define strcasecmp _stricmp
#endif

#define NO_SMOOTHING_GROUP      -1
#define INITIALIZE_SMOOTHING    -2
#define INVALID_ID              -1


//////////////////////////////////////////////////////////////
class pObjectPlugin : public MPxFileTranslator
{
	// �����
	ofstream inputFile; // �������� ����
	map<Material, int, MaterialComparator> bufMaterials; // ����� ����������, int - ���������� ����� ���������, ������ � ����� ������� �������� � ������
	map<int, vector<Index>> arrayIndeces; // ����� � �������� ��������, ���� - ������ ���������
	map <Vertex, int, VertexComparatorFull > arrayVertex; // ������ ������ ��� ���� ������, ����� ��������������� ������������ � ����
	map<Vertex, set<Vector3>, VertexComparator> arrayNormal; // ������ ��������, ���� �� ����� (��� ����������)
	vector<int> tmpIndecesMaterialBuffer; // ��������� ����� �������� ����������
	string nameLogVar; // ��� ���������� ���� �� ������� mel - ��������
	vector<string> meshesName; // ����� �����, ���
	vector<int> meshesIndeces; // ����� �����, - ���-�� ��������� �������
	string pExt; // ���������� �������

public:
	// static �����
	static const char* const optionScript; // ������ ����� ��� ��������
	static const char* const optionScriptDefaultValues; // ������ �������� �� ���������
	static map<string, string> props; // ���� � �����������
	static bool isInit; // ������� �� �������� ������

	// ������������
	pObjectPlugin();
	static void* creator(); // ������ ��������� ������

	// �����������
	virtual ~pObjectPlugin() {};

	// ������
	MStatus reader(const MFileObject& file,
		const MString& optionsString,
		FileAccessMode mode);

	MStatus writer(const MFileObject& file,
		const MString& optionsString,
		FileAccessMode mode);
	bool haveReadMethod() const;
	bool haveWriteMethod() const;
	MString defaultExtension() const;
	MFileKind identifyFile(const MFileObject& fileName,
		const char* buffer,
		short size) const;





	MFnDagNode get_object_by_name(const char* name);
	static void initProperties(); // ������������� ��������

private:
	MStatus OutputPolygons(MDagPath&); // ���������� ���������
	MStatus exportSelection(); // ������� ���������� �������
	MStatus exportAll(); // ������� ���� �������
	bool isVisible(MFnDagNode& fnDag, MStatus& status); // ����� ��� ������� ������ �� �����
	MStatus writePolyFromBuffer(MFnMesh& fnMesh,
		MDagPath& mdagPath,
		MObject& set,
		MObject& comp); // ����� �������� � �����
	void setToLongUnitName(const MDistance::Unit&, MString&); // ������ ������� ��������� � �����
	bool writeInfoFromFile(); // ����� ��� ��������� ���������� � ����
	bool writeMaterialsFromFile(); // ����� ��� �������� � �����
	bool writeLineFromFile(const char* line); // ������ ������ � �����
	bool writeVertexsFromFile(); // ������ ������ � �����
	bool writeMeshesFromFile(); // ������ ���� ����� � �����
	bool writeOptionsFromFile(const MString& mname, const MString& option); // ������ 
	void clear(); // ������� ��� ������ ����� ���������
	void printMessageToLog(const string& m); // ������ � ��� � ������� mel

};