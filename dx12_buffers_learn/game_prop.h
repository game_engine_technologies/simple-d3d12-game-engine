#pragma once

#include "../pEngine/materials.h"
#include "../pEngine/camera.h"
#include "../pEngine/sun.h"
#include "../pEngine/torch.h"

struct GameParametersDirect3D12
{
	map<wstring, CBufferGeneralPtr>* cBuffers;
	ContainerMaterials* materials;
	Camera* camera;
	Sun* sunLight;
	LightPoint* lightCenter;
	Torch* torch;
	size_t* countAllCb;
	size_t* countMaterialsCb;
	Matrix view;
	Matrix proj;
	size_t frameIndex;
	Vector torchProperty;

	GameParametersDirect3D12() {}
};
