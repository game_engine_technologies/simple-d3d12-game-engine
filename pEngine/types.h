#pragma once

#include <string>
#include "utils.h"

#include "../math_graphics/math_graphics/mathematics.h"

using namespace gmath;
using namespace std;

using uint = unsigned int;
using uint8 = unsigned char;
using uint64 = unsigned long long;

class DYNLIB_PENGINE MatrixCPUAndGPU
{
	Matrix4 _m;
	Matrix4 _mT;

	void createTransponse();

public:
	MatrixCPUAndGPU();

	MatrixCPUAndGPU(const Matrix4& m);

	Matrix4& getCPUMatrix();

	Matrix4& getGPUMatrix();

	void updateCPUMatrix(const Matrix4& m);

};

class DYNLIB_PENGINE ModelPosition
{
	MatrixCPUAndGPU world;
	MatrixCPUAndGPU normal;

public:
	ModelPosition(const MatrixCPUAndGPU& w);
	void generateNormal();
	MatrixCPUAndGPU& getWorld();
	MatrixCPUAndGPU& getNormals();

};
