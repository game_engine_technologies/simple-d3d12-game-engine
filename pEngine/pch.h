#pragma once

#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <array>
#include <memory>
#include <ctime>
#include <fstream>
#include <functional>
#include <sstream>
#include <regex>
#include <set>
#include <chrono>
#include <experimental/filesystem>

#include "../math_graphics/math_graphics/mathematics.h"

using namespace std;
namespace fs = experimental::filesystem;
using namespace gmath;
using namespace gmtypes;
