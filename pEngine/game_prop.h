#pragma once

#include <string>
#include <map>
#include <memory>

#include "utils.h"

using namespace std;

enum DYNLIB_PENGINE GameQualityConst : int // pEngine
{
	QUALITY_VERY_LOW = 0,
	QUALITY_LOW
};

class DYNLIB_PENGINE GameQuality // pEngine
{

private:
	map< GameQualityConst, wstring> foldersShader;
	GameQualityConst currentQuality;

public:
	GameQuality(const GameQualityConst& cq);
	wstring& getFolder();
};

using GameQualityPtr = shared_ptr< GameQuality>; // pEngine
