#include "torch.h"

void Torch::init(const Color& c, const Vector& p, const Vector& d, float coff, float outerCutOff)
{
	light = LightSpot(c, p, d, cos(GeneralMath::degreesToRadians(coff)), cos(GeneralMath::degreesToRadians(outerCutOff)));
}
void Torch::update(const Vector& camPos, const Vector& camDir)
{
	light.setPosition(camPos);
	light.setDirection(Vector::Normalize(camDir));
}
void Torch::enable()
{
	_enable = true;
}
void Torch::disable()
{
	_enable = false;
}
bool Torch::isEnable() { return _enable; }
LightSpot& Torch::getLight() { return light; }
void Torch::torchUsed(bool use) { _used = use; }
bool Torch::isTorchUsed() { return _used; }