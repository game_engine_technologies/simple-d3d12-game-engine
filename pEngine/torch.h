#pragma once

#include "lights.h"
#include "utils.h"

class DYNLIB_PENGINE Torch
{
	bool _enable;
	bool _used;
	LightSpot light;
public:
	void init(const Color& c, const Vector& p, const Vector& d, float coff, float outerCutOff);

	void update(const Vector& camPos, const Vector& camDir);

	void enable();
	void disable();
	bool isEnable();
	LightSpot& getLight();
	void torchUsed(bool use);
	bool isTorchUsed();
};