#include "shaders.h"

void Shader::preprocessor(string& codeShader, string& pathShader)
{
	vector<function<bool(string&, string&)>> partsPreprocessor =
	{
		[](string& code, string& shaderDir) -> bool // macro insert
		{
				istringstream iss(code);
				string inc = "#include";
				string newCode;
				size_t countInclude(0);
				list<string> lines;
				for (auto iter = istream_iterator<LineAscii, char>(iss); iter != istream_iterator<LineAscii, char>(); iter++)
				{
					size_t startIndex(0);
					string line(*iter);
					if ((startIndex = line.find(inc)) != string::npos)
					{
						array<string, 2> comm = { "//", "/*" };
						for (auto&& c : comm)
						{
							size_t ind(0);
							if ((ind = line.find(c)) != string::npos)
								line.erase(ind);
						}
						line = regex_replace(line, regex("^[ ]*(.*?)[ ]*$"), "$1");
						if (line.empty())
							continue;
						cmatch result;
						regex reg("(\\s*)(#include)(\\s+)(\")([\\\/\.\\w-]+)(\")(\\s*)"); 
						if (!regex_match(line.c_str(), result, reg))
						{
							// error
							std::cout << "Error #include directive." << std::endl;
							exit(1);
						}
						fs::path p(line.substr(startIndex + inc.size() + 2));
						string _fpath = p.string(); // path to include shader
						_fpath.resize(_fpath.size() - 1);
						fs::path fPath = shaderDir + _fpath;
						ifstream includeShader(fPath);
						if (!includeShader)
						{
							// error
							exit(1);
						}
						stringstream _code;
						_code << includeShader.rdbuf();
						string codeIncludeShader = _code.str();

						line = codeIncludeShader;
						++countInclude;
					}
					lines.emplace_back(line);
				};
				code.clear();
				for (auto&& l : lines) code += l +"\n";
				if (countInclude > 0)
					return true;
				return false;
		},
		//[](string& code, string& shaderDir) -> bool // matrix -> mat4
		//{
		//	size_t index;
		//	string findStr = "matrix";
		//	string replaceStr = "mat4";
		//	while ((index = code.find(findStr)) != string::npos)
		//		code.replace(index, findStr.size(), replaceStr);
		//	return true;
		//},
		//[](string& code, string& shaderDir) -> bool // float4 -> vec4
		//{
		//	size_t index;
		//	string findStr = "float4";
		//	string replaceStr = "vec4";
		//	while ((index = code.find(findStr)) != string::npos)
		//		code.replace(index, findStr.size(), replaceStr);
		//	return true;
		//},
		//[](string& code, string& shaderDir) -> bool // float3 -> vec3
		//{
		//	size_t index;
		//	string findStr = "float3";
		//	string replaceStr = "vec3";
		//	while ((index = code.find(findStr)) != string::npos)
		//		code.replace(index, findStr.size(), replaceStr);
		//	return true;
		//},
		//[](string& code, string& shaderDir) -> bool // float2 -> vec2
		//{
		//	size_t index;
		//	string findStr = "float2";
		//	string replaceStr = "vec2";
		//	while ((index = code.find(findStr)) != string::npos)
		//		code.replace(index, findStr.size(), replaceStr);
		//	return true;
		//},
		//[](string& code, string& shaderDir) -> bool // static -> ""
		//{
		//	size_t index;
		//	string findStr = "static";
		//	string replaceStr = "";
		//	while ((index = code.find(findStr)) != string::npos)
		//		code.replace(index, findStr.size(), replaceStr);
		//	return true;
		//}
	};

	bool isIteration(false);
	for (auto iter = partsPreprocessor.begin(); iter != partsPreprocessor.end(); ++iter)
	{
		bool result = (*iter)(codeShader, pathShader);
		if (iter == partsPreprocessor.begin())
		{
			isIteration = result;
		}
	}
	if (isIteration)
		preprocessor(codeShader, pathShader);
}
int Shader::getTypeShader(const wstring& type)
{
	map<wstring, int> t = { {TypeShaders::vs, GL_VERTEX_SHADER}, {TypeShaders::ps, GL_FRAGMENT_SHADER} };
	// todo: ���������� ������!
	return t[type];
}
bool Shader::initShader(const wstring& path, const wstring& type)
{
	if (!createShader(path, type))
		return false;
	return true;
}
bool Shader::createShader(wstring path, wstring type)
{
	path += L'.' + type;
	string pathAndFilenameShader = string(path.begin(), path.end());
	ifstream shaderFile(pathAndFilenameShader);
	if (!shaderFile)
	{
		// error
		std::cout << (L"Shader " + path + L" not found. Terminated.\n").c_str() << std::endl;
		return false;
	}
	stringstream code;
	code << shaderFile.rdbuf();
	string _code(code.str());
	// preprocessor
	fs::path filenameShader(pathAndFilenameShader);
	string pathShader(pathAndFilenameShader);
	pathShader.resize(pathShader.length() - filenameShader.filename().string().length());
	preprocessor(_code, pathShader);
	std::cout << _code << std::endl << std::endl << std::endl;
	// end preprocessor
	const GLchar* _codeGL = _code.c_str();
	shader = glCreateShader(getTypeShader(type));
	glShaderSource(shader, 1, &_codeGL, NULL);
	glCompileShader(shader);
	// Check for compile time errors
	GLint success;
	GLchar infoLog[512];
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		// TODO: error
		glGetShaderInfoLog(shader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
		return false;
	}
	return true;
}
GLuint Shader::getShader() { return shader; }

void ContainerShader::loadShader(const wstring& path, const wstring& type)
{
	Shader newShader;
	if (!newShader.initShader(path, type))
	{
		std::cout << "Error load shader\n" << std::endl;
		exit(1);
	}
	shaders.insert({ {path, type}, newShader });
}
Shader& ContainerShader::getVertexShader(const wstring& path)
{
	if (shaders.find({ path, TypeShaders::vs }) != shaders.end())
		return shaders[{path, TypeShaders::vs}];
	// error!
}
Shader& ContainerShader::getPixelShader(const wstring& path)
{
	if (shaders.find({ path, TypeShaders::ps }) != shaders.end())
		return shaders[{path, TypeShaders::ps}];
	// error!
}
void ContainerShader::addVertexShader(const wstring& path)
{
	if (shaders.find({ path, TypeShaders::vs }) == shaders.end())
		loadShader(path, TypeShaders::vs);
}
void ContainerShader::addPixelShader(const wstring& path)
{
	if (shaders.find({ path, TypeShaders::ps }) == shaders.end())
		loadShader(path, TypeShaders::ps);
}