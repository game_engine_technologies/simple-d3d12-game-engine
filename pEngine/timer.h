#pragma once

#include "pch.h"
#include "utils.h"

class FPSCounter;

class DYNLIB_PENGINE SystemTimer
{
	clock_t oldTime;
	clock_t newTime;
	clock_t allTime;

	void clear();

	friend class FPSCounter;
public:
	SystemTimer();

	void start();

	void setBeforeTime();

	void setAfterTime();

	bool isSecond() const;

	bool isSecond_1_10() const;

	bool isSecond_1_100() const;

	bool isSecond_1_1000() const;

	float getCurrentTime();

};

class DYNLIB_PENGINE FPSCounter
{
	SystemTimer timerFps;
	size_t frameCount;

	void clear();
public:
	FPSCounter();

	void start();

	void setBeforeTime();

	void setAfterTime();

	bool isReady() const;

	size_t getFps();

	FPSCounter& operator++();
};


