#pragma once

#include <string>
#include <memory>
#include <map>

#include "../pEngineStatic/cBufferTypes.h"

using namespace std;

// cBuffer

class CBufferDataGeneral
{
public:
	virtual void updateData(GLuint shaderProgram, void* d) = 0;
};

class CBufferDataCamera: public CBufferDataGeneral
{
public:
	void updateData(GLuint shaderProgram, void* d)
	{
		cbufferCamera data = *reinterpret_cast<cbufferCamera*>(d);
		glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "camera.world"), 1, GL_FALSE, &(data.world(0, 0)));
		glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "camera.view"), 1, GL_FALSE, &(data.view(0, 0)));
		glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "camera.perspective"), 1, GL_FALSE, &(data.projection(0, 0)));
	}
};

class CBufferDataLight : public CBufferDataGeneral
{
public:
	void updateData(GLuint shaderProgram, void* d)
	{
		cbufferLight data = *reinterpret_cast<cbufferLight*>(d);
		glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "light.normals"), 1, GL_FALSE, &(data.normals(0, 0)));
		glUniform3fv(glGetUniformLocation(shaderProgram, "light.colorDirectional"), 1, &data.colorDirectional.toArray()[0]);
		glUniform3fv(glGetUniformLocation(shaderProgram, "light.directionalDirectional"), 1, &data.directionalDirectional.toArray()[0]);
		glUniform3fv(glGetUniformLocation(shaderProgram, "light.colorPoint"), 1, &data.colorPoint.toArray()[0]);
		glUniform3fv(glGetUniformLocation(shaderProgram, "light.positionPoint"), 1, &data.positionPoint.toArray()[0]);
		glUniform3fv(glGetUniformLocation(shaderProgram, "light.camPos"), 1, &data.cameraPosition.toArray()[0]);
		glUniform3fv(glGetUniformLocation(shaderProgram, "light.directionalSpot"), 1, &data.directionalSpot.toArray()[0]);
		glUniform3fv(glGetUniformLocation(shaderProgram, "light.positionSpot"), 1, &data.positionSpot.toArray()[0]);
		glUniform3fv(glGetUniformLocation(shaderProgram, "light.colorSpot"), 1, &data.colorSpot.toArray()[0]);
		glUniform3fv(glGetUniformLocation(shaderProgram, "light.propertySpot"), 1, &data.propertySpot.toArray()[0]);
	}
};

class CBufferDataMaterial : public CBufferDataGeneral
{
public:
	void updateData(GLuint shaderProgram, void* d)
	{
		cbufferMaterial data = *reinterpret_cast<cbufferMaterial*>(d);
		glUniform3fv(glGetUniformLocation(shaderProgram, "material.ambient"), 1, &data.ambient.toArray()[0]);
		glUniform3fv(glGetUniformLocation(shaderProgram, "material.diffuse"), 1, &data.diffuse.toArray()[0]);
		glUniform3fv(glGetUniformLocation(shaderProgram, "material.specular"), 1, &data.specular.toArray()[0]);
		glUniform1f(glGetUniformLocation(shaderProgram, "material.shininess"), data.shininess);
	}
};

using CBufferGeneralPtr = shared_ptr<CBufferDataGeneral>;

// Factory

class CBufferFactory
{
public:
	virtual CBufferGeneralPtr create() = 0;
};

class CBufferCamera : public CBufferFactory
{
public:
	CBufferGeneralPtr create()override
	{
		CBufferGeneralPtr buffer(new CBufferDataCamera);
		return buffer;
	}
};

class CBufferLight : public CBufferFactory
{
public:
	CBufferGeneralPtr create()override
	{
		CBufferGeneralPtr buffer(new CBufferDataLight);
		return buffer;
	}
};

class CBufferMaterial : public CBufferFactory
{
public:
	CBufferGeneralPtr create()override
	{
		CBufferGeneralPtr buffer(new CBufferDataMaterial);
		return buffer;
	}
};

// Factory creator

class CBufferCreator
{
	static map<wstring, shared_ptr<CBufferFactory>> cBufferCreators;

	static map<wstring, shared_ptr<CBufferFactory>> initCBufferCreators()
	{
		return
		{
			{L"cbCamera", shared_ptr<CBufferFactory>(new CBufferCamera)},
			{L"cbLight", shared_ptr<CBufferFactory>(new CBufferLight)},
			{L"cbMaterial", shared_ptr<CBufferFactory>(new CBufferMaterial)}
		};
	}
public:
	CBufferGeneralPtr createCBuffer(const wstring& key)
	{
		if (cBufferCreators.find(key) == cBufferCreators.end())
		{
			// error
			exit(1);
		}
		return cBufferCreators[key]->create();
	}
};

map<wstring, shared_ptr<CBufferFactory>> CBufferCreator::cBufferCreators(CBufferCreator::initCBufferCreators());
