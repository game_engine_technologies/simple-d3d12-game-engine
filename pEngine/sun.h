#pragma once

#include "lights.h"
#include "timer.h"
#include "utils.h"

class DYNLIB_PENGINE Sun
{
	LightDirection light;
	Vector position;
	SystemTimer updater;
	Matrix rotate;
	float angle;
	int wait;
	int currentWait;
	wstring modelSun;
	Vector center;

	bool isStop();
public:
	void init(const Color& c, const Vector& p, int w);

	Matrix update(bool& status);

	LightDirection& getLightSun();

	void setModel(const wstring& s);

	wstring& getNameModel();

	Vector& getStartPosition();
};
