#include "progressivePlugin.h"

//////////////////////////////////////////////////////////////

const char * const pObjectPlugin::optionScript("progressiveExportOptions");
const char * const pObjectPlugin::optionScriptDefaultValues("");

//////////////////////////////////////////////////////////////

bool pObjectPlugin::isInit(true);
map<string, string> pObjectPlugin::props;

pObjectPlugin::pObjectPlugin()
{
	MStreamUtils::stdErrorStream() << "Progressive config file load succesful.\n";
	if (!Utils::loadMelScript(nameLogVar))
		return;
	MStreamUtils::stdErrorStream() << "pEngineGlobalVariables script file load succesful.\n";
	isInit = true;
	pExt = "pObject";
}

//////////////////////////////////////////////////////////////

void* pObjectPlugin::creator()
{
    return new pObjectPlugin();
}

//////////////////////////////////////////////////////////////
MString pObjectPlugin::defaultExtension() const
{
	return MString(pExt.c_str());
}

//////////////////////////////////////////////////////////////

MStatus pObjectPlugin::reader ( const MFileObject& file,
                                const MString& options,
                                FileAccessMode mode)
{
    fprintf(stderr, "ObjTranslator::reader called in error\n");
    return MS::kFailure;
}

//////////////////////////////////////////////////////////////

MStatus pObjectPlugin::writer ( const MFileObject& file,
                                const MString& options,
                                FileAccessMode mode )
{
	if (!isInit)
	{
		MStreamUtils::stdErrorStream() << "pEngine plugin initialize error! Please, reinstall plugin. \n";
		return MStatus::kFailure;
	}

	printMessageToLog("Range Progressive Engine. Exporting model(s) from maya.");
	
	string opt = options.asChar();
	if (opt == ";") 
	{
		MStreamUtils::stdErrorStream() << "Your export settings are not correct. Please use the \"pEngineTools\" export shell. \n";
		return MStatus::kFailure;
	}

	MStatus status = MStatus::kSuccess;  // ������ ���������� �������
    
	// ��� �����
    MString mname = file.fullName();

	if ((mode == MPxFileTranslator::kExportAccessMode) ||
		(mode == MPxFileTranslator::kSaveAccessMode)) // ���� �� ������� ����� �������������� ��( ��� ��������� ��? )
	{
		status = exportAll(); // TODO: �����������
		clear();
		return MS::kFailure;
	}
    else if( mode == MPxFileTranslator::kExportActiveAccessMode ) // ���� ������� ����� �������������� ����������
        status = exportSelection();

	// TODO: ���������, ��� ����� ��� �������������� ���������� ������(� maya 2000, � ������ 3500)

	if (status != MStatus::kSuccess)
		return status;

	// ������� ����
	inputFile.open(mname.asChar(), std::ios_base::binary);
	if (!inputFile) // ���� ���� ������ �������(����� ������� � �.�.)
	{
		string out = "Error: The file ";
		out += mname.asChar();
		out += " could not be opened for writing.";
		printMessageToLog(out);
		clear();
		return MS::kFailure;
	}

	if (!writeInfoFromFile()) // ����� ��������� ����������
	{
		string out = "Error write data from ";
		out += mname.asChar();
		out += " file.";
		printMessageToLog(out);
		clear();
		return MStatus::kFailure;
	}
	if (!writeMaterialsFromFile()) // ���������� ��� ��������� � ����
	{
		string out = "Error write data from ";
		out += mname.asChar();
		out += " file.";
		printMessageToLog(out);
		clear();
		return MStatus::kFailure;
	}
	if (!writeMeshesFromFile()) // ����� ��� ���� � ����
	{
		string out = "Error write data from ";
		out += mname.asChar();
		out += " file.";
		printMessageToLog(out);
		clear();
		return MStatus::kFailure;
	}
	if (!writeVertexsFromFile()) // ���������� ��� ������� � ����
	{
		string out = "Error write data from ";
		out += mname.asChar();
		out += " file.";
		printMessageToLog(out);
		clear();
		return MStatus::kFailure;
	}
	if (!writeOptionsFromFile(mname, options)) // ���������� ��������� � ����
	{
		clear();
		return MStatus::kFailure;
	}
	
	string out = "Model ";
	out += mname.asChar();
	out += " export successeful.";
	printMessageToLog(out);
	printMessageToLog("---------------------------");
	clear(); // ������� ��� ������
	return MStatus::kSuccess;
}
//////////////////////////////////////////////////////////////

void pObjectPlugin::setToLongUnitName(const MDistance::Unit &unit, MString& unitName)
{
    switch( unit ) 
	{
	case MDistance::kInches:
		/// Inches
		unitName = "inches";
		break;
	case MDistance::kFeet:
		/// Feet
		unitName = "feet";
		break;
	case MDistance::kYards:
		/// Yards
		unitName = "yards";
		break;
	case MDistance::kMiles:
		/// Miles
		unitName = "miles";
		break;
	case MDistance::kMillimeters:
		/// Millimeters
		unitName = "millimeters";
		break;
	case MDistance::kCentimeters:
		/// Centimeters
		unitName = "centimeters";
		break;
	case MDistance::kKilometers:
		/// Kilometers
		unitName = "kilometers";
		break;
	case MDistance::kMeters:
		/// Meters
		unitName = "meters";
		break;
	default:
		break;
	}
}
//////////////////////////////////////////////////////////////

bool pObjectPlugin::haveReadMethod () const
{
    return false;
}
//////////////////////////////////////////////////////////////

bool pObjectPlugin::haveWriteMethod () const
{
    return true;
}

//////////////////////////////////////////////////////////////
MPxFileTranslator::MFileKind pObjectPlugin::identifyFile(
	const MFileObject& fileName,
	const char* buffer,
	short size) const
{
	const char * name = fileName.name().asChar();
	int   nameLength = strlen(name);

	if ((nameLength > pExt.length()) && !strcasecmp(name + nameLength - pExt.length(), pExt.c_str()))
		return kCouldBeMyFileType;
	else
		return kNotMyFileType;
}


//////////////////////////////////////////////////////////////

MStatus pObjectPlugin::OutputPolygons(MDagPath& mdagPath)
{
	// ������� ����� ��������� ������, ��������, ���������� ���������

	// ��������� ����������
	MStatus stat = MS::kSuccess; // ������ ���������� ������
	int instanceNum(0);
	MObjectArray sets;
	MObjectArray comps;

	// �������� ������ ���������� ��� ����� ����
	if (mdagPath.isInstanced())
		instanceNum = mdagPath.instanceNumber();

	MFnMesh fnMesh(mdagPath, &stat);
	if (stat != MS::kSuccess)
	{
		printMessageToLog("Failure in MFnMesh initialization.");
		return MS::kFailure;
	}

	if (!fnMesh.getConnectedSetsAndMembers(instanceNum, sets, comps, true))
	{
		printMessageToLog("ERROR: MFnMesh::getConnectedSetsAndMembers");
		return MS::kFailure;
	}

	// ���������� ��� ���������, � ��������� ������ ���������, ������� ������������ ����������� ���������
	string nameMesh = fnMesh.name().asChar();
	meshesName.push_back(nameMesh);
	meshesIndeces.push_back(0);
	for (unsigned i = 0; i < sets.length(); i++)
	{
		MStatus stat = writePolyFromBuffer(fnMesh, mdagPath, sets[i], comps[i]);
		if (stat == MStatus::kNotImplemented)
			continue;
		else if (stat == MStatus::kFailure)
			return stat;
		meshesIndeces.back()++;
	}

	return MStatus::kSuccess;
}

//////////////////////////////////////////////////////////////

MStatus pObjectPlugin::exportSelection( )
{
	MStatus status;
	MString filename;

	MSelectionList slist;  // ������ ��������
	MGlobal::getActiveSelectionList( slist ); // �������� ������ ���������� �������� � maya
	MItSelectionList iter( slist ); // ������� �������� �� ������ ������� �� ���������� ��������

	if (iter.isDone())  // ���� ���������� �������� ���(���� ������ ����)
	{
		printMessageToLog("Error: Nothing is selected.");
	    return MS::kFailure; // ���������� ������
	}

    // �������������� �������� ��������, ��� ������ ������ ��������
	// 1 - ������� ������� �������� �� ���������
	// 2 - ���������� ���������(����������� ��������)
	// 3 - ������ �������� ���������
    MItDag dagIterator( MItDag::kDepthFirst, MFn::kInvalid, &status);

	// � ����� �������� ��� ���������� ������
	for ( ; !iter.isDone(); iter.next() )
	{	 
		MDagPath objectPath; // ������ ��������� �� ������ ��������
		status = iter.getDagPath( objectPath); // �������� ������ ��������� �� �������� �������

		//  ���������� �������� �������� �� ��������� ����
		status = dagIterator.reset (objectPath.node(), 
									MItDag::kDepthFirst, MFn::kInvalid );	

		// ���������� ��� ���������� �� ������ �������
		for ( ; !dagIterator.isDone(); dagIterator.next() )
		{
			MDagPath dagPath; // ������ ��������� ���� �� �������(���������)
			status = dagIterator.getPath(dagPath); // �������� ������� ����

			if (!status) // ���� �� ����������
			{ 
				printMessageToLog("Failure getting DAG path.");
				return MS::kFailure;
			}

			if (status) // ������ ���������
			{
                MFnDagNode dagNode( dagPath, &status ); // ����� ������� � �����
                if (dagNode.isIntermediateObject())  // ���� ������ �������������(��� ������������ � �.�.)
                    continue; // �� ������������ ���

				// ���������� ������, ���� �������� ����� ������� �������������� �����������
				if (dagPath.hasFn(MFn::kNurbsSurface))  // ���� ��������� ������������ NURBS ���������
				{
					status = MS::kSuccess;
					printMessageToLog("Warning: skipping Nurbs Surface.");
				}
				else if ((  dagPath.hasFn(MFn::kMesh)) && (  dagPath.hasFn(MFn::kTransform))) // ���� ��� ��� � ������������� - �� ������� �� ��������� �����
					continue;
				else if (  dagPath.hasFn(MFn::kMesh)) // ���� ��� ������ ��� - �� ��� ������������
				{
					status = OutputPolygons(dagPath); // ���������� ���������
					if (status != MS::kSuccess) // ������ ������ ������ � ������
					{
						printMessageToLog("Error: exporting geom failed, check your selection.");
						return status;
					}
				}
			}
		}
	}
		
	return status;
}

//////////////////////////////////////////////////////////////
MFnDagNode pObjectPlugin::get_object_by_name(const char* name)
{
	MStatus status;
	MItDag dagIter(MItDag::kDepthFirst, MFn::kInvalid, &status);
	for (; !dagIter.isDone(); dagIter.next()) {
		MDagPath dagPath;
		status = dagIter.getPath(dagPath);

		MFnDagNode nodeFn;
		nodeFn.setObject(dagPath);
		if (strcmp(nodeFn.name().asChar(), name) == 0)
		{
			return dagPath;
		}
	}
	return MDagPath();
}


MStatus pObjectPlugin::exportAll( )
{
	MStatus status = MS::kSuccess;
	int count_model(0);
	vector<MObject> vecObj;

	MItDag dagIterator( MItDag::kBreadthFirst, MFn::kInvalid, &status);

	if ( MS::kSuccess != status)
	{
		printMessageToLog("Failure in DAG iterator setup.");
	    return MS::kFailure;
	}

	for ( ; !dagIterator.isDone(); dagIterator.next() )
	{
		MDagPath dagPath;
		MObject  component = MObject::kNullObj;
		status = dagIterator.getPath(dagPath);

		if (!status) 
		{
			printMessageToLog("Failure getting DAG path.");
			return MS::kFailure;
		}

		MFnDagNode dagNode( dagPath, &status );
		if (dagNode.isIntermediateObject()) 
		{
			continue;
		}

		if ((  dagPath.hasFn(MFn::kNurbsSurface)) &&
			(  dagPath.hasFn(MFn::kTransform)))
		{
			status = MS::kSuccess;
			printMessageToLog("Warning: skipping Nurbs Surface.");
		}
		else if ((  dagPath.hasFn(MFn::kMesh)) &&
				 (  dagPath.hasFn(MFn::kTransform)))
		{
			continue;
		}
		else if (  dagPath.hasFn(MFn::kMesh))
		{			
			MObject obj(dagPath.node());
			vecObj.push_back(obj);
			count_model++;
			
			/*status = OutputPolygons(dagPath, component);
			if (status != MS::kSuccess) {
				fprintf(stderr,"Error: exporting geom failed.\n");
                return MS::kFailure;
            }*/
		}
	}

	// �����!!!!!!!

	//MObjectArray arr(vecObj.size());
	//for (int i(0); i < arr.length(); i++)
	//	arr[i] = vecObj[i];

	//MFnRenderLayer* renderLayer = new MFnRenderLayer();

	//MHWRender::MRenderer* renderer = MHWRender::MRenderer::theRenderer();
	//
	//// ������� ������� ����������� 512 x 512 � ��������� �� ����
	//MImage img;
	//unsigned char pixels[512 * 515 * 4];
	//for (int i = 0; i < 512 * 515 * 4; i += 4)
	//{
	//	pixels[i] = pixels[i + 3] = (char)255;
	//	pixels[i + 1] = pixels[i + 2] = (char)0;
	//}
	//img.setPixels(pixels, 512, 512);
	//img.writeToFile(R"(C:\Users\admin_test\Desktop\test.jpg)", "jpg");
	//

	//M3dView m3d = M3dView::active3dView(&status);
	//if (status != MStatus::kSuccess)
	//	return status;
	//MImage img;

	//// create camera - ok !
	//MDagModifier dagModifier;
	//MObject cameraTransformObj = dagModifier.createNode("transform");
	//dagModifier.renameNode(cameraTransformObj, "myCameraTransform");
	//MFnTransform transformFn(cameraTransformObj);
	//transformFn.setTranslation(MVector(0, 0, 40), MSpace::kTransform);

	//MObject cameraShapeObj = dagModifier.createNode("camera", cameraTransformObj);
	//dagModifier.renameNode(cameraShapeObj, "myCameraShape");
	//dagModifier.doIt();

	//MDagPath cam;
	//if (cameraShapeObj.hasFn(MFn::kDagNode))
	//{
	//	cam = MDagPath::getAPathTo(cameraShapeObj);
	//	m3d.setCamera(cam);
	//	m3d.refresh(); // ��� - �� ������� ���������� ��������
	//}
	//else
	//	return MStatus::kFailure;
	////






	// �������� ������ �����������
	//MDagPath dag = get_object_by_name("persp").dagPath();
	//MFnTransform trans(dag);
	//// �������� ������� � ���������� � ������ ���������
	//MVector pos = trans.getTranslation(MSpace::kTransform);
	//trans.setTranslation(MVector(-pos.x, -pos.y, -pos.z), MSpace::kTransform);
	//// �������� ������� � ������� �� 0
	//MEulerRotation rotate;
	//trans.getRotation(rotate);
	//MEulerRotation newRotate(rotate);
	//newRotate.x = -newRotate.x;
	//newRotate.y = -newRotate.y;
	//newRotate.z = -newRotate.z;
	// ���������� ������ �� 40 ������ ����� �� z
	//trans.setTranslation(MVector(0, 0, -40), MSpace::kTransform);



	//// ���������� ������ ������
	//MDagPath cameraOldPath;
	//m3d.getCamera(cameraOldPath);

	//m3d.setObjectDisplay(M3dView::DisplayObjects::kDisplayMeshes);
	//m3d.refresh(true, true);

	/*m3d.readColorBuffer(img, true);

	status = img.writeToFile(R"(C:\Users\admin_test\Desktop\test.jpg)", "jpg");
	if (status != MStatus::kSuccess)
		return status;*/

	return status;
}


bool pObjectPlugin::isVisible(MFnDagNode & fnDag, MStatus& status)
{
	/*if (fnDag.isIntermediateObject()) 
		return false;*/

	MPlug visPlug = fnDag.findPlug("visibility", &status); // ���������� ��������� ���������
	if (MStatus::kFailure == status)  // �� ������� ��������� ���������
	{
		printMessageToLog("MPlug::findPlug error!");
		//MGlobal::displayError("MPlug::findPlug");
		return false;
	}
	else 
	{
		bool visible;
		status = visPlug.getValue(visible); // �������� �������� ���������
		if (MStatus::kFailure == status)
		{
			//MGlobal::displayError("MPlug::getValue");
			printMessageToLog("MPlug::getValue error!");
			return false;
		}
		return visible;
	}
}


MStatus pObjectPlugin::writePolyFromBuffer(MFnMesh& fnMesh, 
	MDagPath& mdagPath, 
	MObject& set,
	MObject& comp)
{
	// ����������
	MStatus stat = MStatus::kSuccess; // ������ ���������� ������
	MSpace::Space space = MSpace::kWorld; // ����������(�������)
	Material mat; // ������ ��������
	Color colorModel;
	wstring tmpIsColor(IS_COLOR);
	string isColor(tmpIsColor.begin(), tmpIsColor.end());
	
	string texturesPath = props["$game_textures"];

	MFnSet fnSet(set, &stat); // ������� �����
	if (stat == MS::kFailure)   // �� ������� �������
	{
		MStreamUtils::stdErrorStream() << "ERROR: MFnSet::MFnSet\n";
		return MStatus::kNotImplemented;
	}

	MItMeshPolygon polyIter(mdagPath, comp, &stat); // �������� �� �������
	if ((stat == MS::kFailure)/* || comp.isNull()*/) // �� ������� ������� ��������
	{
		MStreamUtils::stdErrorStream() << "ERROR: Can't create poly iterator!\n";
		return MStatus::kNotImplemented;
	}

	MFnDependencyNode fnNode(set); // �������� ��������
	MPlug shaderPlug = fnNode.findPlug("surfaceShader");
	MPlugArray connectedPlugs;
	if (shaderPlug.isNull())
	{
		MStreamUtils::stdErrorStream() << "ERROR: Can't find material!\n";
		return MStatus::kNotImplemented;
	}
	shaderPlug.connectedTo(connectedPlugs, true, false);

	MFnNumericAttribute nAttr;
	MFnEnumAttribute eAttr;
	MFnDependencyNode fnDN(connectedPlugs[0].node()); // �������� ��� ���������
	mat.nameMaterial = fnDN.name().asChar(); // ���������� ��� ���������

	MObject pEngineMaterial = fnDN.attribute("PEngineMaterial", &stat);
	if (stat == MS::kInvalidParameter)
	{
		MStreamUtils::stdErrorStream() << "ERROR: can't find pEngine material\n";
		return MStatus::kNotImplemented;
	}
	int indexMaterial = MPlug(fnDN.object(), pEngineMaterial).asInt(); // get pEngine material
	eAttr.setObject(pEngineMaterial);
	MString mName = eAttr.fieldName(indexMaterial, &stat);
	mat.valueMaterial= mName.asChar();
	if (stat != MS::kSuccess)
	{
		MStreamUtils::stdErrorStream() << "ERROR: can't get pEngine material\n";
		return MStatus::kNotImplemented;
	}

	MObject pEngineDoubleSides= fnDN.attribute("PDoubleSides", &stat);  // get pEngine double sides
	if (stat == MS::kInvalidParameter)
	{
		MStreamUtils::stdErrorStream() << "ERROR: debug\n";
		return MStatus::kNotImplemented;
	}
	nAttr.setObject(pEngineDoubleSides);
	mat.doubleSides = static_cast<bool>(MPlug(fnDN.object(), pEngineDoubleSides).asInt());
	

	MItDependencyGraph dgIt(shaderPlug, MFn::kFileTexture, // �������� ���� �������
		MItDependencyGraph::kUpstream,
		MItDependencyGraph::kBreadthFirst,
		MItDependencyGraph::kNodeLevel,
		&stat);
	if (stat == MS::kFailure) //�� ������ �������� ���� �������
	{
		MStreamUtils::stdErrorStream() << "ERROR: Can't load graph textures\n";
		return MStatus::kNotImplemented;
	}
	dgIt.disablePruningOnFilter();

	if (!dgIt.isDone()) // �������� �������
	{
		MObject textureNode = dgIt.thisNode(); // �������� ��� �������
		MPlug filenamePlug = MFnDependencyNode(textureNode).findPlug("fileTextureName");
		MString textureName; // ��� ��������
		filenamePlug.getValue(textureName);
		mat.nameTexture = textureName.asChar(); // ���������� ��� ��������
		if (mat.nameTexture.find(texturesPath) != string::npos)
			mat.nameTexture = mat.nameTexture.erase(0, texturesPath.length() + 1);
		else
		{
			int indexSlash = mat.nameTexture.rfind("/");
			mat.nameTexture = mat.nameTexture.erase(0, indexSlash + 1);
		}
	}
	else // ������� ���
	{
		vector< float> color(4);
		vector< MString> colorNames = { "colorR", "colorG", "colorB", "transparencyR" };
		for (size_t i(0); i < color.size(); i++)
		{
			auto cl = fnDN.attribute(colorNames[i], &stat);  // get pEngine double sides
			if (stat == MS::kFailure)
			{
				MStreamUtils::stdErrorStream() << "ERROR: Can't find " << colorNames[i] << " \n";
				return MStatus::kNotImplemented;
			}
			color[i] = MPlug(fnDN.object(), cl).asFloat(); // get part color
		}
		colorModel = { color[0], color[1] ,color[2] ,color[3] };
		mat.nameTexture = isColor;
	}

	// �������� ������ ���������
	indexMaterial = -1; // ������ ��������� ��� ������ ��������
	if (bufMaterials.find(mat) != bufMaterials.end())
		indexMaterial = bufMaterials[mat];
	else
	{
		int value(bufMaterials.size());
		bufMaterials.insert({ mat, value });
		indexMaterial = value;
	}
	// �������� ��������� �����, ������� ������������� ����������� ���������
	vector<Index>* indeces;
	if (arrayIndeces.find(indexMaterial) == arrayIndeces.end())
		arrayIndeces.insert({ indexMaterial, {} });
	indeces = &arrayIndeces[indexMaterial];

	 // ����� ������������ ��� ��������, ������� ����������� ����� ��������� � ��� �� ������ ������������ � ���� �����(����� ������� ����� � ����)
	for (; !polyIter.isDone(); polyIter.next())
	{
		int polyVertexCount = polyIter.polygonVertexCount(); // �������� ���������� ������ � ��������
		if (polyVertexCount != 3)  // ��������, ��������������� �� ������
		{
			MStreamUtils::stdErrorStream() << "Model " << fnMesh.name().asChar() << " is not triangulate!\n";
			return MS::kFailure;
		}
		for (int vtx = 0; vtx < polyVertexCount; vtx++) // ���������� ��� ������� � ��������
		{
			// ����������
			Vertex vertex;

			// �������� �������
			MPointArray vert;
			polyIter.getPoints(vert, space);
			vertex.pos = { (float)vert[vtx].x, (float)vert[vtx].y, (float)vert[vtx].z }; // z - ���������� ������������� ��� D3D | -z

			// �������� ���������� ���������� �������
			MFloatArray us;
			MFloatArray vs;
			polyIter.getUVs(us, vs);
			if(mat.nameTexture != isColor)
				vertex.color = { us[vtx] , vs[vtx] , 0.f, -1.f }; // ������ ���������� v ��� D3D | 0 - v
			else
				vertex.color = colorModel;

			// �������� �������
			MVectorArray norm;
			polyIter.getNormals(norm, space);
			vertex.normals = { (float)norm[vtx].x, (float)norm[vtx].y, (float)norm[vtx].z }; // | -z

			// ������������ ������ ������ � ��������
			auto iter = arrayVertex.find(vertex);
			Index indexVertex(0);
			if (iter == arrayVertex.end()) // ���� ����� ������� ���
			{
				indexVertex = arrayVertex.size();
				arrayVertex.insert({ vertex, indexVertex }); // �������� �������
			}
			else
				indexVertex = iter->second;

			// ��������� ������
			indeces->push_back(indexVertex);

			// ������������ ������ �������� ��� ����������
			if (arrayNormal.find(vertex) == arrayNormal.end()) // ���� ��� ����� �������
			{
				std::set<Vector3> s;
				s.insert(vertex.normals);
				arrayNormal.insert(std::make_pair(vertex, s));
			}
			else
				arrayNormal[vertex].insert(vertex.normals);

		}
		// std::swap(indeces->back(), indeces->at(indeces->size() - 3)); // �������������� �����������(����� � ������ ��� ����� � ������)
	}

	return MStatus::kSuccess;
}

//////////////////////////////////////////////////////////////
bool pObjectPlugin::writeInfoFromFile()
{
	// ����� � ����� ����� ����� - ���� ��������� ����������
	int block = pObjFormatBlocks::serviceInfo;
	inputFile.write((char*)&block, sizeInt);
	if (!inputFile)
		return false;
	// ����� � ����� ������ � ������������, ������� ������ ��� ������
	size_t sizeBuffer;
	errno_t error = getenv_s(&sizeBuffer, nullptr, 0, "USERNAME");
	if(error != 0)
		return false;
	string name; name.resize(sizeBuffer);
	error = getenv_s(&sizeBuffer, (char*)name.c_str(), name.size(), "USERNAME");
	if (error != 0)
		return false;
	writeLineFromFile(name.c_str());
	// ����� � ����� ������ � ���� � ������� �������� ������
	tm tim;
	time_t tt = time(NULL);
	error = localtime_s(&tim , &tt);
	if (error != 0)
		return false;
	DateTime dt(tim.tm_sec, tim.tm_min, tim.tm_hour, tim.tm_mday, tim.tm_mon, tim.tm_year);
	inputFile.write((char*)&dt, sizeDataTime);
	if (!inputFile)
		return false;
	// ����� � ���� ������ � ���� � ������� ����������� ������(������������ � ���������)
	inputFile.write((char*)&dt, sizeDataTime);
	if (!inputFile)
		return false;
	// ����� � ���� ���������� ����� � ������
	MSelectionList slist;  // ������ ��������
	MGlobal::getActiveSelectionList(slist); // ��������� ������ �������� �����������(���� export all - �� �������� ���)
	int length(slist.length());
	inputFile.write((char*)&length, sizeInt);
	if (!inputFile)
		return false;
	// ����� � ����� ����� ������(�����)
	//vector<MPoint> centers;
	vector< MBoundingBox> bboxArr;
	for (MItSelectionList iter(slist); !iter.isDone(); iter.next())
	{
		MDagPath path;
		iter.getDagPath(path);
		MFnMesh fnMesh(path); // ������ � ����
		MBoundingBox bbox = fnMesh.boundingBox(); // �������������� ����
		//centers.push_back(bbox.center()); // ���������� �����
		bboxArr.push_back(bbox); // ���������� bbox
	}
	// ������������ ����� �����
	MPoint center; // ������������ �����
	float xl(bboxArr[0].min().x), xr(bboxArr[0].max().x), yd(bboxArr[0].min().y), yu(bboxArr[0].max().y), zr(-bboxArr[0].min().z), zf(-bboxArr[0].max().z); // ������� �������. ������
	if (bboxArr.size() == 1)
	{
		center = bboxArr[0].center();
		center.z = -center.z;
	}
	else
	{
		for (auto&bb : bboxArr)
		{
			auto p = bb.min(); p.z = -p.z;
			auto p1 = bb.max(); p1.z = -p1.z;
			if (xl > p.x)
				xl = p.x;
			if (xr < p1.x)
				xr = p1.x;
			if (yd > p.y)
				yd = p.y;
			if (yu < p1.y)
				yu = p1.y;
			if (zr > p.z)
				zr = p.z;
			if (zf < p1.z)
				zf = p1.z;
		}
		center = { (xr - xl) / 2, (yu - yd) / 2, (zf - zr) / 2 };
	}
	float cx = center.x;
	inputFile.write((char*)&cx, sizeFloat);
	if (!inputFile)
		return false;
	float cy = center.y;
	inputFile.write((char*)&cy, sizeFloat);
	if (!inputFile)
		return false;
	float cz = center.z;
	inputFile.write((char*)&cz, sizeFloat);
	if (!inputFile)
		return false;
	// ����� � ����� ������� ���������� BBox(������ ��� �������� ������)
	//xl--; yd--; zf--; xr++; yu++; zr++;
	inputFile.write((char*)&xl, sizeFloat);
	if (!inputFile)
		return false;
	inputFile.write((char*)&yd, sizeFloat);
	if (!inputFile)
		return false;
	inputFile.write((char*)&zf, sizeFloat);
	if (!inputFile)
		return false;
	inputFile.write((char*)&xr, sizeFloat);
	if (!inputFile)
		return false;
	inputFile.write((char*)&yu, sizeFloat);
	if (!inputFile)
		return false;
	inputFile.write((char*)&zr, sizeFloat);
	if (!inputFile)
		return false;
	// ���������� ������� ��������� � ������ ����
	MString unitName;
	setToLongUnitName(MDistance::uiUnit(), unitName);
	if (!writeLineFromFile(unitName.asChar()))
		return false;

	string out = "Write specified information write: succesfull.";
	printMessageToLog(out);
	return true;
}

//////////////////////////////////////////////////////////////
bool pObjectPlugin::writeMaterialsFromFile()
{
	int block = pObjFormatBlocks::materialsInfo;
	inputFile.write((char*)&block, sizeInt ); // ����� ������� ����(�������)
	if (!inputFile)
		return false;
	int s = bufMaterials.size();
	inputFile.write((char*)&s, sizeInt); // ����� ���-�� ����������
	if (!inputFile)
		return false;
	for (auto& t : bufMaterials)
	{
		tmpIndecesMaterialBuffer.push_back(t.second);
		if (!inputFile)
			return false;
		if (!writeLineFromFile(t.first.nameMaterial.c_str())) // ����� �������� ���������
			return false;
		if (!writeLineFromFile(t.first.nameTexture.c_str())) // ����� ���� � ��������
			return false;
		if (!writeLineFromFile(t.first.valueMaterial.c_str())) // ����� ��������
			return false;
		inputFile.write((char*)&t.first.doubleSides, sizeBoolean); // ����� ���� �����
		if (!inputFile)
			return false;
	}
	string out = "Wtite materials:  " + to_string(s) + ". Succesfull.";
	printMessageToLog(out);
	return true;
}

//////////////////////////////////////////////////////////////
bool pObjectPlugin::writeLineFromFile(const char* line)
{
	for (int i(0); i < strlen(line); i++)
	{
		inputFile.write((char*)&line[i], sizeChar); // ����� �� �������
		if (!inputFile)
			return false;
	}
	char null('\0');
	inputFile.write((char*)&null, sizeChar); // ����� ���� - ��������
	if (!inputFile)
		return false;
	return true;
}

//////////////////////////////////////////////////////////////

bool pObjectPlugin::writeVertexsFromFile()
{
	int block = pObjFormatBlocks::vertexInfo;
	inputFile.write((char*)&block, sizeInt ); // ����� ������� ����(�������)
	if (!inputFile)
		return false;
	int s = arrayVertex.size();
	inputFile.write((char*)&s, sizeInt); // ���������� ������
	if (!inputFile)
		return false;
	map<int, Vertex> vertexWrited;
	for_each(arrayVertex.begin(), arrayVertex.end(), [&vertexWrited](const auto& v)
		{
			vertexWrited.insert({ v.second, v.first });
		}
	);
	for (int i(0); i < vertexWrited.size(); i++)
	{
		auto v = vertexWrited[i]; // �������� �������
		auto n = arrayNormal[v];

		Vector3 n_result;
		for (auto& _n : n)
			n_result += _n;
		v.normals = n_result * (1.f / n.size());
		
		//v.normal = n.normal * (1.f /  n.count); // ������������ �� �������

		//ver.normal.z = -ver.normal.z;
		
		inputFile.write((char*)&v, sizeVertex); // ����� ������ // v
		if (!inputFile)
			return false;
	}
	string out = "Write vertexs:  " + to_string(s) + ". Successfull";
	printMessageToLog(out);
	return true;
}

//////////////////////////////////////////////////////////////

bool pObjectPlugin::writeMeshesFromFile()
{
	int block = pObjFormatBlocks::meshesInfo;
	int blockMesh = block * 10;
	int blockIndecesBuffer;
	int allIndexs(0);

	inputFile.write((char*)&block, sizeInt ); // ����� ������� ����(�������)
	if (!inputFile)
		return false;
	for (int k(0), i(0); k < meshesName.size(); k++)
	{
		// ����� id ��������
		inputFile.write((char*)&blockMesh, sizeInt); // ����� ������� ����
		if (!inputFile)
			return false;
		blockMesh += 0x0001; // ����������� ����� �����
		blockIndecesBuffer = block * 1000; // �������� ����� ����� ��� ��������� �������

		inputFile.write((char*)&meshesIndeces[k], sizeInt); // ����� ���������� ��������� ������� ��� ����
		if (!inputFile)
			return false;
		//����� � ����� �� �������: ��� ����
		if (!writeLineFromFile(meshesName[k].c_str())) // ��� ����
			return false;

		for (int l(0); l < meshesIndeces[k]; l++)
		{
			// ����� id ��������
			inputFile.write((char*)&blockIndecesBuffer, sizeInt); // ����� ������� ����
			if (!inputFile)
				return false;

			int indexIndeces = tmpIndecesMaterialBuffer[i++];
			blockIndecesBuffer += 0x0001; // ����������� ����� �����

			// ����� � ����� ���-�� ��������, ����� �������� � ��� ��� ������ ��������
			int s = arrayIndeces[indexIndeces].size();
			inputFile.write((char*)&s, sizeInt); // ���������� ��������
			if (!inputFile)
				return false;
			allIndexs += arrayIndeces[indexIndeces].size();
			for (auto& ind : arrayIndeces[indexIndeces])
			{
				inputFile.write((char*)&ind, sizeInt); // ����� ��������
				if (!inputFile)
					return false;
			}
		}
	}
	printMessageToLog("Write meshes:  " + to_string(meshesName.size()) + ". Successfull.");
	printMessageToLog("Write indeces: " + to_string(allIndexs) + ". Successfull.");
	return true;
}

//////////////////////////////////////////////////////////////

bool pObjectPlugin::writeOptionsFromFile(const MString& mname, const MString & option)
{
	// ���������
	MString pso; // pipline state

	int block = pObjFormatBlocks::optionsInfo;
	inputFile.write((char*)&block, sizeInt); // ����� ������� ����(�������)
	if (!inputFile)
	{
		MStreamUtils::stdErrorStream() << "Error write data from " << mname.asChar() << " file.\n";
		return false;
	}
	if (option.length() > 0) // ��������� ����
	{
		MStringArray opt;
		option.split(L';', opt); 
		for(int i(0);i<opt.length();i++)
		{
			MStringArray currentOpt;
			opt[i].split(L'=', currentOpt);
			if (currentOpt[0] == "pso")
				pso = currentOpt[1];
		}
	}
	else
	{
		MStreamUtils::stdErrorStream() << "Error read option for export!\n";
		return false;
	}
	// ����� � ����
	if (!writeLineFromFile(pso.asChar())) // ����� �������� ���������
		return false;

	string out = "Write options model: successfull.";
	printMessageToLog(out);
	return true;
}

//////////////////////////////////////////////////////////////
void pObjectPlugin::clear()
{
	bufMaterials.clear(); // ����� ������� � ����������
	arrayIndeces.clear(); // ����� ��������
	arrayVertex.clear(); // ������ ������ ��� ���� ������, ����� ��������������� ������������ � ����
	arrayNormal.clear(); // ������ �������� ���� �� �����
	tmpIndecesMaterialBuffer.clear(); // ��������� ����� �������� ����������
	meshesName.clear(); // ����� �����
	meshesIndeces.clear(); // ��������� ������ ��� ����������� ����
	inputFile.close(); // ��������� ����
}

void pObjectPlugin::printMessageToLog(const string& m)
{
	string message = "scrollField -e -it \"";
	message += m.c_str();
	message += "\\n\"";
	message += nameLogVar.c_str();
	message += ";";
	CHECK_MSTATUS(MGlobal::executeCommand(message.c_str()));
}

void pObjectPlugin::initProperties()
{
	props =
	{
		{"$game_path", ""},
		{"$version_plugin", ""},
		{"$vendor_plugin", ""},
		{"$name_plugin", ""},
		{"$game_textures", ""}
	};
	if (!Utils::loadConfig(props))
	{
		isInit = false;
		return;
	}
}

//////////////////////////////////////////////////////////////
