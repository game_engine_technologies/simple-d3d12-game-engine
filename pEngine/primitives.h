#pragma once

#include "calculate.h"
#include "colors.h"

#include "../pEngineStatic/types.h"

//enum class TypePrimitive : int
//{
//	NoPrimitive = -1,
//	Plane = 0,
//	Cube,
//	Sphere,
//	Frustum
//};

class DYNLIB_PENGINE Primitive
{
	static void generateIndexPlane(vector<Index>& ind, int c); // ���������� ������� ��� ������� ���������
	static void generateIndexSphere(vector<Index>& ind, int count_usech, int count_h); // ���������� ������� ��� ������� �����
public:
	// planes
	static void generatePlane(const TypeRender& tr, vector<Vertex>& v, vector<Index>& ind, float w, int c, const Color& cl); // ������ ��������� � �����
	static void generatePlane(const TypeRender& tr, vector<Vertex>& v, vector<Index>& ind, float w, int c); // ������ ��������� � ���������
	static void generatePlaneNet(const TypeRender& tr, vector<Vertex>& v, vector<Index>& ind, float w, int c); // ������� ��������� - �����(��� ���������)
	// spheres
	static void generateSphere(const TypeRender& tr, vector<Vertex>& v, vector<Index>& ind, float r, int count_usech, int count_h, const Color& cl); // ������ ����� � �����
	static void generateSphere(const TypeRender& tr, vector<Vertex>& v, vector<Index>& ind, float r, int count_usech, int count_h); // ������ ����� � ���������
	static void generateSphereNet(const TypeRender& tr, vector<Vertex>& v, vector<Index>& ind, float r, int count_usech, int count_h); // ������� ����� - �����(��� ���������)
	// cubes
	static void generateCube(const TypeRender& tr, vector<Vertex>& v, vector<Index>& ind, float size, const Color& cl); // ������ ��� � �����
	static void generateCube(const TypeRender& tr, vector<Vertex>& v, vector<Index>& ind, float size); // ������ ��� � ��������
	static void generateCubeNet(const TypeRender& tr, vector<Vertex>& v, vector<Index>& ind, float size); // ������ ��� - �����(��� ���������)
};

// TODO: ���� ��������� ��������� �������� ������ ��� dx! ��������, ��������� ����������� � OpenGL