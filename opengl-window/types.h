#pragma once

// �++
#include <iostream>
#include <vector>
#include <memory>
#include <array>
#include <string>
#include <map>
#include <experimental/filesystem>
#include <fstream>
#include <regex>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// pEngine
#include "../math_graphics/math_graphics/mathematics.h"

// SOIL
#include <SOIL.h>

using namespace gmath;
using namespace gmtypes;
using namespace std;
namespace fs = experimental::filesystem;