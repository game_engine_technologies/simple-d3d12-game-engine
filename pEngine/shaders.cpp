#include "shaders.h"

wstring TypeShaders::vs(L"vs");
wstring TypeShaders::ps(L"ps");

PairShaders::PairShaders() {}
PairShaders::PairShaders(const wstring& vs, const wstring& ps) :vShader(vs), pShader(ps) {}
wstring& PairShaders::getVertexShaderPath() { return vShader; }
wstring& PairShaders::getPixelShaderPath() { return pShader; }

CombinedPairShaders::CombinedPairShaders(const wstring& s)
{
	vShader = s;
}
wstring& CombinedPairShaders::getPixelShaderPath() { return vShader; }