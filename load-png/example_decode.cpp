#include "lodepng.h"
#include <iostream>

//Example 1
//Decode from disk to raw pixels with a single function call
void decodeOneStep(const char* filename) 
{
  std::vector<unsigned char> image; //the raw pixels
  unsigned width, height;

  //decode
  unsigned error = lodepng::decode(image, width, height, filename);

  //if there's an error, display it
  if(error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;

  //the pixels are now in the vector "image", 4 bytes per pixel, ordered RGBARGBA..., use it as texture, draw it, ...
}

////Example 2
////Load PNG file from disk to memory first, then decode to raw pixels in memory.
//void decodeTwoSteps(const char* filename) {
//  std::vector<unsigned char> png;
//  std::vector<unsigned char> image; //the raw pixels
//  unsigned width, height;
//
//  //load and decode
//  unsigned error = lodepng::load_file(png, filename);
//  if(!error) error = lodepng::decode(image, width, height, png);
//
//  //if there's an error, display it
//  if(error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
//
//  //the pixels are now in the vector "image", 4 bytes per pixel, ordered RGBARGBA..., use it as texture, draw it, ...
//}
//
////Example 3
////Load PNG file from disk using a State, normally needed for more advanced usage.
//void decodeWithState(const char* filename) {
//  std::vector<unsigned char> png;
//  std::vector<unsigned char> image; //the raw pixels
//  unsigned width, height;
//  lodepng::State state; //optionally customize this one
//
//  unsigned error = lodepng::load_file(png, filename); //load the image file with given filename
//  if(!error) error = lodepng::decode(image, width, height, state, png);
//
//  //if there's an error, display it
//  if(error) std::cout << "decoder error " << error << ": "<< lodepng_error_text(error) << std::endl;
//
//  //the pixels are now in the vector "image", 4 bytes per pixel, ordered RGBARGBA..., use it as texture, draw it, ...
//  //State state contains extra information about the PNG such as text chunks, ...
//}

int main(int argc, char *argv[]) 
{
  const char* filename = "test1.png";
  decodeOneStep(filename);
}

