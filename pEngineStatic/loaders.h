#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <regex>
#include <map>
#include <array>
#include <functional>
#include <experimental/filesystem>
#include <tuple>
#include <numeric>

#include "pObjConstants.h"
#include "utils.h"
#include "types.h"

using namespace std;
namespace fs = experimental::filesystem;

struct PiplineStateObjectProperties
{
	struct InputLayout
	{
		string semantics;
		size_t countOperands;
	};
	size_t number;
	wstring name;
	vector< InputLayout> inputLayouts;
	wstring vsPath;
	wstring psPath;
	vector<wstring> cBuffNames;
	vector<wstring> sBuffNames;
};

struct MaterialProperties
{
	wstring name;
	array<float, 4> ambient;
	array<float, 4> diffuse;
	array<float, 4> specular;
	float shininess;

};

class PSOLoader
{
	static void writeError(wstring* errBuff, const wstring& error);
public:
	static vector<PiplineStateObjectProperties> load(bool& status, const wstring& cfg, wstring* errBuff = nullptr);
};

class MaterialLoader
{
	static void writeError(wstring* errBuff, const wstring& error);

public:
	static vector<MaterialProperties> load(bool& status, const wstring& cfg, wstring* errBuff = nullptr);

};

enum sizesByte : size_t
{
	sizeInt = sizeof(int),
	sizeFloat = sizeof(float),
	sizeBoolean = sizeof(bool),
	sizeDataTime = sizeof(DateTime),
	sizeChar = sizeof(char),
	sizeVertex = sizeof(Vertex)
};

class ModelLoader
{

	static bool  loadLineFromFile(string& line, std::ifstream& file);

	static bool loadFromFilePObject(const string& path, const TypeRender& tr, BlankModel* data);

	static bool loadFromFileObj(const string& path, const TypeRender& tr, BlankModel* data);

public:

	static bool loadFileFromExpansion(const string& fileName, const TypeRender& tr, BlankModel* data);
};

class ImageLoader
{
	// TGA
	enum TGAVersion : size_t
	{
		TGA_COMPRESSED = 0,
		TGA_UNCOMPRESSED
	};

	enum TGAUsefulHeader : size_t
	{
		TGA_WIDTH_0 = 1,
		TGA_WIDTH_1 = 0,
		TGA_HEIGHT_0 = 3,
		TGA_HEIGHT_1 = 2,
		TGA_BPP = 4
	};

	static bool loadFromFileTga(const string& path, Image* data)
	{
		ifstream file(path, ios::in | ios::binary);
		if (!file)
		{
			// error
			return false;
		}
		file.unsetf(std::ios::skipws); // ��������� ������� ��������
		TGAVersion version;
		if (!isTGA(file, version))
			return false;

		vector<_byte> usefulHeader;
		for (auto iter = istream_iterator<_byte>(file); iter != istream_iterator<_byte>(); ++iter)
		{
			usefulHeader.emplace_back(*iter);
			if (!file)
			{
				// error
				return false;
			}
			if (usefulHeader.size() == 6)
				break;
		}
		data->width = usefulHeader[TGA_WIDTH_0] * 256 + usefulHeader[TGA_WIDTH_1];  // ��������� ������
		data->height = usefulHeader[TGA_HEIGHT_0] * 256 + usefulHeader[TGA_HEIGHT_1];  // ��������� ������
		data->tgaInfo.bpp = usefulHeader[TGA_BPP]; // ��������� ���������� ��� �� �������
		if ((data->width <= 0) || (data->height <= 0) || ((data->tgaInfo.bpp != 24) && (data->tgaInfo.bpp != 32)))
		{
			// error
			return false;
		}
		if (data->tgaInfo.bpp == 24)        // ��� ����������� 24bpp?
			data->type = TI_RGB;
		else                          // ���� �� 24, ����� 32
			data->type = TI_RGBA;
		data->tgaInfo.bytesPerPixel = data->tgaInfo.bpp / 8; // ���� �� �������
		size_t imageSize = data->tgaInfo.bytesPerPixel * data->width * data->height; // ������ �����������
		data->imageData.reserve(imageSize);

		if (version == TGA_COMPRESSED)
			return loadCompressedTGA(file, data);
		else if (version == TGA_UNCOMPRESSED)
			return loadUncompressedTGA(file, data);
		return false; // ���� �������������� ��� TGA
	}

	static bool isTGA(ifstream& file, TGAVersion& version)
	{
		// ��������� ��������� TGA
		vector<_byte> headerTGAuncompare = { 0,0,2,0,0,0,0,0,0,0,0,0 };
		// ��������� ������� TGA
		vector<_byte> headerTGAcompare = { 0,0,10,0,0,0,0,0,0,0,0,0 };
		vector<_byte> fileHeader;
		file.seekg(0, std::ios::end);
		if (file.tellg() <= headerTGAuncompare.size())
			return false;
		file.clear();
		file.seekg(std::ios::beg);
		for (auto iter = istream_iterator<_byte>(file); iter != istream_iterator<_byte>(); ++iter)
		{
			fileHeader.emplace_back(*iter);
			if (!file)
			{
				// error
				return false;
			}
			if (fileHeader.size() == 12)
				break;
		}
		if (headerTGAcompare == fileHeader)
			version = TGA_COMPRESSED;
		else if (headerTGAuncompare == fileHeader)
			version = TGA_UNCOMPRESSED;
		else
			return false;
		return true;
	}

	static bool loadCompressedTGA(ifstream& file, Image* data)
	{
		size_t pixelcount = data->height * data->width;  // ���������� �������� � ����������
		size_t currentpixel(0);     // ������� � ������� �� ������ ���������
		size_t currentbyte(0);      // ���� ������� �� �������� � Imagedata
		// ��������� ��� ������ �������
		vector<_byte> colorBuffer(data->tgaInfo.bytesPerPixel);
		do
		{
			_byte chunkheader(0); // �������� ��� �������� �������������� ������
			// �������� ������� �������������� ������
			if (!file.read((char*)&chunkheader, sizeof(_byte)))
			{
				// error
				return false;
			}
			if (chunkheader < 128)   // ���� ������ �������� 'RAW' �������
			{
				chunkheader++;        // ��������� ������� ��� ��������� ���������� RAW ��������
				// ������ ����� ������ ��������
				for (size_t counter(0); counter < chunkheader; counter++)
				{
					for(auto&& pix: colorBuffer)
						if (!file.read((char*)&pix, sizeof(_byte)))
						{
							// error
							return false;
						}
					data->imageData.emplace_back(colorBuffer[2]);       // �������� ���� 'R'
					data->imageData.emplace_back(colorBuffer[1]); // �������� ���� 'G'
					data->imageData.emplace_back(colorBuffer[0]); // �������� ���� 'B'
					if (data->tgaInfo.bytesPerPixel == 4)          // ���� ��� 32bpp �����������...
						data->imageData.emplace_back(colorBuffer[3]);  // �������� ���� 'A'
					// ����������� ������� ������ �� �������� ������ ���������� ���� �� �������
					currentbyte += data->tgaInfo.bytesPerPixel;
					currentpixel++;          // ����������� ���������� �������� �� 1
				}
			}
			else // ���� ��� RLE �������������
			{
				chunkheader -= 127; // �������� 127 ��� ��������� ���������� ����������
				for (auto&& pix : colorBuffer)
					if (!file.read((char*)&pix, sizeof(_byte)))
					{
						// error
						return false;
					}
				for (short counter = 0; counter < chunkheader; counter++)
				{
					data->imageData.emplace_back(colorBuffer[2]);       // �������� ���� 'R'
					data->imageData.emplace_back(colorBuffer[1]); // �������� ���� 'G'
					data->imageData.emplace_back(colorBuffer[0]); // �������� ���� 'B'
					if (data->tgaInfo.bytesPerPixel == 4)          // ���� ��� 32bpp �����������...
						data->imageData.emplace_back(colorBuffer[3]);  // �������� ���� 'A'
					currentbyte += data->tgaInfo.bytesPerPixel;  // �������������� ������� ������
					currentpixel++;        // �������������� ������� ��������
				}
			}
		} while (currentpixel < pixelcount);
		return true;
	}

	static bool loadUncompressedTGA(ifstream& file, Image* data)
	{
		size_t imageSize = data->imageData.size();
		for (auto iter = istream_iterator<_byte>(file); iter != istream_iterator<_byte>(); ++iter) // BGR -> to RGB
		{
			data->imageData.push_back(*iter);
			if (!file)
			{
				// error
				return false;
			}
		}
		for (size_t i(0); i < imageSize; i += data->tgaInfo.bytesPerPixel)
			// ������ ���� XOR ������ ���� XOR ������ ���� XOR ������ ����
			data->imageData[i] ^= data->imageData[i + 2] ^= data->imageData[i] ^= data->imageData[i + 2];
		return true;
	}

	// png

	struct HeaderZlib
	{
		struct CMF
		{
			_bitset cinfo;
			_bitset cm;

			CMF():cinfo((size_t)4), cm((size_t)4){}
		} cmf;
		struct FLG
		{
			_bitset flevel;
			_bitset fdict;
			_bitset fcheck;

			FLG() :flevel((size_t)2), fdict((size_t)1), fcheck((size_t)5) {}
		}flg;
		_bitset dictid;

		HeaderZlib() :cmf(), flg(), dictid((size_t)32) {}
	};

	struct ColorModel
	{
		int countChannels;
		int bitsForChannels;

		void calculateModel(_byte depth, _byte color_type)
		{
			map<_byte, map<_byte, int>> tableBits =
			{
				{0, {{1,1}, {2, 2}, {4, 4}, {8, 8}, {16, 16}}},
				{2, {{8, 24}, {16, 48}}},
				{3, {{1,1}, {2, 2}, {4, 4}, {8, 8}}},
				{4, {{8, 16}, {16, 32}}},
				{6, {{8, 32}, {16, 64}}}
			};
			map<_byte, int> tabeChannels =
			{
				{0, 1},
				{2, 3},
				{3, 1},
				{4, 2},
				{6, 4}
			};
			countChannels = tabeChannels[color_type];
			bitsForChannels = tableBits[color_type][depth];
		}
	};

	enum TypeTableOffsets
	{
		TABLE_LENGTH = 0,
		TABLE_OFFSETS
	};

	struct ITableZlib
	{
		virtual int calculate(_bitset& block, size_t& i, int value) = 0;
	};

	struct TableLength: public ITableZlib
	{
		struct _data
		{
			int codeLeft; // l code
			int codeRight; // r code
			int bits; // dop bits
			int leftOffsets; // l offset
			int rightOffsets; // r offset
			int bitsOffsets; // bit count offset
		};

		vector < _data> dataLength;

		TableLength()
		{
			// stop code == 256, 286, 287
			// code 285 == 285
			dataLength =
			{
				{257, 264, 0, 3, 10, 0},
				{265, 268, 1, 11, 18, 2},
				{269, 272, 2, 19, 34, 4},
				{273, 276, 3, 35, 66, 8},
				{277, 280, 4, 67, 130, 16},
				{281, 284, 5, 131, 257, 32}
			};
		}

		int calculate(_bitset& block, size_t& i, int value) override
		{
			auto rangeCodes = std::find_if(dataLength.begin(), dataLength.end(), [&value](const _data& c)
				{
					return value >= c.codeLeft && value <= c.codeRight;
				});
			int assumeValue = rangeCodes->leftOffsets + (value - rangeCodes->codeLeft) + rangeCodes->bitsOffsets; // �������������� ��������
			// ���������� ����, ���� �����
			if (rangeCodes->bits != 0)
			{
				int stepOffset = block.to_number(i, rangeCodes->bits);
				cout << block.to_string(i, rangeCodes->bits) << endl;
				i += rangeCodes->bits;
				assumeValue += stepOffset;
			}
			return assumeValue;
		}

	};

	struct TableOffset : public ITableZlib
	{
		struct _data
		{
			int codeFirst; // l first
			int codeSecond; // r first
			int bits; // dop bits
			int leftOffsetsFirst; // l offset first
			int leftOffsetsSecond; // l offset second - not using
			int rightOffsetsFirst; // r offset first
			int rightOffsetsSecond; // r offset second - not using
		};

		vector < _data> dataBackOffsets;

		TableOffset()
		{
			dataBackOffsets =
			{
				{0, 3, 0, 1, 4, 1, 4},
				{4, 5, 1, 5, 6, 7, 8},
				{6, 7, 2, 9, 12, 13, 16},
				{8, 9, 3, 17, 24, 25, 32},
				{10, 11, 4, 33, 48, 49, 64},
				{12, 13, 5, 65, 96, 97, 128},
				{14, 15, 6, 129, 192, 193, 256},
				{16, 17, 7, 257, 384, 385, 512},
				{18, 19, 8, 513, 768, 769, 1024},
				{20, 21, 9, 1025, 1536, 1537, 2048},
				{22, 23, 10, 2049, 3072, 3073, 4096},
				{24, 25, 11, 4097, 6144, 6145, 8192},
				{26, 27, 12, 8193, 12288, 12289, 16384},
				{28, 29, 13, 16385, 24576, 24577, 32768} // 16285 ???
			};

		}

		int calculate(_bitset& block, size_t& i, int value) override
		{
			if (value == 285)
				return 285;
			auto rangeCodes = std::find_if(dataBackOffsets.begin(), dataBackOffsets.end(), [&value](const _data& c)
				{
					return value >= c.codeFirst && value <= c.codeSecond;
				});
			int assumeValue = value == rangeCodes->codeFirst ? rangeCodes->leftOffsetsFirst : rangeCodes->rightOffsetsFirst;
			// ���������� ����, ���� �����
			if (rangeCodes->bits != 0)
			{
				int stepOffset = block.to_number(i, rangeCodes->bits);
				cout << block.to_string(i, rangeCodes->bits) << endl;
				i += rangeCodes->bits;
				assumeValue += stepOffset;
			}
			return assumeValue;
		}
	};




	struct FixedHaffman
	{
		vector<tuple<int, int, int, int>> fixedHaffman;
		vector<int> sizeFixedHaffmanBits;

		FixedHaffman()
		{
			fixedHaffman =
			{
				{256, 279, 0, 23}, // 7 bit | code symbol {l, r}, raage {l, r}
				{0, 143, 48, 191}, // 8 bit
				{280, 287, 192, 199}, // 8 bit
				{144, 255, 400, 511}, // 9 bit
			};
			sizeFixedHaffmanBits = { 7,8,8,9 };
		}

		int calculate(_bitset& block, size_t& i)
		{
			for (size_t j(0); j < sizeFixedHaffmanBits.size(); j++)
			{
				int num = block.to_number(i, sizeFixedHaffmanBits[j]);
				auto [leftCode, rightCode, leftRange, rightRange] = fixedHaffman[j];
				if (num >= leftRange && num <= rightRange)
				{
					i += sizeFixedHaffmanBits[j]; // ����� �� ������ �����
					return num - leftRange + leftCode; 
				}
			}
			return -1;
		}
	};

	struct BlockZlib
	{
		_bitset bfinal;
		_bitset haffmanCode;

		BlockZlib():bfinal((size_t)1), haffmanCode((size_t)2){}
	};

	static bool loadFromFilePng(const string& path, Image* data)
	{
		ifstream file(path, ios::in | ios::binary);
		if (!file)
		{
			// error
			return false;
		}
		file.unsetf(std::ios::skipws); // ��������� ������� ��������
		if (!isPNG(file))
			return false;
		map < string, function<bool(ifstream & file, size_t, Image*)> > chuncksReader =
		{
			{
				"IHDR", [](ifstream& file, size_t sizeChunck, Image* data) // header chunck
				{
					unsigned long int w(0); // width
					file.read((char*)&w, 4);
					if (!file)
						return false;
					data->width =
						((w & 0x000000FF) << 24) |
						((w & 0x0000FF00) << 8) |
						((w & 0x00FF0000) >> 8) |
						((w & 0xFF000000) >> 24);
					unsigned long int h(0); // height
					file.read((char*)&h, 4);
					if (!file)
						return false;
					data->height =
						((h & 0x000000FF) << 24) |
						((h & 0x0000FF00) << 8) |
						((h & 0x00FF0000) >> 8) |
						((h & 0xFF000000) >> 24);
					file.read((char*)&data->pngInfo.depthBit, 1);
					if (!file)
						return false;
					file.read((char*)&data->type, 1);
					if (!file)
						return false;
					file.read((char*)&data->pngInfo.compressedMethod, 1);
					if (!file)
						return false;
					if (data->pngInfo.compressedMethod != 0)
						return false;
					file.read((char*)&data->pngInfo.filterMethod, 1);
					if (!file)
						return false;
					if (data->pngInfo.filterMethod != 0)
						return false;
					file.read((char*)&data->pngInfo.interlaceMethod, 1);
					if (!file)
						return false;
					int crc(-1); // ����������� �����, ���� not using
					file.read((char*)&crc, 4);
					if (!file)
						return false;
					return true;
				}
			},
			{
				"IDAT", [](ifstream& file, size_t sizeChunck, Image* data) // data chunck
				{
					if (!file.read((char*)data->pngInfo.buffCompressedImage.data_add_last(sizeChunck), sizeof(_byte) * sizeChunck)) // ����� �������� ����� �������������
						return false;
					int crc(-1); // ����������� �����, ���� not using
					file.read((char*)&crc, 4);
					if (!file)
						return false;
					return true;
				}
			}
		};
		while (true)
		{
			unsigned long int s(0); // read length chunck
			file.read((char*)&s, 4);
			if (file.eof())
				break;
			if (!file)
				return false;
			unsigned long int size = 
				 ((s & 0x000000FF) << 24) |
				((s & 0x0000FF00) << 8) |
				((s & 0x00FF0000) >> 8) |
				((s & 0xFF000000) >> 24);

			array<char, 4> nameChunck; // read name chunck
			for (auto&& nc : nameChunck)
				if (!file.read((char*)&nc, sizeof(_byte)))
					return false;
			string _name(nameChunck.begin(), nameChunck.end());
			cout << _name << endl;
			if (chuncksReader.find(_name) != chuncksReader.end())
			{
				if (!chuncksReader[_name](file, size, data))
					return false;
			}
			else
			{
				file.seekg(size + sizeof(int), ios::cur);
				if (!file)
					return false;
			}
		}
		if (!decodeBlockImage(data)) // create png
			return false;
		return true;
	}

	static bool isPNG(ifstream& file)
	{
		// ��������� PNG
		vector<_byte> headerPNG = { 0x89, 0x50, 0x4E,  0x47, 0x0D, 0x0A, 0x1A,  0x0A };
		vector<_byte> fileHeader;
		file.seekg(0, std::ios::end);
		if (file.tellg() <= headerPNG.size())
			return false;
		file.clear();
		file.seekg(std::ios::beg);
		for (auto iter = istream_iterator<_byte>(file); iter != istream_iterator<_byte>(); ++iter)
		{
			fileHeader.emplace_back(*iter);
			if (!file)
			{
				// error
				return false;
			}
			if (fileHeader.size() == headerPNG.size())
				break;
		}
		if (fileHeader == headerPNG)
			return true;
		return false;
	}

	static bool isHeaderZlib(_bitset& _block, HeaderZlib* header, size_t& i)
	{
		for (; i < 4; i++)
			header->cmf.cinfo.set(i, _block[i]);
		cout << header->cmf.cinfo.to_string() << endl;

		for (; i < 8; i++)
			header->cmf.cm.set(i - 4, _block[i]);
		cout << header->cmf.cm.to_string()<< endl;

		int num = header->cmf.cm.to_number(0, 4);
		if (num != 8) // �� zlib
			return false;

		header->flg.flevel.set(0, _block[i++]);
		header->flg.flevel.set(1, _block[i++]);
		cout << header->flg.flevel.to_string() << endl;

		header->flg.fdict.set(0, _block[i++]);
		cout << header->flg.fdict.to_string() << endl;

		for (int j(0); j < 5; j++)
			header->flg.fcheck.set(j, _block[i++]);
		cout << header->flg.fcheck.to_string() << endl;

		if (header->flg.fdict[0]) // ������� - ���� png ��� ����, �� �� �� ���������� (��������� ��������)
		{
			// 2 3 4 5 ����� ( � ����� ������������������)
			for (int j(0); j < 32; j++)
				header->dictid.push_back_byte(_block[i++]);
			cout << header->dictid.to_string() << endl;
		}

		return true;
	}

	static bool decodeBlockImage(Image* data)
	{
		_bitset& block = data->pngInfo.buffCompressedImage;

		HeaderZlib zlib;
		size_t i(0);

		if (!isHeaderZlib(block, &zlib, i))
			return false;
		block.reverse();
		cout << block.to_string(i, 16) << endl;

		ColorModel colorModel;
		colorModel.calculateModel(data->pngInfo.depthBit, data->type);

		// fixed Haffman
		FixedHaffman fixedHaffman;
		// table length & table offset
		TableLength tableLength;
		TableOffset tableOffset;
		// size image
		int width = data->width;
		// ��� ������ ����� �� ������ ���� �� ��������� �����������
		vector<_byte> readedValue; 

		auto calculatePixel = [&data](vector<_byte>& colors, bool isDoubleChannels)
		{
			if (isDoubleChannels)
			{
				for (size_t j(0), k(0); j < colors.size(); j += 2, k++)
				{
					_bitset num;
					num.push_back_byte(colors[j]);
					num.push_back_byte(colors[j + 1]);
					int value = ceil((255 * num.to_number(0, 16)) / 65535);
					data->imageData.push_back(value);
				}
			}
			else
			{
				for (size_t j(0); j < colors.size(); j++)
					data->imageData.push_back(colors[j]);
			}
		};

		auto loadPixel = [&fixedHaffman, &block, &colorModel, &i, &data, &readedValue, &calculatePixel](int start, int num) -> bool // load pixel ������ ���
		{
			int countBitForPixel(colorModel.bitsForChannels);
			int countChannels(colorModel.countChannels);
			bool isDoubleChannels = countBitForPixel / 8 > 4 ? true : false; // ����������, ����������� �� ������
			vector<_byte> colors(countBitForPixel / 8);
			colors[0] = num;
			readedValue.push_back(num);
			for (size_t k(start); k < colors.size(); k++)
			{
				colors[k] = fixedHaffman.calculate(block, i);
				readedValue.push_back(colors[k]);
			}
			calculatePixel(colors, isDoubleChannels);
			return true;
		};

		auto loadPixelOffsets = [&loadPixel , &tableLength , &tableOffset, &block, &i, &readedValue, &data, &calculatePixel, &colorModel](int num) -> bool
		{
			int countBitForPixel(colorModel.bitsForChannels);
			bool isDoubleChannels = countBitForPixel / 8 > 4 ? true : false; // ����������, ����������� �� ������

			int lenght = tableLength.calculate(block, i, num);
			int _backOffset = block.to_number(i, 5);
			//cout << block.to_string(i, 5) << endl;
			i += 5;
			int backOffset = tableOffset.calculate(block, i, _backOffset); 
			auto iterStart = readedValue.begin() + readedValue.size() - backOffset;

			vector<_byte> colors;
			if (backOffset == 1)
			{
				colors.resize(lenght);
				for (size_t k(0); k < lenght; ++k)
				{
					colors[k] = *iterStart;
					data->imageData.push_back(colors[k]);
				}
			}
			else 
			{
				colors = { iterStart, iterStart + lenght };
				calculatePixel(colors, isDoubleChannels);
			}
			copy(colors.begin(), colors.end(), back_inserter(readedValue));

			return true;
		};

		BlockZlib compressedZlibHeader;
		while (true)
		{
			compressedZlibHeader.bfinal.set(0, block[i++]);
			compressedZlibHeader.haffmanCode.set(0, block[i++]);
			compressedZlibHeader.haffmanCode.set(1, block[i++]);

			int codeHaffman = compressedZlibHeader.haffmanCode.to_bitset_reverse(0, 2).to_number(0, 2);
			if (codeHaffman == 0) // no compressed
			{
				i = i + (8 -  i % 8);
				int len = block.to_number(i, 16); i += 16;
				int nlen = block.to_number(i, 16); i += 16;
				if (len == 0)
					break;
				else
				{
					// ������ �� ������ ������, ��� ������ - ���� �� ����
					// �������: len - ��� ���������� ���� ��� ����������, ������ ������ ��� ��� �����
					// � �� 4 ���� ��������� �������
					// � ������ len % 4 == 0 (!!!)
					// ���� �������� �� ����� ����������� - ��������������
					for (size_t k(0); k < len; ++k)
					{
						_byte color = block.to_number(i, 8); i += 8;
						data->imageData.push_back(color);
					}
				}
			}
			else if (codeHaffman == 1) // fixed code haffman 
			{
				bool isEnd(true);
				while (isEnd)
				{
					int filter = fixedHaffman.calculate(block, i);
					readedValue.push_back(filter);
					if (filter == 256)
						break;
					for (int j(width); j > 0; j--)
					{
						// ����������, ��� ����� ���������
						int num = fixedHaffman.calculate(block, i);
						if (num == 256)
							isEnd = false;
						else if (num > 257 && num < 285)
						{
							if (num != 286 && num != 287)
								isEnd = loadPixelOffsets(num); // �������� ����������� ������ �� ��������� ��������
							else
								isEnd = loadPixel(1, num); // �������� ����������� ������ �� ������ (��� 286 � 287)
						}
						else
							isEnd = loadPixel(1, num); // �������� ����������� ������ �� ������
						if (!isEnd)
							break;
					}
				}
			}
			else if (codeHaffman == 2) // dynamic code haffman
			{
				// TODO
			}
			else //reserved == error
				return false;
			if (compressedZlibHeader.bfinal[0] != 0) // this last block data
				break;
		}

		return true;
	}

public:
	static bool loadFileFromExpansion(const string& fileName, Image* data)
	{
		map<string, int> commands = { {".tga", 0}, {".png", 1} };
		int num(-1);
		auto exp = fs::path(fileName).extension().string();
		std::transform(exp.begin(), exp.end(), exp.begin(),
			[](unsigned char c) { return std::tolower(c); });
		auto iter = commands.find(exp);
		if (iter != commands.end())
			num = iter->second;
		switch (num)
		{
		case 0:
			return loadFromFileTga(fileName, data); // �������� *.tga

		case 1:
			return loadFromFilePng(fileName, data); // �������� *.png


		default:
		{
			// TODO: �������� ���������� �����, �� ������, ����� ��������� � ���
			return false;
		}
		}
	}
};

// png:
// ��������� ������� - �� �������: �����������, ��� ��������� ������ ������ png