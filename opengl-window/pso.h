#pragma once

#include "shaders.h"
#include "textures.h"

#include "../pEngineStatic/loaders.h"
#include "../pEngineStatic/pso.h"

struct InputLayoutOpenGL
{
	size_t sizeMember; // size member(3, 4 etc)
	size_t shiftBytesCurrent; // shift bytes current
	size_t shiftBytesAll; // shift bytes all

	InputLayoutOpenGL() {}
	InputLayoutOpenGL(size_t sm, size_t sbc, size_t sba) : sizeMember(sm), shiftBytesCurrent(sbc), shiftBytesAll(sba) {}
};

class PipelineStateObject
{
	vector< InputLayoutOpenGL> inputLayout;
	vector< InputLayoutOpenGL> inputLayoutInstanced;
	vector<wstring> cBufferKeys;
	vector<wstring> sBufferKeys;
	GLuint shaderProgram;

	vector<InputLayoutOpenGL> getInputLayoutOpenGLInstanced(const wstring& name)
	{
		map<wstring, vector<InputLayoutOpenGL>> data =
		{
				{L"sbInstanced", 
				{
					{InputLayoutOpenGL(4, 0, 64)}, // worlds
					{InputLayoutOpenGL(4, 16, 64)},
					{InputLayoutOpenGL(4, 32, 64)},
					{InputLayoutOpenGL(4, 48, 64)},
					{InputLayoutOpenGL(4, 0, 64)}, // normals
					{InputLayoutOpenGL(4, 16, 64)},
					{InputLayoutOpenGL(4, 32, 64)},
					{InputLayoutOpenGL(4, 48, 64)},
				}
			}
		};
		// todo: add error
		return data[name];
	}

public:

	bool initPipelineStateObject(ContainerShader& shaders, const PiplineStateObjectProperties& psoProp)
	{
		createInputLayout(psoProp);
		if (!createShaderProgram(shaders, psoProp))
			return false;
		return true;
	}

	void createInputLayout(const PiplineStateObjectProperties& psoProp)
	{
		PsoInputAssemblerDataCreator creator;
		size_t allShift(0);
		size_t shiftBytes(0);
		size_t i(0);
		for (; i < psoProp.inputLayouts.size(); i++)
			allShift += psoProp.inputLayouts[i].countOperands * sizeof(float);

		i = 0;
		for (; i < psoProp.inputLayouts.size(); i++)
		{
			inputLayout.emplace_back(psoProp.inputLayouts[i].countOperands, shiftBytes, allShift);
			shiftBytes += psoProp.inputLayouts[i].countOperands * sizeof(float);
		}

		for (auto&& sb : psoProp.sBuffNames)
		{
			auto ils = getInputLayoutOpenGLInstanced(sb);
			for (auto&& il : ils)
			{
				inputLayoutInstanced.emplace_back(il.sizeMember, il.shiftBytesCurrent, il.shiftBytesAll);
			}
		}
	}

	bool createShaderProgram(ContainerShader& shaders, const PiplineStateObjectProperties& psoProp)
	{
		GLint success;
		GLchar infoLog[512];
		shaderProgram = glCreateProgram();
		GLuint _vs = shaders.getVertexShader(psoProp.vsPath).getShader();
		GLuint _ps = shaders.getPixelShader(psoProp.psPath).getShader();
		glAttachShader(shaderProgram, _vs);
		glAttachShader(shaderProgram, _ps);
		glLinkProgram(shaderProgram);
		glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);

		if (!success) 
		{
			glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
			return false;
		}
		return true;
		// todo: ������� ������� ����� �������� ��������� ��������� (���������)
	}

	void addCBufferKey(const wstring& keyCB)
	{
		this->cBufferKeys.push_back(keyCB);
	}
	void addSBufferKey(const wstring& keySB)
	{
		this->sBufferKeys.push_back(keySB);
	}
	vector<wstring>& getCBufferKeys() { return cBufferKeys; }
	vector<wstring>& getSBufferKeys() { return sBufferKeys; }
	vector< InputLayoutOpenGL>& getInputLayout() { return inputLayout; }
	vector< InputLayoutOpenGL>& getInputLayoutInstanced() { return inputLayoutInstanced; }
	GLuint getShaderProgram() { return shaderProgram; }
};

using PipelineStateObjectPtr = shared_ptr< PipelineStateObject>;
