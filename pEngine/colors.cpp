#include "colors.h"

Color Colors::black(0, 0, 0, 1);
Color Colors::blue(0, 0, 1, 1);
Color Colors::green(0, 1, 0, 1);
Color Colors::red(1, 0, 0, 1);
Color Colors::white(1, 1, 1, 1);
Color Colors::corall(0.67f, 0.35, 0.24f, 1.f);
Color Colors::darkCorall(0.48f, 0.24f, 0.16f, 1.f);