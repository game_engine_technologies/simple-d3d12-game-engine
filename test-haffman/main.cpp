//#include <stdio.h>
//#include <stdlib.h>
//#include "huffman.h"

#include "../pEngineStatic/types.h"

#include <map>
#include <vector>
#include <iostream>
#include <set>

using namespace std;

enum TypeTableOffsets
{
	TABLE_LENGTH = 0,
	TABLE_OFFSETS
};

struct ITableZlib
{
	virtual int calculate(_bitset& block, size_t& i, int value) = 0;
};

struct TableLength : public ITableZlib
{
	struct _data
	{
		int codeLeft; // l code
		int codeRight; // r code
		int bits; // dop bits
		int leftOffsets; // l offset
		int rightOffsets; // r offset
		int bitsOffsets; // bit count offset
	};

	vector < _data> dataLength;

	TableLength()
	{
		// stop code == 256, 286, 287
		// code 285 == 285
		dataLength =
		{
			{257, 264, 0, 3, 10, 0},
			{265, 268, 1, 11, 18, 2},
			{269, 272, 2, 19, 34, 4},
			{273, 276, 3, 35, 66, 8},
			{277, 280, 4, 67, 130, 16},
			{281, 284, 5, 131, 257, 32}
		};
	}

	int calculate(_bitset& block, size_t& i, int value) override
	{
		auto rangeCodes = std::find_if(dataLength.begin(), dataLength.end(), [&value](const _data& c)
			{
				return value >= c.codeLeft && value <= c.codeRight;
			});
		int assumeValue = rangeCodes->leftOffsets + (value - rangeCodes->codeLeft) + rangeCodes->bitsOffsets; // �������������� ��������
		// ���������� ����, ���� �����
		if (rangeCodes->bits != 0)
		{
			int stepOffset = block.to_number(i, rangeCodes->bits);
			cout << block.to_string(i, rangeCodes->bits) << endl;
			i += rangeCodes->bits;
			assumeValue += stepOffset;
		}
		return assumeValue;
	}

};

struct TableOffset : public ITableZlib
{
	struct _data
	{
		int codeFirst; // l first
		int codeSecond; // r first
		int bits; // dop bits
		int leftOffsetsFirst; // l offset first
		int leftOffsetsSecond; // l offset second - not using
		int rightOffsetsFirst; // r offset first
		int rightOffsetsSecond; // r offset second - not using
	};

	vector < _data> dataBackOffsets;

	TableOffset()
	{
		dataBackOffsets =
		{
			//{0, 3, 0, 1, 4, 1, 4},
			{0, 0, 0, 1, 1, 1, 1},
			{1, 1, 0, 2, 2, 2, 2},
			{2, 2, 0, 3, 3, 3, 3},
			{3, 3, 0, 4, 4, 4, 4},
			{4, 5, 1, 5, 6, 7, 8},
			{6, 7, 2, 9, 12, 13, 16},
			{8, 9, 3, 17, 24, 25, 32},
			{10, 11, 4, 33, 48, 49, 64},
			{12, 13, 5, 65, 96, 97, 128},
			{14, 15, 6, 129, 192, 193, 256},
			{16, 17, 7, 257, 384, 385, 512},
			{18, 19, 8, 513, 768, 769, 1024},
			{20, 21, 9, 1025, 1536, 1537, 2048},
			{22, 23, 10, 2049, 3072, 3073, 4096},
			{24, 25, 11, 4097, 6144, 6145, 8192},
			{26, 27, 12, 8193, 12288, 12289, 16384},
			{28, 29, 13, 16385, 24576, 24577, 32768} // 16285 ???
		};

	}

	int calculate(_bitset& block, size_t& i, int value) override
	{
		if (value == 285)
			return 285;
		auto rangeCodes = std::find_if(dataBackOffsets.begin(), dataBackOffsets.end(), [&value](const _data& c)
			{
				return value >= c.codeFirst && value <= c.codeSecond;
			});
		int assumeValue = value == rangeCodes->codeFirst ? rangeCodes->leftOffsetsFirst : rangeCodes->rightOffsetsFirst;
		// ���������� ����, ���� �����
		if (rangeCodes->bits != 0)
		{
			int stepOffset = block.to_number(i, rangeCodes->bits);
			cout << block.to_string(i, rangeCodes->bits) << endl;
			i += rangeCodes->bits;
			assumeValue += stepOffset;
		}
		return assumeValue;
	}
};


void test_load_png()
{
	// table length & table offset
	TableLength tableLength;
	TableOffset tableOffset;

	// 1) ������
	// ��� ������ - to_bitset_reverse �� ������������
	/*short bytes[] =
	{
		0x05,
		0xA0,
		0x61,
		0x0B,
		0x00,
		0x00,
		0x18,
		0x86,
		0x2E,
		0x68,
		0x8F,
		0xEA,
		0xD9,
		0x3D,
		0xAE,
		0xC7,
		0xB4,
		0x9B,
		0x10,
		0xE8,
		0xFF,
		0x40,
		0x21,
		0x07,
		0x14,
		0x56,
		0x5A,
		0xDA
	};*/

	// ��� ������ - to_bitset_reverse ������������
	short bytes[] =
	{
		0x15, 0x8d, 0x51, 0x0a, 0xc0, 0x20, 0x0c, 0x43, 0xff, 0x3d, 0x45, 0xae, 0x56, 0x67, 0xdd, 0x8a, 0x5d, 0x0b, 0xd5, 0x21, 0xde, 0x7e, 0x0a, 0xf9, 0x08, 0x21, 0x2f, 0xc9, 0x4a, 0x57, 0xcb, 0x12, 0x05, 0x5d, 0xec, 0xde, 0x82, 0x18, 0xc6, 0xc3, 0x28, 0x4c, 0x05, 0x5e, 0x61, 0x72, 0x3f, 0x23, 0x0d, 0x6a, 0x7c, 0xe2, 0xce, 0xc8, 0xe1, 0x8d, 0x0d, 0x73, 0x77, 0x3b, 0xc8, 0x0a, 0x94, 0x29, 0x36, 0xe3, 0xa8, 0xba, 0x12, 0xa9, 0x62, 0xf9, 0x17, 0x50, 0xa9, 0x9c, 0xb6, 0xc3, 0xe4, 0x60, 0xb8, 0xe9, 0xc2, 0x24, 0x19, 0xe7, 0xa1, 0x7a, 0xec, 0x2d, 0xe9, 0x78, 0xfd, 0x65, 0x1b, 0x07, 0xa5, 0x90, 0xce, 0xe9, 0x07
	};
	

	_bitset block;
	for (auto&& b : bytes)
	{
		short number(b);
		block.push_back_byte(number);
	}
	block.reverse();
	size_t i = 0;

	// 2) ������ ���������
	int header = block.to_number(i, 3); i += 3;
	int hlit = block.to_bitset_reverse(i, 5).to_number(0, 5) + 257; i += 5;
	int hdist = block.to_bitset_reverse(i, 5).to_number(0, 5) + 1; i += 5;
	int hclen = block.to_bitset_reverse(i, 4).to_number(0, 4) + 4; i += 4;

	// 3) ������� �����
	vector<short> tableValue = { 16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15 };
	map<short, vector<short>> tableCount;
	for (size_t k(0); k < hclen; ++k)
	{
		short v = block.to_bitset_reverse(i, 3).to_number(0, 3); i += 3;
		if (v == 0)
			continue;
		tableCount[v].push_back(tableValue[k]);
	}

	// 4) ����������� ���� �� �������
	auto comparatorTblGroupped = [](pair<short, short> a, pair<short, short> b) -> bool
	{
		if (a.first < b.first)
			return true;
		if (a.first > b.first)
			return false;
		return a.second < b.second;
	};
	set<pair<short, short>, decltype(comparatorTblGroupped)> tableGroupped(comparatorTblGroupped);
	for (auto tc : tableCount)
		for (auto v : tc.second)
			tableGroupped.insert({ tc.first, v });
	for (auto&& tbl : tableGroupped)
		cout << tbl.first << " --- " << tbl.second << endl;

	// 5) �������� ������ ���� 
	auto comparator�odes = [](_bitset _a, _bitset _b) -> bool
	{
		// return _a.to_number(0, _a.size_bits()) < _b.to_number(0, _b.size_bits());
		return _a.to_string() < _b.to_string();
	};
	using type_codes = map<_bitset, short, decltype(comparator�odes)>;
	type_codes codes(comparator�odes);
	_bitset bitCode;
	for(int j(0); j < tableGroupped.begin()->first; j++)
		bitCode.push_back_bit(0);
	short currentCount(bitCode.size_bits());
	for (auto tg = tableGroupped.begin(); tg != tableGroupped.end(); ++tg)
	{
		short _count(-1);
		if (tg->first != currentCount)
		{
			currentCount = tg->first;
			_count = currentCount - 1;
			bitCode.push_back_bit(0);
		}
		else
			_count = currentCount;
		short num_old = bitCode.to_number(0, _count);
#pragma region DEBUG
		cout << "old num: " << num_old << endl;
#pragma endregion
		if (tg != tableGroupped.begin())
		{
			num_old++;
			for (short k(_count - 1), b(0); k >= 0; --k, ++b)
				bitCode.set(k, num_old & (1 << b));
#pragma region DEBUG
			short num_new = bitCode.to_number(0, currentCount > _count ? currentCount : _count);
			cout << "new num: " << num_new << endl;
#pragma endregion
		}
		cout << endl;
		codes[bitCode] = tg->second;
	}
	for (auto c : codes)
		cout << c.first.to_string() << " ---> " << c.second << endl;
	bitCode.clear();

	// 6) ������ ������������������ ��� �������� � ����
	_bitset currentSequence;
	map<short, vector<int>> codeSymbols; // ���� �������� � ����
	int bufShift(0);
	while (true)
	{
		currentSequence.push_back_bit(block.get(i++));
		type_codes::iterator _code = codes.find(currentSequence);
		if (_code != codes.end())
		{
			// ������ ������
			_bitset addBit;
			if (_code->first.size_bits() == 2 && _code->first.to_number(0, 2) == 0)
			{
				++bufShift;
				if (bufShift >= hlit)
					break;
			}
			else if (_code->second == 16)
			{
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				bufShift += (3 + addBit.to_bitset_reverse(0, 2).to_number(0, 2));
				if (bufShift >= hlit)
					break;
			}
			else if (_code->second == 17)
			{
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				bufShift += (3 + addBit.to_bitset_reverse(0, 3).to_number(0, 3));
				if (bufShift >= hlit)
					break;
			}
			else if (_code->second == 18)
			{
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				bufShift += (11 + addBit.to_bitset_reverse(0, 7).to_number(0, 7));
				if (bufShift >= hlit)
					break;
			}
			else
			{
				codeSymbols[_code->second].push_back(bufShift);
				++bufShift;
				if (bufShift >= hlit)
					break;
			}
			currentSequence.clear();
		}
	}
	currentSequence.clear();

	// 7) ������ ������������������ ����� ��������
	map<short, vector<int>> codeOffsets; // ���� ��������
	bufShift = 0;
	while (true)
	{
		currentSequence.push_back_bit(block.get(i++));
		type_codes::iterator _code = codes.find(currentSequence);
		if (_code != codes.end())
		{
			// ������ ������
			_bitset addBit;
			if (_code->first.size_bits() == 2 && _code->first.to_number(0, 2) == 0)
			{
				++bufShift;
				if (bufShift >= hdist)
					break;
			}
			else if (_code->second == 16)
			{
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				bufShift += (3 + addBit.to_bitset_reverse(0, 2).to_number(0, 2));
				if (bufShift >= hdist)
					break;
			}
			else if (_code->second == 17)
			{
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				bufShift += (3 + addBit.to_bitset_reverse(0, 3).to_number(0, 3));
				if (bufShift >= hdist)
					break;
			}
			else if (_code->second == 18)
			{
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				addBit.push_back_bit(block.get(i++));
				bufShift += (11 + addBit.to_bitset_reverse(0, 7).to_number(0, 7));
				if (bufShift >= hdist)
					break;
			}
			else
			{
				codeOffsets[_code->second].push_back(bufShift);
				++bufShift;
				if (bufShift >= hdist)
					break;
			}
			currentSequence.clear();
		}
	}
	currentSequence.clear();

	// 8) ������������ ���� �������� � ����
	set<pair<short, short>, decltype(comparatorTblGroupped)> tableCodeSymbolsGroupped(comparatorTblGroupped);
	for (auto tc : codeSymbols)
		for (auto v : tc.second)
			tableCodeSymbolsGroupped.insert({ tc.first, v });
	for (auto&& tbl : tableCodeSymbolsGroupped)
		cout << tbl.first << " --- " << tbl.second << endl;
	cout << endl;
	type_codes codesSymbolsAndLength(comparator�odes);
	for (int j(0); j < tableCodeSymbolsGroupped.begin()->first; j++)
		bitCode.push_back_bit(0);
	currentCount = bitCode.size_bits();
	for (auto tg = tableCodeSymbolsGroupped.begin(); tg != tableCodeSymbolsGroupped.end(); ++tg)
	{
		short _count(-1);
		if (tg->first != currentCount)
		{
			currentCount = tg->first;
			_count = currentCount - 1;
			bitCode.push_back_bit(0);
		}
		else
			_count = currentCount;
		short num_old = bitCode.to_number(0, _count);
#pragma region DEBUG
		cout << "old num: " << num_old << endl;
#pragma endregion
		if (tg != tableCodeSymbolsGroupped.begin())
		{
			num_old++;
			for (short k(_count - 1), b(0); k >= 0; --k, ++b)
				bitCode.set(k, num_old & (1 << b));
#pragma region DEBUG
			short num_new = bitCode.to_number(0, currentCount > _count ? currentCount : _count);
			cout << "new num: " << num_new << endl;
#pragma endregion
		}
		cout << endl;
		codesSymbolsAndLength[bitCode] = tg->second;
	}
	for (auto c : codesSymbolsAndLength)
		cout << c.first.to_string() << " ---> " << static_cast<char>(c.second) << endl;
	cout << endl;
	bitCode.clear();

	// 8.1) ������������ ���� ��������
	set<pair<short, short>, decltype(comparatorTblGroupped)> tableCodeOffsetsGroupped(comparatorTblGroupped);
	for (auto tc : codeOffsets)
		for (auto v : tc.second)
			tableCodeOffsetsGroupped.insert({ tc.first, v });
	for (auto&& tbl : tableCodeOffsetsGroupped)
		cout << tbl.first << " --- " << tbl.second << endl;
	cout << endl;
	type_codes codesOffsets(comparator�odes);
	for (int j(0); j < tableCodeOffsetsGroupped.begin()->first; j++)
		bitCode.push_back_bit(0);
	currentCount = bitCode.size_bits();
	for (auto tg = tableCodeOffsetsGroupped.begin(); tg != tableCodeOffsetsGroupped.end(); ++tg)
	{
		short _count(-1);
		if (tg->first != currentCount)
		{
			currentCount = tg->first;
			_count = currentCount - 1;
			bitCode.push_back_bit(0);
		}
		else
			_count = currentCount;
		short num_old = bitCode.to_number(0, _count);
#pragma region DEBUG
		cout << "old num: " << num_old << endl;
#pragma endregion
		if (tg != tableCodeOffsetsGroupped.begin())
		{
			num_old++;
			for (short k(_count - 1), b(0); k >= 0; --k, ++b)
				bitCode.set(k, num_old & (1 << b));
#pragma region DEBUG
			short num_new = bitCode.to_number(0, currentCount > _count ? currentCount : _count);
			cout << "new num: " << num_new << endl;
#pragma endregion
		}
		cout << endl;
		codesOffsets[bitCode] = tg->second;
	}
	for (auto c : codesOffsets)
		cout << c.first.to_string() << " ---> " << c.second << endl;
	cout << endl;
	bitCode.clear();

	// 9) ���������� ��������
	string outLine;
	int iter(0);
	while (true)
	{
		currentSequence.push_back_bit(block.get(i++));
		// ���� � �����
		type_codes::iterator _code = codesSymbolsAndLength.find(currentSequence);
		if (_code != codesSymbolsAndLength.end())
		{
			if (_code->second < 255)
				outLine += static_cast<char>(_code->second);
			else
			{
				if (_code->second == 256)
					break; // ����� ����� ������
				int length = tableLength.calculate(block, i, _code->second); // �������� �����
				currentSequence.clear();
				type_codes::iterator _codeOffset; // ��������
				while (true)
				{
					currentSequence.push_back_bit(block.get(i++));
					_codeOffset = codesOffsets.find(currentSequence);
					if (_codeOffset != codesOffsets.end())
					{
						break;
					}
				}
				int offset = tableOffset.calculate(block, i, _codeOffset->second); // �������� ��������
				//if (offset > outLine.length()) // debug
				//	offset = outLine.length();
				auto start_iterator = outLine.begin() + (outLine.length() - offset);
				copy(start_iterator, start_iterator + length, back_inserter(outLine));
				cout << outLine << endl;
			}
			currentSequence.clear();
			continue;
		}
	}
}

int main()
{
	test_load_png();


	////We build the tree depending on the string
	//htTree *codeTree = buildTree("beep boop beer!");
	////We build the table depending on the Huffman tree
	//hlTable *codeTable = buildTable(codeTree);

	////We encode using the Huffman table
	//encode(codeTable,"beep boop beer!");
	////We decode using the Huffman tree
	////We can decode string that only use symbols from the initial string
	//decode(codeTree,"0011111000111");
	////Output : 0011 1110 1011 0001 0010 1010 1100 1111 1000 1001
	return 0;
}

