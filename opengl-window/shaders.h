#pragma once

#include "../pEngine/shaders.h"
#include "../pEngineStatic/utils.h"

#include "types.h"

#include <numeric>
#include <functional>

class Shader
{
	GLuint shader;

	void preprocessor(string& codeShader, string& pathShader);
	int getTypeShader(const wstring& type);

public:
	bool initShader(const wstring& path, const wstring& type);
	bool createShader(wstring path, wstring type);
	GLuint getShader();

};

class ContainerShader // container shaders of auto loading shaders
{
	map<pair<wstring, wstring>, Shader> shaders;

	void loadShader(const wstring& path, const wstring& type);

public:
	Shader& getVertexShader(const wstring& path);

	Shader& getPixelShader(const wstring& path);

	void addVertexShader(const wstring& path);

	void addPixelShader(const wstring& path);

};